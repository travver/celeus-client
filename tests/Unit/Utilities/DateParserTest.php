<?php

declare(strict_types=1);

namespace Tests\Unit\Utilities;

use Celeus\Exceptions\ParseDateFailed;
use Celeus\Utilities\DateParser;
use Tests\TestCase;

/**
 * @covers \Celeus\Utilities\DateParser
 */
final class DateParserTest extends TestCase
{
    public function test_a_date_can_be_parsed_from_a_string(): void
    {
        $date = DateParser::parse('2021-01-02');

        $this->assertEquals('02-01-2021', $date->format('d-m-Y'));
    }

    public function test_an_exception_is_thrown_then_the_string_is_invalid(): void
    {
        $this->expectException(ParseDateFailed::class);
        $this->expectExceptionMessage('Failed to parse date "invalid-date"');

        DateParser::parse('invalid-date');
    }
}