<?php

declare(strict_types=1);

namespace Tests\Unit\Utilities;

use Celeus\Utilities\Clock;
use DateTimeImmutable;
use Tests\TestCase;

/**
 * @covers \Celeus\Utilities\Clock
 */
final class ClockTest extends TestCase
{
    public function test_the_clock_returns_a_datetime_immutable_object(): void
    {
        $clock = new Clock();
        
        $this->assertInstanceOf(DateTimeImmutable::class, $clock->now());
    }
}