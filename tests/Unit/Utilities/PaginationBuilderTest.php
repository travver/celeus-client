<?php

declare(strict_types=1);

namespace Tests\Unit\Utilities;

use Celeus\Requests\Pagination\Pagination;
use Celeus\Requests\Request;
use Celeus\Utilities\PaginationBuilder;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Utilities\PaginationBuilder
 * @covers \Celeus\Responses\Pagination\Pagination
 */
final class PaginationBuilderTest extends TestCase
{
    public function test_a_pagination_object_is_built_from_a_response_and_request_object(): void
    {
        $response = Mockery::mock(ResponseInterface::class);
        $response->expects('getHeader')
            ->withArgs(['X-Per-Page'])
            ->andReturn(['10']);

        $response->expects('getHeader')
            ->withArgs(['X-Total-Count'])
            ->andReturn(['512']);

        $request = Mockery::mock(Request::class);
        $request->expects('getPagination')
            ->andReturn(new Pagination(5, 10));

        $pagination = PaginationBuilder::build($response, $request);

        $this->assertEquals(5, $pagination->page);
        $this->assertEquals(10, $pagination->perPage);
        $this->assertEquals(512, $pagination->total);
        $this->assertEquals(52, $pagination->pages);
    }
}