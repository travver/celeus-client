<?php

declare(strict_types=1);

namespace Tests\Unit\Content;

use Celeus\Entities\Content\Tag;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Content\Tag
 */
final class TagTest extends TestCase
{
    public function test_a_tag_can_be_created_from_an_array(): void
    {
        $tag = Tag::fromArray([
            'id' => 3,
            'title' => 'Stel of groep',
            'url' => 'stel-of-groep',
            'description' => '<h2 style="text-align: center;">Op stap zonder kids?</h2><p style="text-align: center;">Een overzicht van activireiten die wat minder geschikt zijn voor kleine kinderen</p>',
            'meta_description' => 'Met een groep of als stel eropuit en genieten van de leuke activiteiten in de omgeving van het vakantiepark',
            'meta_keywords' => 'stel, groep, activiteiten, bungalow, vakantiepark',
            'page_title' => 'Stel of groep',
            'as_category' => true,
            'as_filter' => true,
            'list_order' => 1,
            'href' => '/content/tags/3.json',
        ]);

        $this->assertEquals(3, $tag->id);
        $this->assertEquals('Stel of groep', $tag->title);
        $this->assertEquals('stel-of-groep', $tag->url);
        $this->assertEquals('<h2 style="text-align: center;">Op stap zonder kids?</h2><p style="text-align: center;">Een overzicht van activireiten die wat minder geschikt zijn voor kleine kinderen</p>', $tag->description);
        $this->assertEquals('Met een groep of als stel eropuit en genieten van de leuke activiteiten in de omgeving van het vakantiepark', $tag->metaDescription);
        $this->assertEquals('stel, groep, activiteiten, bungalow, vakantiepark', $tag->metaKeywords);
        $this->assertEquals('Stel of groep', $tag->pageTitle);
        $this->assertEquals(true, $tag->asCategory);
        $this->assertEquals(true, $tag->asFilter);
        $this->assertEquals(1, $tag->listOrder);
        $this->assertEquals('/content/tags/3.json', $tag->href);
    }
}