<?php

declare(strict_types=1);

namespace Tests\Unit\Values;

use Celeus\Values\ImageUrl;
use Tests\TestCase;

/**
 * @covers \Celeus\Values\ImageUrl
 */
final class ImageUrlTest extends TestCase
{
    public function test_an_image_url_can_be_created_from_an_array(): void
    {
        $imageUrl = ImageUrl::fromArray([
            'main' => 'https://www.example.org/main-image.jpg',
            'thumbnail' => 'https://www.example.org/thumbnail-image.jpg',
        ]);

        $this->assertEquals('https://www.example.org/main-image.jpg', $imageUrl->main);
        $this->assertEquals('https://www.example.org/thumbnail-image.jpg', $imageUrl->thumbnail);
    }
}