<?php

declare(strict_types=1);

namespace Tests\Unit\Values;

use Celeus\Exceptions\InvalidPeriodException;
use Celeus\Values\DatePeriod;
use DateTimeImmutable;
use Tests\TestCase;

/**
 * @covers \Celeus\Values\DatePeriod
 */
final class DatePeriodTest extends TestCase
{
    public function test_a_date_period_can_be_created_from_an_array(): void
    {
        $datePeriod = DatePeriod::fromArray([
            'from' => '2021-01-01',
            'until' => '2021-01-31',
        ]);

        $this->assertEquals(new DateTimeImmutable('2021-01-01 00:00:00'), $datePeriod->from);
        $this->assertEquals(new DateTimeImmutable('2021-01-31 00:00:00'), $datePeriod->until);
    }

    public function test_the_start_date_of_a_date_period_can_be_equal_to_the_end_date(): void
    {
        $datePeriod = new DatePeriod(
            new DateTimeImmutable('2021-01-01 00:00:00'),
            new DateTimeImmutable('2021-01-01 00:00:00'),
        );

        $this->assertEquals(new DateTimeImmutable('2021-01-01 00:00:00'), $datePeriod->from);
        $this->assertEquals(new DateTimeImmutable('2021-01-01 00:00:00'), $datePeriod->until);
    }

    public function test_the_start_date_of_a_date_period_can_not_be_after_the_end_date(): void
    {
        $this->expectException(InvalidPeriodException::class);

        new DatePeriod(
            new DateTimeImmutable('2021-01-31 00:00:00'),
            new DateTimeImmutable('2021-01-01 00:00:00'),
        );
    }
}