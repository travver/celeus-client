<?php

declare(strict_types=1);

namespace Tests\Unit\Values;

use Celeus\Enums\DiscountRestrictionType;
use Celeus\Values\DiscountRestriction;
use Tests\TestCase;

/**
 * @covers \Celeus\Values\DiscountRestriction
 */
final class DiscountRestrictionTest extends TestCase
{
    public function test_a_discount_restriction_can_be_created_from_an_array(): void
    {
        $discountRestriction = DiscountRestriction::fromArray([
            'type' => 'ebd',
            'limit' => 50,
        ]);

        $this->assertEquals(DiscountRestrictionType::EBD, $discountRestriction->type);
        $this->assertEquals(50, $discountRestriction->limit);
    }
}