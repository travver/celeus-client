<?php

declare(strict_types=1);

namespace Tests\Unit\Values;

use Celeus\Enums\CostType;
use Celeus\Values\FeePerBooking;
use Tests\TestCase;

/**
 * @covers \Celeus\Values\FeePerBooking
 */
final class FeePerBookingTest extends TestCase
{
    public function test_a_fee_per_booking_can_be_created_from_an_array(): void
    {
        $feePerBooking = FeePerBooking::fromArray([
            'value' => 15,
            'type' => 'percentage',
        ]);

        $this->assertEquals(15, $feePerBooking->value);
        $this->assertEquals(CostType::PERCENTAGE, $feePerBooking->type);
    }
}