<?php

declare(strict_types=1);

namespace Tests\Unit\Values;

use Celeus\Values\ImageInfo;
use InvalidArgumentException;
use Tests\TestCase;

/**
 * @covers \Celeus\Values\ImageInfo
 */
final class ImageInfoTest extends TestCase
{
    public function test_an_image_info_can_be_created_from_an_array(): void
    {
        $imageInfo = ImageInfo::fromArray([
            '0' => 640,
            '1' => 480,
            '2' => IMAGETYPE_JPEG,
            '3' => 'width="640" height="480"',
            'bits' => 8,
            'channels' => 3,
            'mime' => 'image/jpeg',
        ]);

        $this->assertEquals(640, $imageInfo->width);
        $this->assertEquals(480, $imageInfo->height);
        $this->assertEquals(IMAGETYPE_JPEG, $imageInfo->imageType);
        $this->assertEquals('width="640" height="480"', $imageInfo->widthHeightString);
        $this->assertEquals(8, $imageInfo->bits);
        $this->assertEquals(3, $imageInfo->channels);
        $this->assertEquals('image/jpeg', $imageInfo->mime);
    }

    public function test_null_is_returned_when_no_data_is_provided(): void
    {
        $imageInfo = ImageInfo::fromArray(null);

        $this->assertNull($imageInfo);
    }

    public function test_the_image_width_can_not_be_zero(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The image width must be greater than zero.');

        new ImageInfo(
            width: 0,
            height: 480,
            imageType: IMAGETYPE_JPEG,
            widthHeightString: 'width="640" height="480"',
            bits: 8,
            channels: 3,
            mime: 'image/jpeg',
        );
    }

    public function test_the_image_width_can_not_be_negative(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The image width must be greater than zero.');

        new ImageInfo(
            width: -1,
            height: 480,
            imageType: IMAGETYPE_JPEG,
            widthHeightString: 'width="640" height="480"',
            bits: 8,
            channels: 3,
            mime: 'image/jpeg',
        );
    }

    public function test_the_image_height_can_not_be_zero(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The image height must be greater than zero.');

        new ImageInfo(
            width: 640,
            height: 0,
            imageType: IMAGETYPE_JPEG,
            widthHeightString: 'width="640" height="480"',
            bits: 8,
            channels: 3,
            mime: 'image/jpeg',
        );
    }

    public function test_the_image_height_can_not_be_negative(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The image height must be greater than zero.');

        new ImageInfo(
            width: 640,
            height: -1,
            imageType: IMAGETYPE_JPEG,
            widthHeightString: 'width="640" height="480"',
            bits: 8,
            channels: 3,
            mime: 'image/jpeg',
        );
    }
}