<?php

declare(strict_types=1);

namespace Tests\Unit\Values;

use Celeus\Values\Invoice;
use InvalidArgumentException;
use Tests\TestCase;

/**
 * @covers \Celeus\Values\Invoice
 */
final class InvoiceTest extends TestCase
{
    public function test_an_invoice_can_be_created_from_an_array(): void
    {
        $invoice = Invoice::fromArray([
            'language' => 1,
            'issue' => true,
            'email' => 'email@example.org',
            'address' => 'Street 123, City, Region',
        ]);

        $this->assertEquals(1, $invoice->languageId);
        $this->assertTrue($invoice->issue);
        $this->assertEquals('email@example.org', $invoice->email);
        $this->assertEquals('Street 123, City, Region', $invoice->address);
    }

    public function test_a_valid_invoice_email_address_is_required(): void {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The email address is not valid.');

        new Invoice(
            languageId: 1,
            issue: true,
            email: 'invalid-email-address',
            address: 'Street 123, City, Region',
        );
    }
}