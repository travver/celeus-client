<?php

declare(strict_types=1);

namespace Tests\Unit\Values;

use Celeus\Values\ImageTypeProperties;
use InvalidArgumentException;
use Tests\TestCase;

/**
 * @covers \Celeus\Values\ImageTypeProperties
 */
final class ImageTypePropertiesTest extends TestCase
{
    public function test_an_image_type_properties_can_be_created_from_an_array(): void
    {
        $imageTypeProperties = ImageTypeProperties::fromArray([
            'directory' => 'image-directory',
            'width' => 1920,
            'height' => 1080,
            'thumbnail' => true,
            'thumbnail_width' => 50,
            'thumbnail_height' => 50,
        ]);

        $this->assertEquals('image-directory', $imageTypeProperties->directory);
        $this->assertEquals(1920, $imageTypeProperties->width);
        $this->assertEquals(1080, $imageTypeProperties->height);
        $this->assertTrue($imageTypeProperties->thumbnail);
        $this->assertEquals(50, $imageTypeProperties->thumbnailWidth);
        $this->assertEquals(50, $imageTypeProperties->thumbnailHeight);
    }

    public function test_the_image_type_properties_width_can_not_be_zero(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The image width must be greater than zero.');

        new ImageTypeProperties(
            directory: 'image-directory',
            width: 0,
            height: 1080,
            thumbnail: true,
            thumbnailWidth: 50,
            thumbnailHeight: 50,
        );
    }

    public function test_the_image_type_properties_width_can_not_be_negative(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The image width must be greater than zero.');

        new ImageTypeProperties(
            directory: 'image-directory',
            width: -1,
            height: 1080,
            thumbnail: true,
            thumbnailWidth: 50,
            thumbnailHeight: 50,
        );
    }

    public function test_the_image_type_properties_height_can_not_be_zero(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The image height must be greater than zero.');

        new ImageTypeProperties(
            directory: 'image-directory',
            width: 1920,
            height: -1,
            thumbnail: true,
            thumbnailWidth: 50,
            thumbnailHeight: 50,
        );
    }

    public function test_the_image_type_properties_height_can_not_be_negative(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The image height must be greater than zero.');

        new ImageTypeProperties(
            directory: 'image-directory',
            width: 1920,
            height: -1,
            thumbnail: true,
            thumbnailWidth: 50,
            thumbnailHeight: 50,
        );
    }

    public function test_the_image_type_properties_thumbnail_width_can_not_be_zero(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The image thumbnail width must be greater than zero.');

        new ImageTypeProperties(
            directory: 'image-directory',
            width: 1920,
            height: 1080,
            thumbnail: true,
            thumbnailWidth: 0,
            thumbnailHeight: 50,
        );
    }

    public function test_the_image_type_properties_thumbnail_width_can_not_be_negative(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The image thumbnail width must be greater than zero.');

        new ImageTypeProperties(
            directory: 'image-directory',
            width: 1920,
            height: 1080,
            thumbnail: true,
            thumbnailWidth: -1,
            thumbnailHeight: 50,
        );
    }

    public function test_the_image_type_properties_thumbnail_height_can_not_be_zero(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The image thumbnail height must be greater than zero.');

        new ImageTypeProperties(
            directory: 'image-directory',
            width: 1920,
            height: 1080,
            thumbnail: true,
            thumbnailWidth: 50,
            thumbnailHeight: 0,
        );
    }

    public function test_the_image_type_properties_thumbnail_height_can_not_be_negative(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The image thumbnail height must be greater than zero.');

        new ImageTypeProperties(
            directory: 'image-directory',
            width: 1920,
            height: 1080,
            thumbnail: true,
            thumbnailWidth: 50,
            thumbnailHeight: -1,
        );
    }
}