<?php

declare(strict_types=1);

namespace Tests\Unit\Values;

use Celeus\Enums\CostType;
use Celeus\Values\Commission;
use InvalidArgumentException;
use Tests\TestCase;

/**
 * @covers \Celeus\Values\Commission
 */
final class CommissionTest extends TestCase
{
    public function test_a_commission_can_be_created_from_an_array(): void
    {
        $commission = Commission::fromArray([
            'value' => 19.95,
            'type' => 'value',
            'vat' => 21,
            'vat_included' => true,
        ]);

        $this->assertEquals(19.95, $commission->value);
        $this->assertEquals(CostType::VALUE, $commission->type);
        $this->assertEquals(21, $commission->vat);
        $this->assertTrue($commission->vatIncluded);
    }

    public function test_the_commission_vat_percentage_can_not_be_negative(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('VAT percentage can not be negative.');

        new Commission(
            value: 19.95,
            type: CostType::VALUE,
            vat: -1,
            vatIncluded: true,
        );
    }
}