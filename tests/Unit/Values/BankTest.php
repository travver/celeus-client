<?php

declare(strict_types=1);

namespace Tests\Unit\Values;

use Celeus\Values\Bank;
use Tests\TestCase;

/**
 * @covers \Celeus\Values\Bank
 */
final class BankTest extends TestCase
{
    public function test_a_bank_can_be_created_from_an_array(): void
    {
        $bank = Bank::fromArray([
            'iban' => 'bank-iban',
            'name' => 'bank-name',
            'swift' => 'bank-swift',
            'bic' => 'bank-bic',
            'holder' => 'bank-holder',
        ]);

        $this->assertEquals('bank-iban', $bank->iban);
        $this->assertEquals('bank-name', $bank->name);
        $this->assertEquals('bank-swift', $bank->swift);
        $this->assertEquals('bank-bic', $bank->bic);
        $this->assertEquals('bank-holder', $bank->holder);
    }
}