<?php

declare(strict_types=1);

namespace Tests\Unit\Values;

use Celeus\Values\Time;
use InvalidArgumentException;
use Tests\TestCase;

/**
 * @covers \Celeus\Values\Time
 */
final class TimeTest extends TestCase
{
    public function test_a_time_can_be_created_from_a_string(): void
    {
        $time = Time::fromString('12:34:56');

        $this->assertEquals(12, $time->hour);
        $this->assertEquals(34, $time->minute);
        $this->assertEquals(56, $time->second);

        $this->assertEquals('12:34:56', (string)$time);
    }

    public function test_the_time_hours_cannot_exceed_23_hours(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid time provided.');

        new Time(
            hour: 24,
            minute: 0,
            second: 0,
        );
    }

    public function test_the_time_hours_cannot_be_negative(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid time provided.');

        new Time(
            hour: -1,
            minute: 0,
            second: 0,
        );
    }

    public function test_the_time_minutes_cannot_exceed_59_minutes(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid time provided.');

        new Time(
            hour: 0,
            minute: 60,
            second: 0,
        );
    }

    public function test_the_time_minutes_cannot_be_negative(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid time provided.');

        new Time(
            hour: 0,
            minute: -1,
            second: 0,
        );
    }

    public function test_the_time_seconds_cannot_exceed_59_seconds(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid time provided.');

        new Time(
            hour: 0,
            minute: 0,
            second: 60,
        );
    }

    public function test_the_time_seconds_cannot_be_negative(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid time provided.');

        new Time(
            hour: 0,
            minute: 0,
            second: -1,
        );
    }
}