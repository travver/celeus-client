<?php

declare(strict_types=1);

namespace Tests\Unit\Values;

use Celeus\Values\SameDay;
use Tests\TestCase;

/**
 * @covers \Celeus\Values\SameDay
 */
final class SameDayTest extends TestCase
{
    public function test_a_same_day_can_be_created_from_an_array(): void
    {
        $sameDay = SameDay::fromArray([
            'confirmation' => 'test-confirmation',
            'stop' => 'test-stop',
        ]);

        $this->assertEquals('test-confirmation', $sameDay->confirmation);
        $this->assertEquals('test-stop', $sameDay->stop);
    }

    public function test_the_same_day_is_null_when_the_array_is_null(): void
    {
        $sameDay = SameDay::fromArray(null);

        $this->assertNull($sameDay);
    }
}