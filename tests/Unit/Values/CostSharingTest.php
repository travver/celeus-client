<?php

declare(strict_types=1);

namespace Tests\Unit\Values;

use Celeus\Enums\CostType;
use Celeus\Values\CostSharing;
use InvalidArgumentException;
use Tests\TestCase;

/**
 * @covers \Celeus\Values\CostSharing
 */
final class CostSharingTest extends TestCase
{
    public function test_a_cost_sharing_can_be_created_from_an_array(): void
    {
        $costSharing = CostSharing::fromArray([
            'value' => 19.95,
            'type' => 'percentage',
            'vat' => 21,
        ]);

        $this->assertEquals(19.95, $costSharing->value);
        $this->assertEquals(CostType::PERCENTAGE, $costSharing->type);
        $this->assertEquals(21, $costSharing->vat);
    }

    public function test_the_cost_sharing_vat_percentage_can_not_be_negative(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('VAT percentage can not be negative.');

        new CostSharing(
            value: 19.95,
            type: CostType::PERCENTAGE,
            vat: -1,
        );
    }
}