<?php

declare(strict_types=1);

namespace Tests\Unit\Values;

use Celeus\Enums\CalculationBase;
use Celeus\Enums\CostType;
use Celeus\Values\Discount;
use Tests\TestCase;

/**
 * @covers \Celeus\Values\Discount
 */
final class DiscountTest extends TestCase
{
    public function test_a_discount_can_be_created_from_an_array(): void
    {
        $discount = Discount::fromArray([
            'type' => 'percentage',
            'base' => 'full',
            'value' => 10,
        ]);

        $this->assertEquals(CostType::PERCENTAGE, $discount->type);
        $this->assertEquals(CalculationBase::FULL, $discount->base);
        $this->assertEquals(10, $discount->value);
    }
}