<?php

declare(strict_types=1);

namespace Tests\Unit\Client;

use Celeus\Authentication\Token;
use Celeus\Client\Configuration;
use DateTimeImmutable;
use InvalidArgumentException;
use Tests\TestCase;

/**
 * @covers \Celeus\Client\Configuration
 */
final class ConfigurationTest extends TestCase
{
    public function test_an_invalid_argument_exception_is_thrown_when_an_invalid_endpoint_is_provided(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid endpoint');

        new Configuration('invalid-endpoint');
    }

    public function test_an_invalid_argument_exception_is_thrown_when_no_schema_is_provided(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid endpoint');

        new Configuration('example.org');
    }

    public function test_an_invalid_argument_exception_is_thrown_when_no_host_is_provided(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid endpoint');

        new Configuration('https://');
    }

    public function test_the_configuration_can_be_constructed_with_a_valid_endpoint(): void
    {
        $configuration = new Configuration('https://www.example.org/endpoint');

        $this->assertEquals('https://www.example.org/endpoint', $configuration->endpoint);
        $this->assertNull($configuration->token);
    }

    public function test_trailing_slashes_are_removed_from_the_endpoint(): void
    {
        $configuration = new Configuration('https://www.example.org/endpoint/');

        $this->assertEquals('https://www.example.org/endpoint', $configuration->endpoint);
    }

    public function test_a_token_is_present_when_provided(): void
    {
        $configuration = new Configuration(
            'https://www.example.org/endpoint/',
            new Token(
                'test-token',
                new DateTimeImmutable(),
                [],
                true,
            )
        );

        $this->assertEquals('test-token', $configuration->token->token);
    }
}