<?php

declare(strict_types=1);

namespace Tests\Unit\Client;

use Celeus\Authentication\Token;
use Celeus\Client\Client;
use Celeus\Client\Configuration;
use Celeus\Enums\RequestMethod;
use Celeus\Requests\Filters\Filter;
use Celeus\Requests\Options\Options;
use Celeus\Requests\Pagination\Pagination;
use Celeus\Requests\Request;
use Celeus\Responses\Response;
use DateTimeImmutable;
use Mockery;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;
use RuntimeException;
use Tests\Fakes\ClockFake;
use Tests\TestCase;

/**
 * @covers \Celeus\Client\Client
 */
final class ClientTest extends TestCase
{
    public function test_the_client_can_acquire_a_token(): void
    {
        $request = Mockery::mock(RequestInterface::class);
        $request->expects('withHeader')
            ->withArgs([
                'Authorization',
                'Basic ' . base64_encode('username:password'),
            ])
            ->andReturn($request);

        $requestFactory = Mockery::mock(RequestFactoryInterface::class);
        $requestFactory->expects('createRequest')->andReturn($request);

        $stream = Mockery::mock(StreamInterface::class);
        $stream->expects('getContents')->andReturn(json_encode([
            'access_token' => 'test-token',
            'expires_in' => 3600,
            'permissions' => ['test-permission1', 'test-permission2'],
        ]));

        $response = Mockery::mock(ResponseInterface::class);
        $response->expects('getBody')->andReturn($stream);

        $httpClient = Mockery::mock(ClientInterface::class);
        $httpClient->expects('sendRequest')->withArgs([$request])->andReturn($response);

        $client = new Client(
            $httpClient,
            $requestFactory,
            Mockery::mock(StreamFactoryInterface::class),
            new Configuration('https://www.example.org/endpoint'),
            new ClockFake('2021-01-01 00:00:00'),
        );

        $token = $client->acquireToken('username', 'password');

        $this->assertEquals('test-token', $token->token);
        $this->assertEquals(new DateTimeImmutable('2021-01-01 01:00:00'), $token->expiresAt);
        $this->assertEquals(['test-permission1', 'test-permission2'], $token->permissions);
        $this->assertFalse($token->verified);
    }

    public function test_the_client_can_validate_a_token(): void
    {
        $request = Mockery::mock(RequestInterface::class);
        $request->expects('withBody')->andReturn($request);

        $stream = Mockery::mock(StreamInterface::class);

        $requestFactory = Mockery::mock(RequestFactoryInterface::class);
        $requestFactory->expects('createRequest')
            ->withArgs(['POST', 'https://www.example.org/endpoint/token/verify'])
            ->andReturn($request);

        $streamFactory = Mockery::mock(StreamFactoryInterface::class);
        $streamFactory->expects('createStream')->andReturn($stream);

        $response = Mockery::mock(ResponseInterface::class);
        $response->expects('getStatusCode')->andReturn(204);

        $httpClient = Mockery::mock(ClientInterface::class);
        $httpClient->expects('sendRequest')->withArgs([$request])->andReturn($response);

        $inputToken = new Token(
            'test-token',
            new DateTimeImmutable('2021-01-01 01:00:00'),
            ['test-permission1', 'test-permission2'],
            false,
        );

        $client = new Client(
            $httpClient,
            $requestFactory,
            $streamFactory,
            new Configuration('https://www.example.org/endpoint', $inputToken),
            new ClockFake('2021-01-01 00:00:00'),
        );

        $outputToken = $client->verifyToken('123456');

        $this->assertEquals('test-token', $outputToken->token);
        $this->assertEquals(new DateTimeImmutable('2021-01-01 01:00:00'), $outputToken->expiresAt);
        $this->assertEquals(['test-permission1', 'test-permission2'], $outputToken->permissions);
        $this->assertTrue($outputToken->verified);
    }

    public function test_the_client_returns_false_when_not_successfully_validating_a_token(): void
    {
        $request = Mockery::mock(RequestInterface::class);
        $request->expects('withBody')->andReturn($request);

        $stream = Mockery::mock(StreamInterface::class);

        $requestFactory = Mockery::mock(RequestFactoryInterface::class);
        $requestFactory->expects('createRequest')
            ->withArgs(['POST', 'https://www.example.org/endpoint/token/verify'])
            ->andReturn($request);

        $streamFactory = Mockery::mock(StreamFactoryInterface::class);
        $streamFactory->expects('createStream')->andReturn($stream);

        $response = Mockery::mock(ResponseInterface::class);
        $response->expects('getStatusCode')->andReturn(401);

        $httpClient = Mockery::mock(ClientInterface::class);
        $httpClient->expects('sendRequest')->withArgs([$request])->andReturn($response);

        $inputToken = new Token(
            'test-token',
            new DateTimeImmutable('2021-01-01 01:00:00'),
            ['test-permission1', 'test-permission2'],
            false,
        );

        $client = new Client(
            $httpClient,
            $requestFactory,
            $streamFactory,
            new Configuration('https://www.example.org/endpoint', $inputToken),
            new ClockFake('2021-01-01 00:00:00'),
        );

        $outputToken = $client->verifyToken('123456');

        $this->assertFalse($outputToken);
    }

    public function test_the_client_will_execute_requests_when_provided_with_a_request_class(): void
    {
        $fakeRequest = new class implements Request {
            public function getEndpoint(): string
            {
                return '/test-endpoint.json';
            }

            public function getFilter(): ?Filter
            {
                return new class implements Filter {
                    public function getQueryParameters(): array
                    {
                        return [
                            'filter1' => 'value1',
                            'filter2' => 'value2',
                        ];
                    }
                };
            }

            public function getOptions(): ?Options
            {
                return new class implements Options {
                    public function getQueryParameters(): array
                    {
                        return [
                            'option1' => 'value1',
                            'option2' => 'value2',
                        ];
                    }
                };
            }

            public function getPagination(): ?Pagination
            {
                return new Pagination(1, 60);
            }

            public function getMethod(): RequestMethod
            {
                return RequestMethod::GET;
            }

            public function getResponse(ResponseInterface $response): Response
            {
                return new class('test-response') implements Response {
                    public function __construct(public readonly string $testResponse) {}

                    public static function create(Request $request, ResponseInterface $response): Response
                    {
                        return new self('test-response');
                    }
                };
            }
        };

        $request = Mockery::mock(RequestInterface::class);
        $request->expects('withHeader')
            ->withArgs(['Authorization', 'Bearer test-token'])
            ->andReturn($request);

        $requestFactory = Mockery::mock(RequestFactoryInterface::class);
        $requestFactory->expects('createRequest')
            ->withArgs([
                'GET',
                'https://www.example.org/endpoint/test-endpoint.json?filter1=value1&filter2=value2&option1=value1&option2=value2&page=1&per_page=60',
            ])
            ->andReturn($request);

        $response = Mockery::mock(ResponseInterface::class);
        $response->expects('getStatusCode')->twice()->andReturn(200);

        $httpClient = Mockery::mock(ClientInterface::class);
        $httpClient->expects('sendRequest')
            ->withArgs([$request])
            ->andReturn($response);

        $client = new Client(
            $httpClient,
            $requestFactory,
            Mockery::mock(StreamFactoryInterface::class),
            new Configuration('https://www.example.org/endpoint', new Token(
                'test-token',
                new DateTimeImmutable('2021-01-01 01:00:00'),
                ['test-permission1', 'test-permission2'],
                true,
            )),
            new ClockFake('2021-01-01 00:00:00'),
        );

        $response = $client->get($fakeRequest);

        $this->assertEquals('test-response', $response->testResponse);
    }

    public function test_the_client_will_omit_query_parameters_when_no_filters_or_pagination_provided(): void
    {
        $fakeRequest = new class implements Request {
            public function getEndpoint(): string
            {
                return '/test-endpoint.json';
            }

            public function getFilter(): ?Filter
            {
                return null;
            }

            public function getOptions(): ?Options
            {
                return null;
            }

            public function getPagination(): ?Pagination
            {
                return null;
            }

            public function getMethod(): RequestMethod
            {
                return RequestMethod::GET;
            }

            public function getResponse(ResponseInterface $response): Response
            {
                return new class('test-response') implements Response {
                    public function __construct(public readonly string $testResponse) {}

                    public static function create(Request $request, ResponseInterface $response): Response
                    {
                        return new self('test-response');
                    }
                };
            }
        };

        $request = Mockery::mock(RequestInterface::class);
        $request->expects('withHeader')
            ->withArgs(['Authorization', 'Bearer test-token'])
            ->andReturn($request);

        $requestFactory = Mockery::mock(RequestFactoryInterface::class);
        $requestFactory->expects('createRequest')
            ->withArgs([
                'GET',
                'https://www.example.org/endpoint/test-endpoint.json',
            ])
            ->andReturn($request);

        $response = Mockery::mock(ResponseInterface::class);
        $response->expects('getStatusCode')->twice()->andReturn(200);

        $httpClient = Mockery::mock(ClientInterface::class);
        $httpClient->expects('sendRequest')
            ->withArgs([$request])
            ->andReturn($response);

        $client = new Client(
            $httpClient,
            $requestFactory,
            Mockery::mock(StreamFactoryInterface::class),
            new Configuration('https://www.example.org/endpoint', new Token(
                'test-token',
                new DateTimeImmutable('2021-01-01 01:00:00'),
                ['test-permission1', 'test-permission2'],
                true,
            )),
            new ClockFake('2021-01-01 00:00:00'),
        );

        $response = $client->get($fakeRequest);

        $this->assertEquals('test-response', $response->testResponse);
    }

    public function test_the_client_throws_an_exception_when_the_service_returns_a_404_response(): void
    {
        $fakeRequest = new class implements Request {
            public function getEndpoint(): string
            {
                return '/test-endpoint.json';
            }

            public function getFilter(): ?Filter
            {
                return null;
            }

            public function getOptions(): ?Options
            {
                return null;
            }

            public function getPagination(): ?Pagination
            {
                return null;
            }

            public function getMethod(): RequestMethod
            {
                return RequestMethod::GET;
            }

            public function getResponse(ResponseInterface $response): Response
            {
                return new class('test-response') implements Response {
                    public function __construct(public readonly string $testResponse) {}

                    public static function create(Request $request, ResponseInterface $response): Response
                    {
                        return new self('test-response');
                    }
                };
            }
        };

        $request = Mockery::mock(RequestInterface::class);
        $request->expects('withHeader')
            ->withArgs(['Authorization', 'Bearer test-token'])
            ->andReturn($request);

        $requestFactory = Mockery::mock(RequestFactoryInterface::class);
        $requestFactory->expects('createRequest')
            ->withArgs([
                'GET',
                'https://www.example.org/endpoint/test-endpoint.json',
            ])
            ->andReturn($request);

        $response = Mockery::mock(ResponseInterface::class);
        $response->expects('getStatusCode')->andReturn(404);

        $httpClient = Mockery::mock(ClientInterface::class);
        $httpClient->expects('sendRequest')
            ->withArgs([$request])
            ->andReturn($response);

        $client = new Client(
            $httpClient,
            $requestFactory,
            Mockery::mock(StreamFactoryInterface::class),
            new Configuration('https://www.example.org/endpoint', new Token(
                'test-token',
                new DateTimeImmutable('2021-01-01 01:00:00'),
                ['test-permission1', 'test-permission2'],
                true,
            )),
            new ClockFake('2021-01-01 00:00:00'),
        );

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Not found');

        $client->get($fakeRequest);
    }

    public function test_the_client_throws_an_exception_when_the_service_returns_a_401_response(): void
    {
        $fakeRequest = new class implements Request {
            public function getEndpoint(): string
            {
                return '/test-endpoint.json';
            }

            public function getFilter(): ?Filter
            {
                return null;
            }

            public function getOptions(): ?Options
            {
                return null;
            }

            public function getPagination(): ?Pagination
            {
                return null;
            }

            public function getMethod(): RequestMethod
            {
                return RequestMethod::GET;
            }

            public function getResponse(ResponseInterface $response): Response
            {
                return new class('test-response') implements Response {
                    public function __construct(public readonly string $testResponse) {}

                    public static function create(Request $request, ResponseInterface $response): Response
                    {
                        return new self('test-response');
                    }
                };
            }
        };

        $request = Mockery::mock(RequestInterface::class);
        $request->expects('withHeader')
            ->withArgs(['Authorization', 'Bearer test-token'])
            ->andReturn($request);

        $requestFactory = Mockery::mock(RequestFactoryInterface::class);
        $requestFactory->expects('createRequest')
            ->withArgs([
                'GET',
                'https://www.example.org/endpoint/test-endpoint.json',
            ])
            ->andReturn($request);

        $response = Mockery::mock(ResponseInterface::class);
        $response->expects('getStatusCode')->twice()->andReturn(401);

        $httpClient = Mockery::mock(ClientInterface::class);
        $httpClient->expects('sendRequest')
            ->withArgs([$request])
            ->andReturn($response);

        $client = new Client(
            $httpClient,
            $requestFactory,
            Mockery::mock(StreamFactoryInterface::class),
            new Configuration('https://www.example.org/endpoint', new Token(
                'test-token',
                new DateTimeImmutable('2021-01-01 01:00:00'),
                ['test-permission1', 'test-permission2'],
                true,
            )),
            new ClockFake('2021-01-01 00:00:00'),
        );

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Unauthorized');

        $client->get($fakeRequest);
    }

    public function test_the_client_throws_an_exception_when_no_token_is_provided_for_the_token_verification(): void
    {
        $configuration = new Configuration('https://www.example.org/endpoint', null);

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('No token provided for verification');

        $client = new Client(
            Mockery::mock(ClientInterface::class),
            Mockery::mock(RequestFactoryInterface::class),
            Mockery::mock(StreamFactoryInterface::class),
            $configuration,
            new ClockFake('2021-01-01 00:00:00'),
        );

        $client->verifyToken('123467');
    }

    public function test_the_client_throws_an_exception_when_no_token_is_provided_for_a_request(): void
    {
        $configuration = new Configuration('https://www.example.org/endpoint', null);

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('No token provided');

        $client = new Client(
            Mockery::mock(ClientInterface::class),
            Mockery::mock(RequestFactoryInterface::class),
            Mockery::mock(StreamFactoryInterface::class),
            $configuration,
            new ClockFake('2021-01-01 00:00:00'),
        );

        $request = new class implements Request {

            public function getEndpoint(): string
            {
                return '';
            }

            public function getFilter(): ?Filter
            {
                return null;
            }

            public function getOptions(): ?Options
            {
                return null;
            }

            public function getPagination(): ?Pagination
            {
                return null;
            }

            public function getMethod(): RequestMethod
            {
                return RequestMethod::GET;
            }

            public function getResponse(ResponseInterface $response): Response
            {
                return new class('test-response') implements Response {
                    public function __construct(public readonly string $testResponse) {}

                    public static function create(Request $request, ResponseInterface $response): Response
                    {
                        return new self('test-response');
                    }
                };
            }
        };

        $client->get($request);
    }
}