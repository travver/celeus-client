<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\City;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\City
 */
final class CityTest extends TestCase
{
    public function test_a_city_can_be_created_from_an_array(): void
    {
        $city = City::fromArray([
            'id' => 423,
            'name' => 'Test city',
            'region_id' => 234,
            'href' => '/cities/423.json',
        ]);

        $this->assertEquals(423, $city->id);
        $this->assertEquals('Test city', $city->name);
        $this->assertEquals(234, $city->regionId);
        $this->assertEquals('/cities/423.json', $city->href);
        $this->assertNull($city->region);
        $this->assertNull($city->country);
    }
}