<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\Country;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Country
 */
final class CountryTest extends TestCase
{
    public function test_a_country_can_be_created_from_an_array(): void
    {
        $country = Country::fromArray([
            'id' => 123,
            'name' => 'Test country',
            'url' => 'https://example.org',
            'list_order' => 3,
            'code' => 'NL',
            'href' => '/countries/1.json',
        ]);

        $this->assertEquals(123, $country->id);
        $this->assertEquals('Test country', $country->name);
        $this->assertEquals('https://example.org', $country->url);
        $this->assertEquals(3, $country->listOrder);
        $this->assertEquals('NL', $country->code);
        $this->assertEquals('/countries/1.json', $country->href);
    }
}