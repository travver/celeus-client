<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\ChainContract;
use Celeus\Enums\CostType;
use Celeus\Enums\PaymentType;
use DateTimeImmutable;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\ChainContract
 */
final class ChainContractTest extends TestCase
{
    public function test_a_chain_contract_can_be_created_from_an_array(): void
    {
        $chainContract = ChainContract::fromArray([
            'id' => 1,
            'commission' => [
                'value' => 10,
                'type' => 'percentage',
                'vat' => 21,
                'vat_included' => true,
            ],
            'cost_sharing' => [
                'value' => 10,
                'type' => 'value',
                'vat' => 21,
            ],
            'fee_per_booking' => [
                'value' => 19.95,
                'type' => 'value',
            ],
            'target_bonus' => 'Test target bonus',
            'payment_type' => 'by_invoice',
            'debtor_number' => 'Test debtor number',
            'vat_number' => 'Test vat-number',
            'invoice' => [
                'language' => 1,
                'issue' => true,
                'email' => 'test@example.org',
                'address' => 'Test address',
            ],
            'contact_email' => 'Test contact email',
            'bank' => [
                'iban' => 'Test IBAN',
                'name' => 'Test bank name',
                'swift' => 'Test swift',
                'bic' => 'Test bic',
                'holder' => 'Test holder',
            ],
            'costs' => 'Test costs',
            'arrival' => [
                'from' => '2021-01-01',
                'until' => '2021-12-31',
            ],
            'creation' => [
                'from' => '2020-01-01',
                'until' => '2020-12-31',
            ],
            'href' => '/chain_contracts/1.json',
        ]);

        $this->assertEquals(1, $chainContract->id);
        $this->assertEquals(10, $chainContract->commission->value);
        $this->assertEquals(CostType::PERCENTAGE, $chainContract->commission->type);
        $this->assertEquals(21, $chainContract->commission->vat);
        $this->assertTrue($chainContract->commission->vatIncluded);
        $this->assertEquals(10, $chainContract->costSharing->value);
        $this->assertEquals(CostType::VALUE, $chainContract->costSharing->type);
        $this->assertEquals(21, $chainContract->costSharing->vat);
        $this->assertEquals(19.95, $chainContract->feePerBooking->value);
        $this->assertEquals(CostType::VALUE, $chainContract->feePerBooking->type);
        $this->assertEquals('Test target bonus', $chainContract->targetBonus);
        $this->assertEquals(PaymentType::BY_INVOICE, $chainContract->paymentType);
        $this->assertEquals('Test debtor number', $chainContract->debtorNumber);
        $this->assertEquals('Test vat-number', $chainContract->vatNumber);
        $this->assertEquals(1, $chainContract->invoice->languageId);
        $this->assertTrue($chainContract->invoice->issue);
        $this->assertEquals('test@example.org', $chainContract->invoice->email);
        $this->assertEquals('Test address', $chainContract->invoice->address);
        $this->assertEquals('Test contact email', $chainContract->contactEmail);
        $this->assertEquals('Test IBAN', $chainContract->bank->iban);
        $this->assertEquals('Test bank name', $chainContract->bank->name);
        $this->assertEquals('Test swift', $chainContract->bank->swift);
        $this->assertEquals('Test bic', $chainContract->bank->bic);
        $this->assertEquals('Test holder', $chainContract->bank->holder);
        $this->assertEquals('Test costs', $chainContract->costs);
        $this->assertEquals(new DateTimeImmutable('2021-01-01 00:00:00'), $chainContract->arrival->from);
        $this->assertEquals(new DateTimeImmutable('2021-12-31 00:00:00'), $chainContract->arrival->until);
        $this->assertEquals(new DateTimeImmutable('2020-01-01 00:00:00'), $chainContract->creation->from);
        $this->assertEquals(new DateTimeImmutable('2020-12-31 00:00:00'), $chainContract->creation->until);
        $this->assertEquals('/chain_contracts/1.json', $chainContract->href);
    }
}