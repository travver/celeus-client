<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\Region;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Region
 */
final class RegionTest extends TestCase
{
    public function test_a_region_can_be_created_from_an_array(): void
    {
        $region = Region::fromArray([
            'id' => 213,
            'name' => 'Test region',
            'url' => 'https://example.org',
            'country_id' => 3,
            'administrative' => true,
            'href' => '/region/213.json',
        ]);

        $this->assertEquals(213, $region->id);
        $this->assertEquals('Test region', $region->name);
        $this->assertEquals('https://example.org', $region->url);
        $this->assertEquals(3, $region->countryId);
        $this->assertTrue($region->administrative);
        $this->assertEquals('/region/213.json', $region->href);
    }
}