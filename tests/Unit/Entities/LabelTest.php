<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\Label;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Label
 */
final class LabelTest extends TestCase
{
    public function test_a_label_can_be_created_from_an_array(): void
    {
        $label = Label::fromArray([
            'id' => 123,
            'name' => 'Test label',
            'name_list' => 'Test name list',
            'href' => '/labels/123.json',
        ]);

        $this->assertEquals(123, $label->id);
        $this->assertEquals('Test label', $label->name);
        $this->assertEquals('Test name list', $label->nameList);
        $this->assertEquals('/labels/123.json', $label->href);
    }
}