<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\FacilitiesCategory;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\FacilitiesCategory
 */
final class FacilitiesCategoryTest extends TestCase
{
    public function test_a_facilities_category_can_be_created_from_an_array(): void
    {
        $facilitiesCategory = FacilitiesCategory::fromArray([
            'id' => 123,
            'name' => 'Test facility',
            'for_properties' => true,
            'for_accommodations' => false,
            'href' => '/facilities/123.json',
        ]);

        $this->assertEquals(123, $facilitiesCategory->id);
        $this->assertEquals('Test facility', $facilitiesCategory->name);
        $this->assertTrue($facilitiesCategory->forProperties);
        $this->assertFalse($facilitiesCategory->forAccommodations);
        $this->assertEquals('/facilities/123.json', $facilitiesCategory->href);
    }
}