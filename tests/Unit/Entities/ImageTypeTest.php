<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\ImageType;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\ImageType
 */
final class ImageTypeTest extends TestCase
{
    public function test_an_image_type_can_be_created_from_an_array(): void
    {
        $imageType = ImageType::fromArray([
            'id' => 321,
            'name' => 'Image type name',
            'properties' => [
                'directory' => '/accommodations/',
                'width' => 100,
                'height' => 100,
                'thumbnail' => true,
                'thumbnail_width' => 50,
                'thumbnail_height' => 50,
            ],
            'href' => '/image_types/321.json',
            'url' => 'https://example.org/image_types/321.jpg',
        ]);

        $this->assertEquals(321, $imageType->id);
        $this->assertEquals('Image type name', $imageType->name);
        $this->assertEquals('/accommodations/', $imageType->properties->directory);
        $this->assertEquals(100, $imageType->properties->width);
        $this->assertEquals(100, $imageType->properties->height);
        $this->assertTrue($imageType->properties->thumbnail);
        $this->assertEquals(50, $imageType->properties->thumbnailWidth);
        $this->assertEquals(50, $imageType->properties->thumbnailHeight);
        $this->assertEquals('/image_types/321.json', $imageType->href);
        $this->assertEquals('https://example.org/image_types/321.jpg', $imageType->url);
    }
}