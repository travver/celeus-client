<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\ChainMessage;
use Celeus\Enums\MessageVisibility;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\ChainMessage
 */
final class ChainMessageTest extends TestCase
{
    public function test_a_chain_message_can_be_created_from_an_array(): void
    {
        $chainMessage = ChainMessage::fromArray([
            'id' => 1,
            'date_from' => '2021-01-01',
            'date_until' => '2021-12-31',
            'date_stay' => true,
            'message' => 'Test message',
            'list_order' => 1,
            'published' => true,
            'visibility' => [
                'website',
            ],
            'exclude_properties' => [1, 2, 3],
            'href' => '/test/category',
        ], null);

        $this->assertEquals(1, $chainMessage->id);
        $this->assertNull($chainMessage->category);
        $this->assertEquals('2021-01-01', $chainMessage->dateFrom->format('Y-m-d'));
        $this->assertEquals('2021-12-31', $chainMessage->dateUntil->format('Y-m-d'));
        $this->assertTrue($chainMessage->dateStay);
        $this->assertEquals('Test message', $chainMessage->message);
        $this->assertEquals(1, $chainMessage->listOrder);
        $this->assertTrue($chainMessage->published);
        $this->assertEquals([MessageVisibility::WEBSITE], $chainMessage->visibility);
        $this->assertEquals([1, 2, 3], $chainMessage->excludeProperties);
        $this->assertEquals('/test/category', $chainMessage->href);
    }
}