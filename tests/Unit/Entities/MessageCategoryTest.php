<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\MessageCategory;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\MessageCategory
 */
final class MessageCategoryTest extends TestCase
{
    public function test_a_message_category_can_be_created_from_an_array(): void
    {
        $messageCategory = MessageCategory::fromArray([
            'id' => 1,
            'name' => 'Test message category',
            'list_order' => 0,
            'href' => '/message_categories/1.json',
        ]);

        $this->assertEquals(1, $messageCategory->id);
        $this->assertEquals('Test message category', $messageCategory->name);
        $this->assertEquals(0, $messageCategory->listOrder);
        $this->assertEquals('/message_categories/1.json', $messageCategory->href);
    }
}