<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\Property;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Property
 */
final class PropertyTest extends TestCase
{
    public function test_a_property_can_be_created_from_an_array(): void
    {
        $property = Property::fromArray([
            'id' => 1,
            'chain_id' => 2,
            'city_id' => 3,
            'label' => 4,
            'name' => 'Test property',
            'name_sorting' => 'Test property',
            'name_letter' => 'T',
            'url' => 'https://example.org',
            'description' => 'Test description',
            'good_to_know' => 'Test good to know',
            'remarks_comments' => 'Test remarks comments',
            'usp' => 'Test usp',
            'address' => 'Test address',
            'postalcode' => '1234AB',
            'check_in' => '15:00:00',
            'check_out' => '11:00:00',
            'latitude' => 1.234,
            'longitude' => 5.678,
            'published' => true,
            'label_name' => 'Test label name',
            'label_in_list' => 'Test label in list',
            'notification_email' => 'email@example.org',
            'wj_cards' => true,
            'facilities' => [1, 2, 3],
            'href' => '/properties/1.json',
        ]);

        $this->assertEquals(1, $property->id);
        $this->assertEquals(2, $property->chainId);
        $this->assertEquals(3, $property->cityId);
        $this->assertEquals(4, $property->label);
        $this->assertEquals('Test property', $property->name);
        $this->assertEquals('Test property', $property->nameSorting);
        $this->assertEquals('T', $property->nameLetter);
        $this->assertEquals('https://example.org', $property->url);
        $this->assertEquals('Test description', $property->description);
        $this->assertEquals('Test good to know', $property->goodToKnow);
        $this->assertEquals('Test remarks comments', $property->remarksComments);
        $this->assertEquals('Test usp', $property->usp);
        $this->assertEquals('Test address', $property->address);
        $this->assertEquals('1234AB', $property->postalCode);
        $this->assertEquals('15:00:00', (string)$property->checkIn);
        $this->assertEquals('11:00:00', (string)$property->checkOut);
        $this->assertEquals(1.234, $property->latitude);
        $this->assertEquals(5.678, $property->longitude);
        $this->assertTrue($property->published);
        $this->assertEquals('Test label name', $property->labelName);
        $this->assertEquals('Test label in list', $property->labelInList);
        $this->assertEquals('email@example.org', $property->notificationEmail);
        $this->assertTrue($property->giftCardsAllowed);
        $this->assertEquals([1, 2, 3], $property->facilities);
        $this->assertEquals('/properties/1.json', $property->href);
    }
}