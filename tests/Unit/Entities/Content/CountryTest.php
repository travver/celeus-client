<?php

declare(strict_types=1);

namespace Tests\Unit\Entities\Content;

use Celeus\Entities\Content\Country;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Content\Country
 */
final class CountryTest extends TestCase
{
    public function test_a_country_can_be_created_from_arrays(): void
    {
        $country = Country::fromArray([
            'id' => 123,
            'name' => 'Country',
            'url' => 'ct',
            'list_order' => 5,
            'code' => 'CT',
            'href' => '/countries/123.json',
        ], [
            'id' => 456,
            'title' => 'Country content title',
            'teaser' => 'This is teaser text for the country',
            'description' => 'This is a description for the country',
            'meta_keywords' => 'these, are, the, meta, keywords, for, the, country',
            'meta_description' => 'This is a meta description for the country',
            'page_title' => 'Country page title',
            'as_category' => true,
            'as_filter' => true,
        ]);

        $this->assertEquals(123, $country->id);
        $this->assertEquals('Country', $country->name);
        $this->assertEquals('ct', $country->url);
        $this->assertEquals(5, $country->listOrder);
        $this->assertEquals('CT', $country->code);
        $this->assertEquals('/countries/123.json', $country->href);
        $this->assertEquals(456, $country->contentId);
        $this->assertEquals('Country content title', $country->title);
        $this->assertEquals('This is teaser text for the country', $country->teaser);
        $this->assertEquals('This is a description for the country', $country->description);
        $this->assertEquals('these, are, the, meta, keywords, for, the, country', $country->metaKeywords);
        $this->assertEquals('This is a meta description for the country', $country->metaDescription);
        $this->assertEquals('Country page title', $country->pageTitle);
        $this->assertTrue($country->asCategory);
        $this->assertTrue($country->asFilter);
    }
}