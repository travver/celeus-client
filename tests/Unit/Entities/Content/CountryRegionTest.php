<?php

declare(strict_types=1);

namespace Tests\Unit\Entities\Content;

use Celeus\Entities\Content\CountryRegion;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Content\CountryRegion
 */
final class CountryRegionTest extends TestCase
{
    public function test_a_country_region_can_be_created_from_array(): void
    {
        $countryRegion = CountryRegion::fromArray([
            'id' => 8,
            'name' => 'Country',
            'url' => 'country',
            'list_order' => 3,
            'code' => 'NL',
            'href' => '/counties/8.json',
        ], [
            'id' => 123,
            'name' => 'Region',
            'url' => 'rg',
            'country_id' => 5,
            'administrative' => true,
            'href' => '/content/regions/123.json',
        ], [
            'title' => 'Region content title',
            'teaser' => 'This is teaser text for the region',
            'description' => 'This is a description for the region',
            'meta_keywords' => 'these, are, the, meta, keywords, for, the, region',
            'meta_description' => 'This is a meta description for the region',
            'page_title' => 'Region page title',
            'list_order' => 3,
            'as_category' => true,
            'as_filter' => true,
        ]);

        $this->assertEquals(8, $countryRegion->countryId);
        $this->assertEquals('Country', $countryRegion->countryName);
        $this->assertEquals('country', $countryRegion->countryUrl);
        $this->assertEquals(3, $countryRegion->countryListOrder);
        $this->assertEquals('NL', $countryRegion->countryCode);
        $this->assertEquals('/counties/8.json', $countryRegion->countryHref);
        $this->assertEquals(123, $countryRegion->regionId);
        $this->assertEquals('Region', $countryRegion->regionName);
        $this->assertEquals('rg', $countryRegion->regionUrl);
        $this->assertTrue($countryRegion->regionAdministrative);
        $this->assertEquals('/content/regions/123.json',  $countryRegion->regionHref);
        $this->assertEquals('Region content title', $countryRegion->title);
        $this->assertEquals('This is teaser text for the region', $countryRegion->teaser);
        $this->assertEquals('This is a description for the region', $countryRegion->description);
        $this->assertEquals('these, are, the, meta, keywords, for, the, region', $countryRegion->metaKeywords);
        $this->assertEquals('This is a meta description for the region', $countryRegion->metaDescription);
        $this->assertEquals('Region page title', $countryRegion->pageTitle);
        $this->assertEquals(3, $countryRegion->listOrder);
        $this->assertTrue($countryRegion->asCategory);
        $this->assertTrue($countryRegion->asFilter);
    }
}