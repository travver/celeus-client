<?php

declare(strict_types=1);

namespace Tests\Unit\Entities\Content;

use Celeus\Entities\Content\ListingActivity;
use Celeus\Entities\Content\ListingCountry;
use Celeus\Entities\Content\ListingRegion;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Content\ListingActivity
 */
final class ListingActivityTest extends TestCase
{
    public function test_a_listing_activity_can_be_created_from_an_array(): void
    {
        $activity = ListingActivity::fromArray([
            'id' => 834,
            'title' => 'Abbaye de Fontenay',
            'published' => true,
            'region' => [
                'id' => 82,
                'name' => 'Bourgogne-Franche-Comté',
                'url' => 'bourgogne-france-comte'
            ],
            'country' => [
                'id' => 10,
                'name' => 'Frankrijk',
                'url' => 'frankrijk'
            ],
            'href' => '/content/activities/834.json',
        ]);

        $this->assertEquals(834, $activity->id);
        $this->assertEquals('Abbaye de Fontenay', $activity->title);
        $this->assertEquals(true, $activity->published);
        $this->assertInstanceOf(ListingRegion::class, $activity->region);
        $this->assertEquals(82, $activity->region->id);
        $this->assertEquals('Bourgogne-Franche-Comté', $activity->region->name);
        $this->assertEquals('bourgogne-france-comte', $activity->region->url);
        $this->assertInstanceOf(ListingCountry::class, $activity->country);
        $this->assertEquals(10, $activity->country->id);
        $this->assertEquals('Frankrijk', $activity->country->name);
        $this->assertEquals('frankrijk', $activity->country->url);
        $this->assertEquals('/content/activities/834.json', $activity->href);
    }
}