<?php

declare(strict_types=1);

namespace Tests\Unit\Entities\Content;

use Celeus\Entities\Content\ListingTag;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Content\ListingTag
 */
final class ListingTagTest extends TestCase
{
    public function test_a_listing_tag_can_be_created_from_an_array(): void
    {
        $tag = ListingTag::fromArray([
            'id' => 3,
            'title' => 'Stel of groep',
            'as_category' => true,
            'as_filter' => true,
            'list_order' => 1,
            'activities' => 657,
            'href' => '/content/tags/3.json',
        ]);

        $this->assertEquals(3, $tag->id);
        $this->assertEquals('Stel of groep', $tag->title);
        $this->assertEquals(true, $tag->asCategory);
        $this->assertEquals(true, $tag->asFilter);
        $this->assertEquals(1, $tag->listOrder);
        $this->assertEquals(657, $tag->activities);
        $this->assertEquals('/content/tags/3.json', $tag->href);
    }
}