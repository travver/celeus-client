<?php

declare(strict_types=1);

namespace Tests\Unit\Entities\Content;

use Celeus\Entities\Content\ListingRegion;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Content\ListingRegion
 */
final class ListingRegionTest extends TestCase
{
    public function test_a_listing_region_can_be_created_from_an_array(): void
    {
        $listingRegion = ListingRegion::fromArray([
            'id' => 123,
            'name' => 'Region',
            'url' => 'region',
            'content_id' => null,
        ]);

        $this->assertEquals(123, $listingRegion->id);
        $this->assertEquals('Region', $listingRegion->name);
        $this->assertEquals('region', $listingRegion->url);
        $this->assertNull($listingRegion->contentId);
    }
}