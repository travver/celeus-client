<?php

declare(strict_types=1);

namespace Tests\Unit\Entities\Content;

use Celeus\Entities\Content\Activity;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Content\Activity
 */
final class ActivityTest extends TestCase
{
    public function test_a_activity_can_be_created_from_arrays(): void
    {
        $activity = Activity::fromArray([
            'id' => 238,
            'title' => 'Adbij de Westerburcht',
            'url' => 'adbij-de-westerburcht',
            'usp' => 'Culinair genieten in een Abdij',
            'teaser' => 'Abdij de Westerburcht is een ideale combinatie van een historische ambiance, streekgebonden producten en Drentse gastvrijheid.',
            'description' => '<p>Naast heerlijke gerechten voor lunch en diner, is er een uitgebreide bierkaart passend bij de seizoensgerechten. Uiteraard kun je, passend bij de locatie, een trappistenbier bestellen. Ook is het mogelijk voor een High Tea, High Wine of High Beer te kiezen met hartige hapjes.</p>',
            'source_url' => 'https://www.westerburcht.nl',
            'source_url_text' => 'Website: Westerburcht',
            'image' => '',
            'image_title' => '',
            'image_source' => '',
            'address' => 'Abdij de Westerburcht Hoofdstraat 7 9431 AB Westerbork',
            'meta_keywords' => 'lunch, diner, eten, drinken, abdij, culinair, bungalowpark, vakantiepar',
            'meta_description' => 'High Tea, High Wine of High Beer in de Abdij de Westerburcht in Drenthe.',
            'page_title' => 'Abdij de Westerburcht',
            'icon' => 'silverware',
            'latitude' => 52.8496507,
            'longitude' => 6.606351,
            'latitude_min' => 52.5801607,
            'longitude_min' => 6.14221744,
            'latitude_max' => 53.1191407,
            'radius' => 30,
            'region_id' => 15,
            'published' => true,
            'href' => '/content/activities/238.json',
            'tags' => [
                15, 2, 3
            ],
        ]);

        $this->assertEquals(238, $activity->id);
        $this->assertEquals('Adbij de Westerburcht', $activity->title);
        $this->assertEquals('adbij-de-westerburcht', $activity->url);
        $this->assertEquals('Culinair genieten in een Abdij', $activity->usp);
        $this->assertEquals('Abdij de Westerburcht is een ideale combinatie van een historische ambiance, streekgebonden producten en Drentse gastvrijheid.', $activity->teaser);
        $this->assertEquals('<p>Naast heerlijke gerechten voor lunch en diner, is er een uitgebreide bierkaart passend bij de seizoensgerechten. Uiteraard kun je, passend bij de locatie, een trappistenbier bestellen. Ook is het mogelijk voor een High Tea, High Wine of High Beer te kiezen met hartige hapjes.</p>', $activity->description);
        $this->assertEquals('https://www.westerburcht.nl', $activity->sourceUrl);
        $this->assertEquals('Website: Westerburcht', $activity->sourceUrlText);
        $this->assertEquals('', $activity->image);
        $this->assertEquals('', $activity->imageTitle);
        $this->assertEquals('', $activity->imageSource);
        $this->assertEquals('Abdij de Westerburcht Hoofdstraat 7 9431 AB Westerbork', $activity->address);
        $this->assertEquals('lunch, diner, eten, drinken, abdij, culinair, bungalowpark, vakantiepar', $activity->metaKeywords);
        $this->assertEquals('High Tea, High Wine of High Beer in de Abdij de Westerburcht in Drenthe.', $activity->metaDescription);
        $this->assertEquals('Abdij de Westerburcht', $activity->pageTitle);
        $this->assertEquals('silverware', $activity->icon);
        $this->assertEquals(52.8496507, $activity->latitude);
        $this->assertEquals(6.606351, $activity->longitude);
        $this->assertEquals(52.5801607, $activity->latitudeMin);
        $this->assertEquals(6.14221744, $activity->longitudeMin);
        $this->assertEquals(53.1191407, $activity->latitudeMax);
        $this->assertEquals(30, $activity->radius);
        $this->assertEquals(15, $activity->regionId);
        $this->assertTrue($activity->published);
        $this->assertEquals('/content/activities/238.json', $activity->href);
        $this->assertIsArray($activity->tags);
        $this->assertEquals([15, 2, 3], $activity->tags);
    }
}