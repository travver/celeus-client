<?php

declare(strict_types=1);

namespace Tests\Unit\Entities\Content;

use Celeus\Entities\Content\Region;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Content\Region
 */
final class RegionTest extends TestCase
{
    public function test_a_region_can_be_created_from_arrays(): void
    {
        $region = Region::fromArray([
            'id' => 123,
            'name' => 'Region',
            'url' => 'rg',
            'country_id' => 5,
            'administrative' => true,
            'href' => '/content/regions/123.json',
        ], [
            'title' => 'Region content title',
            'teaser' => 'This is teaser text for the region',
            'description' => 'This is a description for the region',
            'meta_keywords' => 'these, are, the, meta, keywords, for, the, region',
            'meta_description' => 'This is a meta description for the region',
            'page_title' => 'Region page title',
            'list_order' => 3,
            'as_category' => true,
            'as_filter' => true,
        ]);

        $this->assertEquals(123, $region->id);
        $this->assertEquals('Region', $region->name);
        $this->assertEquals('rg', $region->url);
        $this->assertEquals(5, $region->countryId);
        $this->assertTrue($region->administrative);
        $this->assertEquals('/content/regions/123.json', $region->href);
        $this->assertEquals('Region content title', $region->title);
        $this->assertEquals('This is teaser text for the region', $region->teaser);
        $this->assertEquals('This is a description for the region', $region->description);
        $this->assertEquals('these, are, the, meta, keywords, for, the, region', $region->metaKeywords);
        $this->assertEquals('This is a meta description for the region', $region->metaDescription);
        $this->assertEquals('Region page title', $region->pageTitle);
        $this->assertEquals(3, $region->listOrder);
        $this->assertTrue($region->asCategory);
        $this->assertTrue($region->asFilter);
    }
}