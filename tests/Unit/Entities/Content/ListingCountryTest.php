<?php

declare(strict_types=1);

namespace Tests\Unit\Entities\Content;

use Celeus\Entities\Content\ListingCountry;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Content\ListingCountry
 */
final class ListingCountryTest extends TestCase
{
    public function test_a_listing_country_can_be_created_from_an_array(): void
    {
        $listingCountry = ListingCountry::fromArray([
            'id' => 123,
            'name' => 'Country',
            'url' => 'country',
            'content_id' => null,
        ]);

        $this->assertEquals(123, $listingCountry->id);
        $this->assertEquals('Country', $listingCountry->name);
        $this->assertEquals('country', $listingCountry->url);
        $this->assertNull($listingCountry->contentId);
    }
}