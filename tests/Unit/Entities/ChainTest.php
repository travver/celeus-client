<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\Chain;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Chain
 */
final class ChainTest extends TestCase
{
    public function test_a_chain_can_be_created_from_an_array(): void
    {
        $chain = Chain::fromArray([
            'id' => 5,
            'name' => 'Test chain',
            'url' => 'https://www.example.org',
            'description' => 'Test chain description',
            'ho_address' => 'Test chain home office address',
            'ho_email' => 'Test chain home office email',
            'ho_phone' => 'Test chain home office phone',
            'ho_language' => 1,
            'notification_email' => 'email@example.org',
            'wj_cards' => true,
            'sameday' => [
                'confirmation' => 'Test chain same day confirmation',
                'stop' => 'Test chain same day stop',
            ],
            'href' => '/chains/5.json',
        ]);

        $this->assertEquals(5, $chain->id);
        $this->assertEquals('Test chain', $chain->name);
        $this->assertEquals('https://www.example.org', $chain->url);
        $this->assertEquals('Test chain description', $chain->description);
        $this->assertEquals('Test chain home office address', $chain->homeOfficeAddress);
        $this->assertEquals('Test chain home office email', $chain->homeOfficeEmail);
        $this->assertEquals('Test chain home office phone', $chain->homeOfficePhone);
        $this->assertEquals(1, $chain->homeOfficeLanguageId);
        $this->assertEquals('email@example.org', $chain->notificationEmail);
        $this->assertTrue($chain->giftCardsAllowed);
        $this->assertEquals('Test chain same day confirmation', $chain->sameDay->confirmation);
        $this->assertEquals('Test chain same day stop', $chain->sameDay->stop);
        $this->assertEquals('/chains/5.json', $chain->href);
    }
}