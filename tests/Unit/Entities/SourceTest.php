<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\Source;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Source
 */
final class SourceTest extends TestCase
{
    public function test_a_source_can_be_created_from_an_array(): void
    {
        $source = Source::fromArray([
            'id' => 4,
            'name' => 'centerparks'
        ]);

        $this->assertEquals(4, $source->id);
        $this->assertEquals('centerparks', $source->name);

        $source = Source::fromArray(null);
        $this->assertNull($source);
    }
}