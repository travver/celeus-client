<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\AccommodationType;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\AccommodationType
 */
final class AccommodationTypeTest extends TestCase
{
    public function test_an_accommodation_type_can_be_created_from_an_array(): void
    {
        $accommodationType = AccommodationType::fromArray([
            'id' => 3,
            'name' => 'Hotel',
            'href' => '/accommodation_types/3.json',
        ]);

        $this->assertEquals(3, $accommodationType->id);
        $this->assertEquals('Hotel', $accommodationType->name);
        $this->assertEquals('/accommodation_types/3.json', $accommodationType->href);
    }
}