<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\AccommodationMessage;
use Celeus\Enums\MessageVisibility;
use DateTimeImmutable;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\AccommodationMessage
 */
final class AccommodationMessageTest extends TestCase
{
    public function test_an_accommodation_message_can_be_created_from_an_array(): void
    {
        $accommodationMessage = AccommodationMessage::fromArray(
            [
                'id' => 456,
                'accommodation_id' => 789,
                'date_from' => '2021-01-02',
                'date_until' => '2021-01-03',
                'date_stay' => true,
                'message' => 'Test Message',
                'list_order' => 1,
                'published' => true,
                'visibility' => [
                    'website',
                    'booking_confirmation',
                ],
                'href' => '/accommodations/789/messages/456.json',
            ],
            null
        );

        $this->assertEquals(456, $accommodationMessage->id);
        $this->assertEquals(789, $accommodationMessage->accommodationId);
        $this->assertNull($accommodationMessage->category);
        $this->assertEquals(new DateTimeImmutable('2021-01-02 00:00:00'), $accommodationMessage->dateFrom);
        $this->assertEquals(new DateTimeImmutable('2021-01-03 00:00:00'), $accommodationMessage->dateUntil);
        $this->assertTrue($accommodationMessage->dateStay);
        $this->assertEquals('Test Message', $accommodationMessage->message);
        $this->assertEquals(1, $accommodationMessage->listOrder);
        $this->assertTrue($accommodationMessage->published);
        $this->assertEquals(
            [
                MessageVisibility::WEBSITE,
                MessageVisibility::BOOKING_CONFIRMATION,
            ],
            $accommodationMessage->visibility
        );
        $this->assertEquals('/accommodations/789/messages/456.json', $accommodationMessage->href);
    }
}