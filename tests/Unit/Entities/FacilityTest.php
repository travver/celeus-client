<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\Facility;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Facility
 */
final class FacilityTest extends TestCase
{
    public function test_a_facility_can_be_created_from_an_array(): void
    {
        $facility = Facility::fromArray([
            'id' => 123,
            'name' => 'Test facility',
            'published' => true,
            'show_in_teaser' => false,
            'icon' => 'test-icon',
            'as_filter' => true,
            'accommodations' => 3,
            'properties' => 2,
            'href' => '/facilities/123.json',
            'categories' => [1, 2, 3],
        ]);

        $this->assertEquals(123, $facility->id);
        $this->assertEquals('Test facility', $facility->name);
        $this->assertTrue($facility->published);
        $this->assertFalse($facility->showInTeaser);
        $this->assertEquals('test-icon', $facility->icon);
        $this->assertTrue($facility->asFilter);
        $this->assertEquals(3, $facility->accommodations);
        $this->assertEquals(2, $facility->properties);
        $this->assertEquals('/facilities/123.json', $facility->href);
        $this->assertEquals([1, 2, 3], $facility->categories);
    }
}