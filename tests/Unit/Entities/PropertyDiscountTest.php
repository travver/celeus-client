<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\PropertyDiscount;
use Celeus\Enums\CalculationBase;
use Celeus\Enums\CostType;
use Celeus\Enums\DiscountRestrictionType;
use Celeus\Enums\Period;
use DateTimeImmutable;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\PropertyDiscount
 */
final class PropertyDiscountTest extends TestCase
{
    public function test_a_property_discount_can_be_created_from_an_array(): void
    {
        $propertyDiscount = PropertyDiscount::fromArray([
            'id' => 123,
            'discount' => [
                'type' => 'percentage',
                'base' => 'full',
                'value' => 10,
            ],
            'restrict' => [
                'type' => 'oldest',
                'limit' => 2,
            ],
            'accommodations_max' => 5,
            'date_booking' => [
                'from' => '2021-01-01',
                'until' => '2021-12-31',
            ],
            'date_arrival' => [
                'from' => '2022-01-01',
                'until' => '2022-12-31',
            ],
            'allow_combination' => false,
            'slogan' => 'Test discount slogan',
            'accommodations' => [1, 2, 3],
            'periods' => [
                'weekend',
                'midweek',
                'week',
            ],
        ]);

        $this->assertEquals(123, $propertyDiscount->id);
        $this->assertEquals(CostType::PERCENTAGE, $propertyDiscount->discount->type);
        $this->assertEquals(10, $propertyDiscount->discount->value);
        $this->assertEquals(CalculationBase::FULL, $propertyDiscount->discount->base);
        $this->assertEquals(DiscountRestrictionType::OLDEST, $propertyDiscount->restriction->type);
        $this->assertEquals(2, $propertyDiscount->restriction->limit);
        $this->assertEquals(5, $propertyDiscount->accommodationsMax);
        $this->assertEquals(new DateTimeImmutable('2021-01-01 00:00:00'), $propertyDiscount->dateBooking->from);
        $this->assertEquals(new DateTimeImmutable('2021-12-31 00:00:00'), $propertyDiscount->dateBooking->until);
        $this->assertEquals(new DateTimeImmutable('2022-01-01 00:00:00'), $propertyDiscount->dateArrival->from);
        $this->assertEquals(new DateTimeImmutable('2022-12-31 00:00:00'), $propertyDiscount->dateArrival->until);
        $this->assertFalse($propertyDiscount->allowCombination);
        $this->assertEquals('Test discount slogan', $propertyDiscount->slogan);
        $this->assertEquals([1, 2, 3], $propertyDiscount->accommodations);
        $this->assertEquals([Period::WEEKEND, Period::MIDWEEK, Period::WEEK], $propertyDiscount->periods);
    }
}