<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\PropertyContract;
use Celeus\Enums\CostType;
use Celeus\Enums\PaymentType;
use DateTimeImmutable;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\PropertyContract
 */
final class PropertyContractTest extends TestCase
{
    public function test_a_property_contract_can_be_created_from_an_array(): void
    {
        $propertyContract = PropertyContract::fromArray([
            'commission' => [
                'value' => 19.95,
                'type' => 'value',
                'vat' => 21,
            ],
            'cost_sharing' => [
                'value' => 19.95,
                'type' => 'value',
                'vat' => 21,
            ],
            'fee_per_booking' => [
                'value' => 19.95,
                'type' => 'value',
            ],
            'target_bonus' => 'Test target bonus',
            'payment_type' => 'at_arrival',
            'debtor_number' => 'Test debtor number',
            'vat_number' => 'Test VAT number',
            'invoice' => [
                'language' => 1,
                'issue' => true,
                'email' => 'email@example.org',
                'address' => 'Street 123, City, Region',
            ],
            'contact_email' => 'email@example.org',
            'bank' => [
                'iban' => 'Test IBAN',
                'name' => 'Test bank name',
                'swift' => 'Test swift name',
                'bic' => 'Test BIC',
                'holder' => 'Test holder',
            ],
            'costs' => 'Test costs',
            'arrival' => [
                'from' => '2021-01-01',
                'until' => '2021-12-31',
            ],
            'creation' => [
                'from' => '2020-01-01',
                'until' => '2020-12-31',
            ],
        ]);

        $this->assertEquals(19.95, $propertyContract->commission->value);
        $this->assertEquals(CostType::VALUE, $propertyContract->commission->type);
        $this->assertEquals(21, $propertyContract->commission->vat);
        $this->assertEquals(19.95, $propertyContract->costSharing->value);
        $this->assertEquals(CostType::VALUE, $propertyContract->costSharing->type);
        $this->assertEquals(21, $propertyContract->costSharing->vat);
        $this->assertEquals(19.95, $propertyContract->feePerBooking->value);
        $this->assertEquals(CostType::VALUE, $propertyContract->feePerBooking->type);
        $this->assertEquals('Test target bonus', $propertyContract->targetBonus);
        $this->assertEquals(PaymentType::AT_ARRIVAL, $propertyContract->paymentType);
        $this->assertEquals('Test debtor number', $propertyContract->debtorNumber);
        $this->assertEquals('Test VAT number', $propertyContract->vatNumber);
        $this->assertEquals(1, $propertyContract->invoice->languageId);
        $this->assertTrue($propertyContract->invoice->issue);
        $this->assertEquals('email@example.org', $propertyContract->invoice->email);
        $this->assertEquals('Street 123, City, Region', $propertyContract->invoice->address);
        $this->assertEquals('email@example.org', $propertyContract->contactEmail);
        $this->assertEquals('Test IBAN', $propertyContract->bank->iban);
        $this->assertEquals('Test bank name', $propertyContract->bank->name);
        $this->assertEquals('Test swift name', $propertyContract->bank->swift);
        $this->assertEquals('Test BIC', $propertyContract->bank->bic);
        $this->assertEquals('Test holder', $propertyContract->bank->holder);
        $this->assertEquals('Test costs', $propertyContract->costs);
        $this->assertEquals(new DateTimeImmutable('2021-01-01 00:00:00'), $propertyContract->arrival->from);
        $this->assertEquals(new DateTimeImmutable('2021-12-31 00:00:00'), $propertyContract->arrival->until);
        $this->assertEquals(new DateTimeImmutable('2020-01-01 00:00:00'), $propertyContract->creation->from);
        $this->assertEquals(new DateTimeImmutable('2020-12-31 00:00:00'), $propertyContract->creation->until);
    }
}