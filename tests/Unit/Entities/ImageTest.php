<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\Image;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Image
 */
final class ImageTest extends TestCase
{
    public function test_an_image_can_be_created_from_an_array(): void
    {
        $image = Image::fromArray([
            'id' => 123,
            'file' => 'image-123.jpg',
            'image_info' => null,
            'thumbnail_info' => null,
            'alt' => 'Image alt text',
            'title' => 'Image title',
            'is_primary' => true,
            'focus_left' => 10,
            'focus_top' => 30,
            'list_order' => 1,
            'relation_value' => 312,
            'relation_type' => 'accommodation',
            'published' => true,
            'type' => [
                'id' => 321,
                'name' => 'Image type name',
                'properties' => [
                    'directory' => '/accommodations/',
                    'width' => 100,
                    'height' => 100,
                    'thumbnail' => true,
                    'thumbnail_width' => 50,
                    'thumbnail_height' => 50,
                ],
                'href' => '/image_types/321.json',
                'url' => 'https://example.org/image_types/321.jpg',
            ],
            'url' => [
                'main' => 'https://example.org/accommodations/image-123.jpg',
                'thumbnail' => 'https://example.org/accommodations/image-123-thumbnail.jpg',
            ],
        ]);

        $this->assertEquals(123, $image->id);
        $this->assertEquals('image-123.jpg', $image->file);
        $this->assertNull($image->imageInfo);
        $this->assertNull($image->thumbnailInfo);
        $this->assertEquals('Image alt text', $image->alt);
        $this->assertEquals('Image title', $image->title);
        $this->assertTrue($image->isPrimary);
        $this->assertEquals(10, $image->focusLeft);
        $this->assertEquals(30, $image->focusTop);
        $this->assertEquals(1, $image->listOrder);
        $this->assertEquals(312, $image->relationValue);
        $this->assertEquals('accommodation', $image->relationType->value);
        $this->assertTrue($image->published);
        $this->assertEquals(321, $image->imageType->id);
        $this->assertEquals('Image type name', $image->imageType->name);
        $this->assertEquals('/accommodations/', $image->imageType->properties->directory);
        $this->assertEquals(100, $image->imageType->properties->width);
        $this->assertEquals(100, $image->imageType->properties->height);
        $this->assertTrue($image->imageType->properties->thumbnail);
        $this->assertEquals(50, $image->imageType->properties->thumbnailWidth);
        $this->assertEquals(50, $image->imageType->properties->thumbnailHeight);
        $this->assertEquals('/image_types/321.json', $image->imageType->href);
        $this->assertEquals('https://example.org/image_types/321.jpg', $image->imageType->url);
        $this->assertEquals('https://example.org/accommodations/image-123.jpg', $image->imageUrl->main);
        $this->assertEquals('https://example.org/accommodations/image-123-thumbnail.jpg', $image->imageUrl->thumbnail);
    }
}