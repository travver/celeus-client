<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\PropertyMessage;
use Celeus\Enums\MessageVisibility;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\PropertyMessage
 */
final class PropertyMessageTest extends TestCase
{
    public function test_a_property_message_can_be_created_from_an_array(): void
    {
        $propertyMessage = PropertyMessage::fromArray([
            'id' => 123,
            'date_from' => null,
            'date_until' => null,
            'date_stay' => false,
            'message' => 'Test message',
            'list_order' => 2,
            'published' => true,
            'visibility' => [
                'website',
                'nice_trip_email',
            ],
            'exclude_accommodations' => [3, 2, 1],
            'href' => '/property/1/message/123.json',
        ], null);

        $this->assertEquals(123, $propertyMessage->id);
        $this->assertNull($propertyMessage->category);
        $this->assertNull($propertyMessage->dateFrom);
        $this->assertNull($propertyMessage->dateUntil);
        $this->assertFalse($propertyMessage->dateStay);
        $this->assertEquals('Test message', $propertyMessage->message);
        $this->assertEquals(2, $propertyMessage->listOrder);
        $this->assertTrue($propertyMessage->published);
        $this->assertEquals([
            MessageVisibility::WEBSITE,
            MessageVisibility::NICE_TRIP_EMAIL,
        ], $propertyMessage->visibility);
        $this->assertEquals([3, 2, 1], $propertyMessage->excludeAccommodations);
        $this->assertEquals('/property/1/message/123.json', $propertyMessage->href);
    }
}