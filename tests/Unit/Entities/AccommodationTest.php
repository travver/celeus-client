<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Celeus\Entities\Accommodation;
use Tests\TestCase;

/**
 * @covers \Celeus\Entities\Accommodation
 */
final class AccommodationTest extends TestCase
{
    public function test_an_accommodation_can_be_created_from_an_array(): void
    {
        $accommodation = Accommodation::fromArray([
            'id' => 123,
            'property_id' => 456,
            'type_id' => 789,
            'label' => 321,
            'name' => 'name-value',
            'url' => 'https://example.org',
            'description' => 'description-value',
            'persons_max' => 6,
            'adults_min' => 2,
            'adults_max' => 5,
            'teenagers_max' => 4,
            'children_max' => 3,
            'babies_max' => 2,
            'pets_max' => 1,
            'bedrooms' => 3,
            'surface' => 100,
            'label_name' => 'label-name-value',
            'label_in_list' => 'label-in-list-value',
            'published' => true,
            'deleted' => false,
            'facilities' => [1, 2, 3],
            'href' => '/accommodations/123.json',
        ]);

        $this->assertEquals(123, $accommodation->id);
        $this->assertEquals(456, $accommodation->propertyId);
        $this->assertEquals(789, $accommodation->typeId);
        $this->assertEquals(321, $accommodation->label);
        $this->assertEquals('name-value', $accommodation->name);
        $this->assertEquals('https://example.org', $accommodation->url);
        $this->assertEquals('description-value', $accommodation->description);
        $this->assertEquals(6, $accommodation->personsMax);
        $this->assertEquals(2, $accommodation->adultsMin);
        $this->assertEquals(5, $accommodation->adultsMax);
        $this->assertEquals(4, $accommodation->teenagersMax);
        $this->assertEquals(3, $accommodation->childrenMax);
        $this->assertEquals(2, $accommodation->babiesMax);
        $this->assertEquals(1, $accommodation->petsMax);
        $this->assertEquals(3, $accommodation->bedrooms);
        $this->assertEquals(100, $accommodation->surface);
        $this->assertEquals('label-name-value', $accommodation->labelName);
        $this->assertEquals('label-in-list-value', $accommodation->labelInList);
        $this->assertTrue($accommodation->published);
        $this->assertFalse($accommodation->deleted);
        $this->assertEquals([1, 2, 3], $accommodation->facilities);
        $this->assertEquals('/accommodations/123.json', $accommodation->href);
    }
}