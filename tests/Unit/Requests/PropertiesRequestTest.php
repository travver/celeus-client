<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Filters\PropertiesRequestFilter;
use Celeus\Requests\PropertiesRequest;
use Celeus\Responses\PropertiesResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\PropertiesRequest
 * @covers \Celeus\Responses\PropertiesResponse
 * @covers \Celeus\Collections\PropertyCollection
 */
final class PropertiesRequestTest extends TestCase
{
    public function test_the_properties_request_can_be_created(): void
    {
        $request = new PropertiesRequest(new PropertiesRequestFilter());

        $this->assertEquals('/properties.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertEquals(new PropertiesRequestFilter(), $request->getFilter());

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['properties' => []]));

        $httpResponse->shouldReceive('getHeader')
            ->with('X-Per-Page')
            ->andReturn([60]);

        $httpResponse->shouldReceive('getHeader')
            ->with('X-Total-Count')
            ->andReturn([12345]);

        $this->assertInstanceOf(PropertiesResponse::class, $request->getResponse($httpResponse));
    }
}