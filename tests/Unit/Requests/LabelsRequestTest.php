<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\LabelsRequest;
use Celeus\Responses\LabelsResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\LabelsRequest
 * @covers \Celeus\Responses\LabelsResponse
 * @covers \Celeus\Collections\LabelCollection
 */
final class LabelsRequestTest extends TestCase
{
    public function test_the_labels_request_can_be_created(): void
    {
        $request = new LabelsRequest();

        $this->assertEquals('/labels.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['labels' => []]));

        $this->assertInstanceOf(LabelsResponse::class, $request->getResponse($httpResponse));
    }
}