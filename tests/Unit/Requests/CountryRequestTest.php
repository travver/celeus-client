<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\CountryRequest;
use Celeus\Responses\CountryResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\CountryRequest
 * @covers \Celeus\Responses\CountryResponse
 * @covers \Celeus\Collections\RegionCollection
 */
final class CountryRequestTest extends TestCase
{
    public function test_the_country_request_can_be_created(): void
    {
        $request = new CountryRequest(4);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                'country' => [
                    'id' => 4,
                    'name' => 'Test country',
                    'url' => 'https://example.org',
                    'list_order' => 2,
                    'code' => 'NL',
                    'href' => '/countries/4.json',
                ],
                'regions' => [],
            ]));

        $this->assertEquals('/countries/4.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(CountryResponse::class, $request->getResponse($httpResponse));
    }
}