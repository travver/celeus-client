<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\CountriesRequest;
use Celeus\Responses\CountriesResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\CountriesRequest
 * @covers \Celeus\Responses\CountriesResponse
 * @covers \Celeus\Collections\CountryCollection
 */
final class CountriesRequestTest extends TestCase
{
    public function test_the_countries_request_can_be_created(): void
    {
        $request = new CountriesRequest();

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['countries' => []]));

        $this->assertEquals('/countries.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(CountriesResponse::class, $request->getResponse($httpResponse));
    }
}