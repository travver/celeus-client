<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RelationType;
use Celeus\Enums\RequestMethod;
use Celeus\Requests\Filters\ImageRequestFilter;
use Celeus\Requests\ImagesRequest;
use Celeus\Responses\ImagesResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\ImagesRequest
 * @covers \Celeus\Responses\ImagesResponse
 * @covers \Celeus\Collections\ImageCollection
 */
final class ImagesRequestTest extends TestCase
{
    public function test_the_images_request_can_be_created(): void
    {
        $request = new ImagesRequest(
            new ImageRequestFilter(2, RelationType::CARD, 123),
        );

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['images' => []]));

        $httpResponse->shouldReceive('getHeader')
            ->with('X-Per-Page')
            ->andReturn([60]);

        $httpResponse->shouldReceive('getHeader')
            ->with('X-Total-Count')
            ->andReturn([12345]);

        $this->assertEquals('/images.json', $request->getEndpoint());
        $this->assertEquals([
            'image_type_id' => 2,
            'relation_type' => 'card',
            'relation_value' => 123,
        ], $request->getFilter()->getQueryParameters());

        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(ImagesResponse::class, $request->getResponse($httpResponse));
    }
}