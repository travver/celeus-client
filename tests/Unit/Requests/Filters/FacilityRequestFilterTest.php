<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Filters;

use Celeus\Requests\Filters\FacilityRequestFilter;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Filters\FacilityRequestFilter
 */
final class FacilityRequestFilterTest extends TestCase
{
    public function test_the_facility_request_filter_can_be_created(): void
    {
        $filter = new FacilityRequestFilter(
            123,
            false,
            true,
            true,
            false,
        );

        $this->assertEquals([
            'category' => 123,
            'published' => false,
            'as_filter' => true,
            'for_properties' => true,
            'for_accommodations' => false,
        ], $filter->getQueryParameters());
    }

    public function test_the_facility_request_filter_will_return_an_empty_array_when_no_filters_are_provided(): void
    {
        $filter = new FacilityRequestFilter();

        $this->assertEquals([], $filter->getQueryParameters());
    }
}