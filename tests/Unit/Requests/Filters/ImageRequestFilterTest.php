<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Filters;

use Celeus\Enums\RelationType;
use Celeus\Requests\Filters\ImageRequestFilter;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Filters\ImageRequestFilter
 */
final class ImageRequestFilterTest extends TestCase
{
    public function test_the_image_request_filter_can_be_created(): void
    {
        $filter = new ImageRequestFilter(
            123,
            RelationType::CHAIN,
            4,
        );

        $this->assertEquals([
            'image_type_id' => 123,
            'relation_type' => 'chain',
            'relation_value' => 4,
        ], $filter->getQueryParameters());
    }

    public function test_the_image_request_filter_will_return_an_empty_array_when_no_filters_are_provided(): void
    {
        $filter = new ImageRequestFilter();

        $this->assertEquals([], $filter->getQueryParameters());
    }
}