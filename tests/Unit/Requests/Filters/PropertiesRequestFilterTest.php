<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Filters;

use Celeus\Requests\Filters\PropertiesRequestFilter;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Filters\PropertiesRequestFilter
 */
final class PropertiesRequestFilterTest extends TestCase
{
    public function test_the_properties_request_filter_can_be_created(): void
    {
        $filter = new PropertiesRequestFilter(
            21,
            22,
            23,
            24,
            25,
            true
        );

        $this->assertEquals([
            'chain' => 21,
            'country' => 22,
            'region' => 23,
            'city' => 24,
            'facility' => 25,
            'published' => true,
        ], $filter->getQueryParameters());
    }

    public function test_the_properties_request_filter_will_return_an_empty_array_when_no_filters_are_provided(): void
    {
        $filter = new PropertiesRequestFilter();

        $this->assertEquals([], $filter->getQueryParameters());
    }
}