<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Filters;

use Celeus\Requests\Filters\AccommodationRequestFilter;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Filters\AccommodationRequestFilter
 */
final class AccommodationRequestFilterTest extends TestCase
{
    public function test_the_accommodation_request_filter_can_be_created(): void
    {
        $filter = new AccommodationRequestFilter(
            123,
            456,
            [1, 2, 3],
            true,
            true
        );

        $this->assertEquals([
            'accommodation' => 123,
            'property' => 456,
            'facilities' => [1, 2, 3],
            'published' => true,
            'source' => true
        ], $filter->getQueryParameters());
    }

    public function test_the_accommodation_request_filter_will_return_an_empty_array_when_no_filters_are_provided(): void
    {
        $filter = new AccommodationRequestFilter();

        $this->assertEquals([], $filter->getQueryParameters());
    }
}