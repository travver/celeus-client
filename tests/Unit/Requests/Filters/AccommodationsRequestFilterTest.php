<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Filters;

use Celeus\Requests\Filters\AccommodationsRequestFilter;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Filters\AccommodationsRequestFilter
 */
final class AccommodationsRequestFilterTest extends TestCase
{
    public function test_the_accommodation_request_filter_can_be_created(): void
    {
        $filter = new AccommodationsRequestFilter(
            21,
            22,
            23
        );

        $this->assertEquals([
            'chain' => 21,
            'property' => 22,
            'facility' => 23,
        ], $filter->getQueryParameters());
    }

    public function test_the_accommodation_request_filter_will_return_an_empty_array_when_no_filters_are_provided(): void
    {
        $filter = new AccommodationsRequestFilter();

        $this->assertEquals([], $filter->getQueryParameters());
    }
}