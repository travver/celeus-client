<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\FacilityRequest;
use Celeus\Responses\FacilityResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\FacilityRequest
 * @covers \Celeus\Responses\FacilityResponse
 */
final class FacilityRequestTest extends TestCase
{
    public function test_the_facility_request_can_be_created(): void
    {
        $request = new FacilityRequest(123);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                'facility' => [
                    'id' => 123,
                    'name' => 'Test facility',
                    'published' => true,
                    'show_in_teaser' => false,
                    'icon' => 'facility-icon',
                    'as_filter' => true,
                    'accommodations' => 2,
                    'properties' => 3,
                    'href' => '/facilities/123.json',
                ],
            ]));

        $this->assertEquals('/facilities/123.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(FacilityResponse::class, $request->getResponse($httpResponse));
    }
}