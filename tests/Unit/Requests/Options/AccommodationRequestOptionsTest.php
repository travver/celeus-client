<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Options;

use Celeus\Requests\Options\AccommodationRequestOptions;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Options\AccommodationRequestOptions
 */
final class AccommodationRequestOptionsTest extends TestCase
{
    public function test_the_accommodation_request_options_can_be_created(): void
    {
        $options = new AccommodationRequestOptions(
            true
        );

        $this->assertEquals([
            'labels' => true,
        ], $options->getQueryParameters());
    }

    public function test_the_accommodation_request_options_will_return_an_empty_array_when_no_options_are_provided(): void
    {
        $options = new AccommodationRequestOptions();

        $this->assertEquals([], $options->getQueryParameters());
    }
}