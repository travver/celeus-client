<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\CityRequest;
use Celeus\Responses\CityResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\CityRequest
 * @covers \Celeus\Responses\CityResponse
 */
final class CityRequestTest extends TestCase
{
    public function test_the_city_request_can_be_created(): void
    {
        $request = new CityRequest(123);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                'city' => [
                    'id' => 123,
                    'name' => 'Test city',
                    'region_id' => 312,
                    'href' => '/cities/123.json',
                ],
                'region' => [
                    'id' => 312,
                    'name' => 'Test region',
                    'url' => 'https://example.org',
                    'country_id' => 4,
                    'administrative' => true,
                    'href' => '/regions/312.json',
                ],
                'country' => [
                    'id' => 4,
                    'name' => 'Test country',
                    'url' => 'https://example.org',
                    'list_order' => 2,
                    'code' => 'NL',
                    'href' => '/countries/4.json',
                ],
            ]));

        $this->assertEquals('/cities/123.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(CityResponse::class, $request->getResponse($httpResponse));
    }
}