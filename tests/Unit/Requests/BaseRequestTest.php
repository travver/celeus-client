<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Requests\BaseRequest;
use Tests\TestCase;

readonly class FakeRequest extends BaseRequest {}

/**
 * @covers \Celeus\Requests\BaseRequest
 */
final class BaseRequestTest extends TestCase
{
    public function test_the_request_returns_null_by_default_for_all_methods(): void
    {
        $request = new FakeRequest();

        $this->assertNull($request->getFilter());
        $this->assertNull($request->getPagination());
        $this->assertNull($request->getOptions());
    }
}