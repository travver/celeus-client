<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\ChainContractRequest;
use Celeus\Responses\ChainContractResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\ChainContractRequest
 * @covers \Celeus\Responses\ChainContractResponse
 */
final class ChainContractRequestTest extends TestCase
{
    public function test_the_chain_contract_request_can_be_created(): void
    {
        $request = new ChainContractRequest(123);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                'contract' => [
                    'id' => 123,
                    'commission' => [
                        'value' => 10,
                        'type' => 'percentage',
                        'vat' => 21,
                        'vat_included' => false,
                    ],
                    'cost_sharing' => [
                        'value' => 10,
                        'type' => 'value',
                        'vat' => 6,
                    ],
                    'fee_per_booking' => [
                        'value' => 19.95,
                        'type' => 'value',
                    ],
                    'target_bonus' => 'Test target bonus',
                    'payment_type' => 'by_invoice',
                    'debtor_number' => 'Test debtor number',
                    'vat_number' => 'Test VAT number',
                    'invoice' => [
                        'language' => 2,
                        'issue' => true,
                        'email' => 'test@example.org',
                        'address' => 'Test address',
                    ],
                    'contact_email' => 'test@example.org',
                    'bank' => [
                        'iban' => 'Test IBAN',
                        'name' => 'Test name',
                        'swift' => 'Test SWIFT',
                        'bic' => 'Test BIC',
                        'holder' => 'Test holder',
                    ],
                    'costs' => 'Test costs',
                    'arrival' => [
                        'from' => null,
                        'until' => null,
                    ],
                    'creation' => [
                        'from' => null,
                        'until' => null,
                    ],
                    'href' => '/chains/123/contract.json',
                ],
            ]));

        $this->assertEquals('/chains/123/contract.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(ChainContractResponse::class, $request->getResponse($httpResponse));
    }
}