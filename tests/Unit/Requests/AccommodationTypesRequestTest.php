<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\AccommodationTypesRequest;
use Celeus\Responses\AccommodationTypesResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\AccommodationTypesRequest
 * @covers \Celeus\Responses\AccommodationTypesResponse
 * @covers \Celeus\Collections\AccommodationTypeCollection
 */
final class AccommodationTypesRequestTest extends TestCase
{
    public function test_the_accommodation_types_request_can_be_created(): void
    {
        $request = new AccommodationTypesRequest();

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['types' => []]));

        $this->assertEquals('/accommodations/types.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(AccommodationTypesResponse::class, $request->getResponse($httpResponse));
    }
}