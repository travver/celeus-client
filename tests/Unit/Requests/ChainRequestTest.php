<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\ChainRequest;
use Celeus\Responses\ChainResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\ChainRequest
 * @covers \Celeus\Responses\ChainResponse
 */
final class ChainRequestTest extends TestCase
{
    public function test_the_chain_request_can_be_created(): void
    {
        $request = new ChainRequest(123);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                'chain' => [
                    'id' => 123,
                    'name' => 'Test name',
                    'url' => 'https://example.org',
                    'description' => 'Test description',
                    'ho_address' => 'Test address',
                    'ho_email' => 'test@example.org',
                    'ho_phone' => '1234567890',
                    'ho_language' => 1,
                    'notification_email' => 'test@example.org',
                    'wj_cards' => false,
                    'href' => '/chains/123.json',
                ],
            ]));

        $this->assertEquals('/chains/123.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(ChainResponse::class, $request->getResponse($httpResponse));
    }
}
