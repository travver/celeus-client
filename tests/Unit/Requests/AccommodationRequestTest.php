<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\AccommodationRequest;
use Celeus\Requests\Filters\AccommodationRequestFilter;
use Celeus\Requests\Options\AccommodationRequestOptions;
use Celeus\Responses\AccommodationResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\AccommodationRequest
 * @covers \Celeus\Responses\AccommodationResponse
 */
final class AccommodationRequestTest extends TestCase
{
    public function test_the_accommodation_request_can_be_created(): void
    {
        $request = new AccommodationRequest(
            123,
            new AccommodationRequestFilter(),
            new AccommodationRequestOptions()
        );

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                'accommodation' => [
                    'id' => 123,
                    'property_id' => 123,
                    'type_id' => 123,
                    'label' => 1,
                    'name' => 'Test',
                    'url' => 'test',
                    'description' => 'Test',
                    'persons_max' => 5,
                    'adults_min' => 5,
                    'adults_max' => 5,
                    'teenagers_max' => 3,
                    'children_max' => 3,
                    'babies_max' => 1,
                    'pets_max' => 2,
                    'bedrooms' => 3,
                    'surface' => 120,
                    'label_name' => 'Test',
                    'label_in_list' => 'Test',
                    'published' => true,
                    'deleted' => false,
                    'facilities' => [],
                    'href' => '/accommodations/123.json',
                ],
            ]));

        $this->assertEquals('/accommodations/123.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(AccommodationResponse::class, $request->getResponse($httpResponse));
        $this->assertEquals(new AccommodationRequestFilter(), $request->getFilter());
        $this->assertEquals(new AccommodationRequestOptions(), $request->getOptions());
    }
}