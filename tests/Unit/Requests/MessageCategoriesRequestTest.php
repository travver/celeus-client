<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\MessageCategoriesRequest;
use Celeus\Responses\MessageCategoriesResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\MessageCategoriesRequest
 * @covers \Celeus\Responses\MessageCategoriesResponse
 * @covers \Celeus\Collections\MessageCategoryCollection
 */
final class MessageCategoriesRequestTest extends TestCase
{
    public function test_the_message_categories_request_can_be_created(): void
    {
        $request = new MessageCategoriesRequest();

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['categories' => []]));

        $this->assertEquals('/messages/categories.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(MessageCategoriesResponse::class, $request->getResponse($httpResponse));
    }
}