<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\PropertyDiscountsRequest;
use Celeus\Responses\PropertyDiscountsResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\PropertyDiscountsRequest
 * @covers \Celeus\Responses\PropertyDiscountsResponse
 * @covers \Celeus\Collections\PropertyDiscountCollection
 */
final class PropertyDiscountsRequestTest extends TestCase
{
    public function test_the_property_discounts_request_can_be_created(): void
    {
        $request = new PropertyDiscountsRequest(123);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['discounts' => []]));

        $this->assertEquals('/properties/123/discounts.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(PropertyDiscountsResponse::class, $request->getResponse($httpResponse));
    }
}