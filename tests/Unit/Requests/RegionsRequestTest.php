<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Filters\RegionsRequestFilter;
use Celeus\Requests\RegionsRequest;
use Celeus\Responses\RegionsResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\RegionsRequest
 * @covers \Celeus\Requests\Filters\RegionsRequestFilter
 * @covers \Celeus\Responses\RegionsResponse
 */
final class RegionsRequestTest extends TestCase
{
    public function test_the_regions_request_can_be_created(): void
    {
        $filter = new RegionsRequestFilter([3, 5]);
        $request = new RegionsRequest($filter);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(
                json_encode([
                    'regions' => [
                        [
                            'id' => 54,
                            'country_id' => 8,
                            'name' => 'Achterhoek',
                            'url' => 'achterhoek',
                            'administrative' => false,
                            'href' => '/regions/54.json',
                            'country' => [
                                'id' => 8,
                                'name' => 'Nederland',
                                'href' => '/countries/8.json',
                            ],
                        ],
                    ],
                ])
            );

        $this->assertEquals([
            'country' => '3,5',
        ], $filter->getQueryParameters());
        $this->assertEquals(new RegionsRequestFilter([3, 5]), $request->getFilter());
        $this->assertEquals('/regions.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());

        $this->assertInstanceOf(RegionsResponse::class, $request->getResponse($httpResponse));
    }
}