<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Exceptions\UnprocessableEntityException;
use Celeus\Requests\Content\CountryRegionRequest;
use Celeus\Responses\Content\CountryRegionResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Content\CountryRegionRequest
 * @covers \Celeus\Responses\Content\CountryRegionResponse
 */
final class CountryRegionRequestTest extends TestCase
{
    public function test_the_country_region_request_can_be_created(): void
    {
        $request = new CountryRegionRequest('nl', 'friesland');

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getStatusCode')
            ->andReturn(200);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                'country' => [
                    'id' => 8,
                    'name' => 'Country',
                    'url' => 'country',
                    'list_order' => 3,
                    'code' => 'NL',
                    'href' => '/counties/8.json',
                ],
                'region' => [
                    'id' => 123,
                    'name' => 'Region',
                    'url' => 'rg',
                    'country_id' => 5,
                    'administrative' => true,
                    'href' => '/content/regions/123.json',
                ],
                'content' => [
                    'title' => 'Region content title',
                    'teaser' => 'This is teaser text for the region',
                    'description' => 'This is a description for the region',
                    'meta_keywords' => 'these, are, the, meta, keywords, for, the, region',
                    'meta_description' => 'This is a meta description for the region',
                    'page_title' => 'Region page title',
                    'list_order' => 3,
                    'as_category' => true,
                    'as_filter' => true,
                ]
            ]));

        $this->assertEquals('/content/regions/nl/friesland.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(CountryRegionResponse::class, $request->getResponse($httpResponse));
    }

    public function test_the_country_region_response_throws_unprocessable_entity_exception(): void
    {
        $this->expectException(UnprocessableEntityException::class);

        $request = new CountryRegionRequest('nl', 'friesland');

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getStatusCode')
            ->andReturn(422);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                "status" => 422,
                "message" => "Unprocessable Entity",
                "errors" => [
                    "url" => [
                        [
                            "code" => 3,
                            "message" => "Region not found"
                        ]
                    ]
                ]
            ]));

        $this->assertInstanceOf(CountryRegionResponse::class, $request->getResponse($httpResponse));
    }
}