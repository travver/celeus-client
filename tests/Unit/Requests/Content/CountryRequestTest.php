<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Content\CountryRequest;
use Celeus\Responses\Content\CountryResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Content\CountryRequest
 * @covers \Celeus\Responses\Content\CountryResponse
 */
final class CountryRequestTest extends TestCase
{
    public function test_the_country_request_can_be_created(): void
    {
        $request = new CountryRequest(123);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                'country' => [
                    'id' => 123,
                    'name' => 'Country',
                    'url' => 'ct',
                    'list_order' => 5,
                    'code' => 'CT',
                    'href' => '/countries/123.json',
                ],
                'content' => [
                    'id' => 456,
                    'title' => 'Country content title',
                    'teaser' => 'This is teaser text for the country',
                    'description' => 'This is a description for the country',
                    'meta_keywords' => 'these, are, the, meta, keywords, for, the, country',
                    'meta_description' => 'This is a meta description for the country',
                    'page_title' => 'Country page title',
                    'as_category' => true,
                    'as_filter' => true,
                ]
            ]));

        $this->assertEquals('/content/countries/123.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(CountryResponse::class, $request->getResponse($httpResponse));
    }
}