<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Content\TagRequest;
use Celeus\Requests\Filters\AccommodationRequestFilter;
use Celeus\Requests\Options\AccommodationRequestOptions;
use Celeus\Responses\AccommodationResponse;
use Celeus\Responses\Content\TagResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Content\TagRequest
 * @covers \Celeus\Responses\Content\TagResponse
 */
final class TagRequestTest extends TestCase
{
    public function test_the_tag_request_can_be_created(): void
    {
        $request = new TagRequest(
            3,
        );

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                'tag' => [
                    'id' => 3,
                    'title' => 'Stel of groep',
                    'url' => 'stel-of-groep',
                    'description' => '<h2 style="text-align: center;">Op stap zonder kids?</h2><p style="text-align: center;">Een overzicht van activireiten die wat minder geschikt zijn voor kleine kinderen</p>',
                    'meta_description' => 'Met een groep of als stel eropuit en genieten van de leuke activiteiten in de omgeving van het vakantiepark',
                    'meta_keywords' => 'stel, groep, activiteiten, bungalow, vakantiepark',
                    'page_title' => 'Stel of groep',
                    'as_category' => true,
                    'as_filter' => true,
                    'list_order' => 1,
                    'href' => '/content/tags/3.json',
                ],
            ]));

        $this->assertEquals('/content/tags/3.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(TagResponse::class, $request->getResponse($httpResponse));
    }
}