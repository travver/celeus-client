<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Exceptions\UnprocessableEntityException;
use Celeus\Requests\Content\ListingActivitiesRequest;
use Celeus\Requests\Pagination\Pagination;
use Celeus\Responses\Content\ListingActivitiesResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Content\ListingActivitiesRequest
 * @covers \Celeus\Responses\Content\ListingActivitiesResponse
 * @covers \Celeus\Collections\Content\ListingActivityCollection
 */
final class ListingActivityRequestTest extends TestCase
{
    public function test_the_listing_activity_request_can_be_created(): void
    {
        $request = new ListingActivitiesRequest();

        $this->assertEquals('/content/activities.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertNull($request->getFilter());
        $this->assertEquals(new Pagination(), $request->getPagination());

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getStatusCode')
            ->andReturn(200);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['activities' => []]));

        $httpResponse->shouldReceive('getHeader')
            ->with('X-Per-Page')
            ->andReturn([60]);

        $httpResponse->shouldReceive('getHeader')
            ->with('X-Total-Count')
            ->andReturn([12345]);

        $this->assertInstanceOf(ListingActivitiesResponse::class, $request->getResponse($httpResponse));
    }

    public function test_the_listing_activity_response_throws_unprocessable_entity_exception(): void
    {
        $this->expectException(UnprocessableEntityException::class);

        $request = new ListingActivitiesRequest();

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getStatusCode')
            ->andReturn(422);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                "status" => 422,
                "message" => "Unprocessable Entity",
                "errors" => [
                    "page" => [
                        [
                            "code" => "invalid",
                            "message" => "Invalid page number"
                        ]
                    ]
                ]
            ]));

        $this->assertInstanceOf(ListingActivitiesResponse::class, $request->getResponse($httpResponse));
    }
}