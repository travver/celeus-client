<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Content\ListingCountriesRequest;
use Celeus\Responses\Content\ListingCountriesResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Content\ListingCountriesRequest
 * @covers \Celeus\Responses\Content\ListingCountriesResponse
 * @covers \Celeus\Collections\Content\ListingCountryCollection
 */
final class ListingCountriesRequestTest extends TestCase
{
    public function test_the_listing_countries_request_can_be_created(): void
    {
        $request = new ListingCountriesRequest();

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['countries' => []]));

        $this->assertEquals('/content/countries.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(ListingCountriesResponse::class, $request->getResponse($httpResponse));
    }
}