<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Content\ActivityByCountryAndRegionRequest;
use Celeus\Responses\Content\ActivityResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Content\ActivityByCountryAndRegionRequest
 * @covers \Celeus\Requests\Content\Filters\ListingActivityRequestFilter
 * @covers \Celeus\Responses\Content\ActivityResponse
 * @covers \Celeus\Collections\Content\ImageCollection
 */
final class ActivityByCountryAndRegionRequestTest extends TestCase
{
    public function test_the_activity_request_can_be_created(): void
    {
        $request = new ActivityByCountryAndRegionRequest('nl', 'zeeland', 'zeekajakvaren');

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getStatusCode')
            ->andReturn(200);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                    'activity' => [
                    'id' => 238,
                    'title' => 'Adbij de Westerburcht',
                    'url' => 'adbij-de-westerburcht',
                    'usp' => 'Culinair genieten in een Abdij',
                    'teaser' => 'Abdij de Westerburcht is een ideale combinatie van een historische ambiance, streekgebonden producten en Drentse gastvrijheid.',
                    'description' => '<p>Naast heerlijke gerechten voor lunch en diner, is er een uitgebreide bierkaart passend bij de seizoensgerechten. Uiteraard kun je, passend bij de locatie, een trappistenbier bestellen. Ook is het mogelijk voor een High Tea, High Wine of High Beer te kiezen met hartige hapjes.</p>',
                    'source_url' => 'https://www.westerburcht.nl',
                    'source_url_text' => 'Website: Westerburcht',
                    'image' => '',
                    'image_title' => '',
                    'image_source' => '',
                    'address' => 'Abdij de Westerburcht Hoofdstraat 7 9431 AB Westerbork',
                    'meta_keywords' => 'lunch, diner, eten, drinken, abdij, culinair, bungalowpark, vakantiepar',
                    'meta_description' => 'High Tea, High Wine of High Beer in de Abdij de Westerburcht in Drenthe.',
                    'page_title' => 'Abdij de Westerburcht',
                    'icon' => 'silverware',
                    'latitude' => 52.8496507,
                    'longitude' => 6.606351,
                    'latitude_min' => 52.5801607,
                    'longitude_min' => 6.14221744,
                    'latitude_max' => 53.1191407,
                    'radius' => 30,
                    'region_id' => 15,
                    'published' => true,
                    'href' => '/content/activities/238.json',
                    'tags' => [
                        15, 2, 3
                    ],
                ],
                'region' => [
                    'id' => 15,
                    'name' => 'Drenthe',
                    'url' => 'drenthe',
                    'country_id' => 8,
                    'administrative' => true,
                    'href' => "/regions/15.json"
                ],
                'country' => [
                    'id' => 8,
                    'name' => 'Nederland',
                    'url' => 'nl',
                    'list_order' => 3,
                    'code' => 'NL',
                    'href' => '/countries/8.json'
                ],
                'images' => [
                    [
                        "id" => 31258,
                        "file" => "abbaye-de-fontenay-pzusr.jpg",
                        "image_info" => [
                            1200,
                            744,
                            2,
                            "width=\"1200\" height=\"744\"",
                            "bits" => 8,
                            "channels" => 3,
                            "mime" => "image/jpeg",
                        ],
                        "thumbnail_info" => [
                            "0" => 400,
                            "1" => 248,
                            "2" => 2,
                            "3" => "width=\"400\" height=\"248\"",
                            "bits" => 8,
                            "channels" => 3,
                            "mime" => "image/jpeg",
                        ],
                        "alt" => "Shutterstock",
                        "title" => "",
                        "is_primary" => true,
                        "focus_left" => 50,
                        "focus_top" => 50,
                        "list_order" => 1,
                        "relation_value" => 834,
                        "relation_type" => "contentactivity",
                        "published" => true,
                        "href" => "/images/31258.json",
                        "type" => [
                            "id" => 10,
                            "name" => "contentactivities",
                            "properties" => [
                                "directory" => "contentactivities",
                                "width" => 1200,
                                "height" => 1200,
                                "thumbnail" => true,
                                "thumbnail_width" => 400,
                                "thumbnail_height" => 400,
                            ],
                            "href" => "/images/types/10.json",
                        ],
                        "url" => [
                            "main" => "https://api.bnglw.bookunited.com/v1/files/images/contentactivities/abbaye-de-fontenay-pzusr.jpg",
                            "thumbnail" => "https://api.bnglw.bookunited.com/v1/files/images/contentactivities/th_abbaye-de-fontenay-pzusr.jpg",
                        ],
                    ],
                ]
            ]));

        $this->assertNull($request->getFilter());
        $this->assertEquals('/content/activities/nl/zeeland/zeekajakvaren.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(ActivityResponse::class, $request->getResponse($httpResponse));
    }
}