<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Content\Filters;

use Celeus\Requests\Content\Filters\ListingActivityRequestFilter;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Content\Filters\ListingActivityRequestFilter
 */
final class ListingActivityRequestFilterTest extends TestCase
{
    public function test_the_listing_activity_request_filter_can_be_created(): void
    {
        $filter = new ListingActivityRequestFilter(
            true,
            true,
        );

        $this->assertEquals([
            'nearby' => true,
            'images' => true,
        ], $filter->getQueryParameters());
    }

    public function test_the_listing_activity_request_filter_will_return_an_empty_array_when_no_filters_are_provided(): void
    {
        $filter = new ListingActivityRequestFilter();

        $this->assertEquals([], $filter->getQueryParameters());
    }
}