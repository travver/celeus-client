<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Content\Filters;

use Celeus\Requests\Content\Filters\ListingTagsRequestFilter;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Content\Filters\ListingTagsRequestFilter
 */
final class ListingTagsRequestFilterTest extends TestCase
{
    public function test_the_listing_tags_request_filter_can_be_created(): void
    {
        $filter = new ListingTagsRequestFilter(
            [1, 2, 3],
            ['cultuur', 'wadden'],
        );

        $this->assertEquals([
            'id' => '1,2,3',
            'url' => 'cultuur,wadden',
        ], $filter->getQueryParameters());
    }

    public function test_the_listing_tags_request_filter_will_return_an_empty_array_when_no_filters_are_provided(): void
    {
        $filter = new ListingTagsRequestFilter();

        $this->assertEquals([], $filter->getQueryParameters());
    }
}