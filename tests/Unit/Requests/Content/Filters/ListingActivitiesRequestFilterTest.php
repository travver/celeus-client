<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Content\Filters;

use Celeus\Requests\Content\Filters\ListingActivitiesRequestFilter;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Content\Filters\ListingActivitiesRequestFilter
 */
final class ListingActivitiesRequestFilterTest extends TestCase
{
    public function test_the_listing_activities_request_filter_can_be_created(): void
    {
        $filter = new ListingActivitiesRequestFilter(
            [1, 2, 3],
            [8],
            [24],
            '52.2176967',
            '5.9617204'
        );

        $this->assertEquals([
            'tag' => '1,2,3',
            'country' => '8',
            'region' => '24',
            'lat' => '52.2176967',
            'lng' => '5.9617204',
        ], $filter->getQueryParameters());
    }

    public function test_the_listing_activities_request_filter_will_return_an_empty_array_when_no_filters_are_provided(): void
    {
        $filter = new ListingActivitiesRequestFilter();

        $this->assertEquals([], $filter->getQueryParameters());
    }
}