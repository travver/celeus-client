<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Content;

use Celeus\Collections\Content\ListingTagCollection;
use Celeus\Enums\RequestMethod;
use Celeus\Requests\Content\ListingTagsRequest;
use Celeus\Requests\Pagination\Pagination;
use Celeus\Responses\Content\ListingTagsResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Content\ListingTagsRequest
 * @covers \Celeus\Responses\Content\ListingTagsResponse
 * @covers \Celeus\Collections\Content\ListingTagCollection
 */
final class ListingTagRequestTest extends TestCase
{
    public function test_the_listing_tags_request_can_be_created(): void
    {
        $request = new ListingTagsRequest();

        $this->assertEquals('/content/tags.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertNull($request->getFilter());
        $this->assertEquals(new Pagination(), $request->getPagination());

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['tags' => []]));

        $this->assertInstanceOf(ListingTagsResponse::class, $request->getResponse($httpResponse));
        $this->assertInstanceOf(ListingTagCollection::class, $request->getResponse($httpResponse)->tags);
    }
}