<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Content\RegionRequest;
use Celeus\Responses\Content\RegionResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Content\RegionRequest
 * @covers \Celeus\Responses\Content\RegionResponse
 */
final class RegionRequestTest extends TestCase
{
    public function test_the_region_request_can_be_created(): void
    {
        $request = new RegionRequest(123);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                'region' => [
                    'id' => 123,
                    'name' => 'Region',
                    'url' => 'rg',
                    'country_id' => 5,
                    'administrative' => true,
                    'href' => '/content/regions/123.json',
                ],
                'content' => [
                    'title' => 'Region content title',
                    'teaser' => 'This is teaser text for the region',
                    'description' => 'This is a description for the region',
                    'meta_keywords' => 'these, are, the, meta, keywords, for, the, region',
                    'meta_description' => 'This is a meta description for the region',
                    'page_title' => 'Region page title',
                    'list_order' => 3,
                    'as_category' => true,
                    'as_filter' => true,
                ]
            ]));

        $this->assertEquals('/content/regions/123.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(RegionResponse::class, $request->getResponse($httpResponse));
    }
}