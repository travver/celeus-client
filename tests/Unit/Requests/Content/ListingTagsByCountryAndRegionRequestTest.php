<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Content;

use Celeus\Collections\Content\ListingTagCollection;
use Celeus\Enums\RequestMethod;
use Celeus\Exceptions\UnprocessableEntityException;
use Celeus\Requests\Content\ListingTagsByCountryAndRegionRequest;
use Celeus\Responses\Content\ListingTagsByCountryAndRegionResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Content\ListingTagsByCountryAndRegionRequest
 * @covers \Celeus\Responses\Content\ListingTagsByCountryAndRegionResponse
 * @covers \Celeus\Collections\Content\ListingTagCollection
 */
final class ListingTagsByCountryAndRegionRequestTest extends TestCase
{
    public function test_the_listing_tags_by_country_and_region_request_can_be_created(): void
    {
        $request = new ListingTagsByCountryAndRegionRequest('nl', 'gelderland');

        $this->assertEquals('/content/regions/nl/gelderland/tags.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertNull($request->getFilter());

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getStatusCode')
            ->andReturn(200);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['tags' => []]));

        $this->assertInstanceOf(ListingTagsByCountryAndRegionResponse::class, $request->getResponse($httpResponse));
        $this->assertInstanceOf(ListingTagCollection::class, $request->getResponse($httpResponse)->listingTags);
    }

    public function test_the_listing_tags_by_country_and_region_response_throws_unprocessable_entity_exception(): void
    {
        $this->expectException(UnprocessableEntityException::class);

        $request = new ListingTagsByCountryAndRegionRequest('nl', 'gelderland');

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getStatusCode')
            ->andReturn(422);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                "status" => 422,
                "message" => "Unprocessable Entity",
                "errors" => [
                    "url" => [
                        [
                            "code" => 3,
                            "message" => "Region not found"
                        ]
                    ]
                ]
            ]));

        $this->assertInstanceOf(ListingTagsByCountryAndRegionResponse::class, $request->getResponse($httpResponse));
    }
}