<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Content\ListingRegionsRequest;
use Celeus\Responses\Content\ListingRegionsResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Content\ListingRegionsRequest
 * @covers \Celeus\Responses\Content\ListingRegionsResponse
 * @covers \Celeus\Collections\Content\ListingRegionCollection
 */
final class ListingRegionsRequestTest extends TestCase
{
    public function test_the_listing_countries_request_can_be_created(): void
    {
        $request = new ListingRegionsRequest(123);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['regions' => []]));

        $this->assertEquals('/content/countries/123/regions.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(ListingRegionsResponse::class, $request->getResponse($httpResponse));
    }
}