<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\AccommodationsRequest;
use Celeus\Requests\Filters\AccommodationsRequestFilter;
use Celeus\Responses\AccommodationsResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\AccommodationsRequest
 * @covers \Celeus\Responses\AccommodationsResponse
 * @covers \Celeus\Collections\AccommodationCollection
 */
final class AccommodationsRequestTest extends TestCase
{
    public function test_the_accommodations_request_can_be_created(): void
    {
        $request = new AccommodationsRequest(new AccommodationsRequestFilter());

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['accommodations' => []]));

        $httpResponse->shouldReceive('getHeader')
            ->with('X-Per-Page')
            ->andReturn([60]);

        $httpResponse->shouldReceive('getHeader')
            ->with('X-Total-Count')
            ->andReturn([12345]);

        $this->assertEquals('/accommodations.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertEquals(new AccommodationsRequestFilter(), $request->getFilter());
        $this->assertInstanceOf(AccommodationsResponse::class, $request->getResponse($httpResponse));
    }
}