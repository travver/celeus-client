<?php

declare(strict_types=1);

namespace Tests\Unit\Requests\Pagination;

use Celeus\Requests\Pagination\Pagination;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\Pagination\Pagination
 */
final class PaginationTest extends TestCase
{
    public function test_the_pagination_can_be_created(): void
    {
        $pagination = new Pagination(1, 10);

        $this->assertEquals(['page' => 1, 'per_page' => 10], $pagination->getQueryParameters());
    }

    public function test_the_pagination_returns_a_default_value_when_no_parameters_are_provided(): void
    {
        $pagination = new Pagination();

        $this->assertEquals(['page' => 1], $pagination->getQueryParameters());
    }
}
