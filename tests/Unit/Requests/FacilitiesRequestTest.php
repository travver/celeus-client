<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\FacilitiesRequest;
use Celeus\Requests\Filters\FacilityRequestFilter;
use Celeus\Responses\FacilitiesResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\FacilitiesRequest
 * @covers \Celeus\Responses\FacilitiesResponse
 * @covers \Celeus\Collections\FacilityCollection
 */
final class FacilitiesRequestTest extends TestCase
{
    public function test_the_facilities_request_can_be_created(): void
    {
        $request = new FacilitiesRequest(new FacilityRequestFilter());

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['facilities' => []]));

        $httpResponse->shouldReceive('getHeader')
            ->with('X-Per-Page')
            ->andReturn([60]);

        $httpResponse->shouldReceive('getHeader')
            ->with('X-Total-Count')
            ->andReturn([12345]);

        $this->assertEquals(new FacilityRequestFilter(), $request->getFilter());
        $this->assertEquals('/facilities.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());

        $this->assertInstanceOf(FacilitiesResponse::class, $request->getResponse($httpResponse));
    }
}