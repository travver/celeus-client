<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\LabelRequest;
use Celeus\Responses\LabelResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\LabelRequest
 * @covers \Celeus\Responses\LabelResponse
 */
final class LabelRequestTest extends TestCase
{
    public function test_the_label_request_can_be_created(): void
    {
        $request = new LabelRequest(123);

        $this->assertEquals('/labels/123.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                'label' => [
                    'id' => 123,
                    'name' => 'Test Label',
                    'name_list' => 'Test List',
                    'href' => '/labels/123.json',
                ],
            ]));

        $this->assertInstanceOf(LabelResponse::class, $request->getResponse($httpResponse));
    }
}