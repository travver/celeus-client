<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\ChainsRequest;
use Celeus\Responses\ChainsResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\ChainsRequest
 * @covers \Celeus\Responses\ChainsResponse
 * @covers \Celeus\Collections\ChainCollection
 */
final class ChainsRequestTest extends TestCase
{
    public function test_the_chains_request_can_be_created(): void
    {
        $request = new ChainsRequest();

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['chains' => []]));

        $this->assertEquals('/chains.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(ChainsResponse::class, $request->getResponse($httpResponse));
    }
}
