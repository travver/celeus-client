<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\PropertyMessagesRequest;
use Celeus\Responses\PropertyMessagesResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\PropertyMessagesRequest
 * @covers \Celeus\Responses\PropertyMessagesResponse
 * @covers \Celeus\Collections\PropertyMessageCollection
 */
final class PropertyMessagesRequestTest extends TestCase
{
    public function test_the_property_messages_request_can_be_created(): void
    {
        $request = new PropertyMessagesRequest(123);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['messages' => [], 'categories' => []]));

        $this->assertEquals('/properties/123/messages.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(PropertyMessagesResponse::class, $request->getResponse($httpResponse));
    }
}