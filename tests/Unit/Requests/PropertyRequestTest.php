<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\PropertyRequest;
use Celeus\Responses\PropertyResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\PropertyRequest
 * @covers \Celeus\Responses\PropertyResponse
 */
final class PropertyRequestTest extends TestCase
{
    public function test_the_property_request_can_be_created(): void
    {
        $request = new PropertyRequest(123);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                'property' => [
                    'id' => 123,
                    'chain_id' => 456,
                    'city_id' => 789,
                    'label' => 4,
                    'name' => 'Test name',
                    'name_sorting' => 'Test name sorting',
                    'name_letter' => 'T',
                    'url' => 'https://example.org',
                    'description' => 'Test description',
                    'good_to_know' => 'Test good to know',
                    'remarks_comments' => 'Test remarks comments',
                    'usp' => 'Test usp',
                    'address' => 'Test address',
                    'postalcode' => '1234AB',
                    'check_in' => '14:00:00',
                    'check_out' => '12:00:00',
                    'latitude' => 1.234,
                    'longitude' => 5.678,
                    'published' => true,
                    'label_name' => 'Test label name',
                    'label_in_list' => 'Test label in list',
                    'notification_email' => 'test@example.org',
                    'wj_cards' => true,
                    'facilities' => [1, 2, 3],
                    'href' => '/properties/123.json',
                ],
            ]));

        $this->assertEquals('/properties/123.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(PropertyResponse::class, $request->getResponse($httpResponse));
    }
}