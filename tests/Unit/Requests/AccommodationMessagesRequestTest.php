<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\AccommodationMessagesRequest;
use Celeus\Responses\AccommodationMessagesResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\AccommodationMessagesRequest
 * @covers \Celeus\Responses\AccommodationMessagesResponse
 * @covers \Celeus\Collections\AccommodationMessageCollection
 */
final class AccommodationMessagesRequestTest extends TestCase
{
    public function test_the_accommodation_messages_request_can_be_created(): void
    {
        $request = new AccommodationMessagesRequest(123);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['categories' => [], 'messages' => []]));

        $this->assertEquals('/accommodations/123/messages.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(AccommodationMessagesResponse::class, $request->getResponse($httpResponse));
    }
}