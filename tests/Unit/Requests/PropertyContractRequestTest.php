<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\PropertyContractRequest;
use Celeus\Responses\PropertyContractResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\PropertyContractRequest
 * @covers \Celeus\Responses\PropertyContractResponse
 */
final class PropertyContractRequestTest extends TestCase
{
    public function test_the_property_contract_request_can_be_created(): void
    {
        $request = new PropertyContractRequest(123);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                'contract' => [
                    'commission' => [
                        'value' => 10,
                        'type' => 'percentage',
                        'vat' => 21,
                        'vat_included' => false,
                    ],
                    'cost_sharing' => [
                        'value' => 10,
                        'type' => 'value',
                        'vat' => 6,
                    ],
                    'fee_per_booking' => [
                        'value' => 19.95,
                        'type' => 'value',
                    ],
                    'target_bonus' => 'Test target bonus',
                    'payment_type' => 'at_arrival',
                    'debtor_number' => 'Test debtor number',
                    'vat_number' => 'Test VAT number',
                    'invoice' => [
                        'language' => 2,
                        'issue' => true,
                        'email' => 'test@example.org',
                        'address' => 'Test address',
                    ],
                    'contact_email' => 'test@example.org',
                    'bank' => [
                        'iban' => 'Test IBAN',
                        'name' => 'Test name',
                        'swift' => 'Test SWIFT',
                        'bic' => 'Test BIC',
                        'holder' => 'Test holder',
                    ],
                    'costs' => 'Test costs',
                    'arrival' => [
                        'from' => '2020-01-01',
                        'until' => '2020-12-31',
                    ],
                    'creation' => [
                        'from' => '2023-01-01',
                        'until' => '2023-12-31',
                    ],
                ],
            ]));

        $this->assertEquals('/properties/123/contract.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(PropertyContractResponse::class, $request->getResponse($httpResponse));
    }
}