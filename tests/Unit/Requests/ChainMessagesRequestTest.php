<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\ChainMessagesRequest;
use Celeus\Responses\ChainMessageResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\ChainMessagesRequest
 * @covers \Celeus\Responses\ChainMessageResponse
 * @covers \Celeus\Collections\ChainMessageCollection
 */
final class ChainMessagesRequestTest extends TestCase
{
    public function test_the_chain_messages_request_can_be_created(): void
    {
        $request = new ChainMessagesRequest(123);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['messages' => [], 'categories' => []]));

        $this->assertEquals('/chains/123/messages.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(ChainMessageResponse::class, $request->getResponse($httpResponse));
    }
}