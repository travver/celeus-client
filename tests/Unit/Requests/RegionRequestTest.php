<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\RegionRequest;
use Celeus\Responses\RegionResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\RegionRequest
 * @covers \Celeus\Responses\RegionResponse
 * @covers \Celeus\Collections\CityCollection
 */
final class RegionRequestTest extends TestCase
{
    public function test_the_region_request_can_be_created(): void
    {
        $request = new RegionRequest(123);

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode([
                'region' => [
                    'id' => 123,
                    'name' => 'Test region',
                    'url' => 'https://example.org',
                    'country_id' => 4,
                    'administrative' => false,
                    'href' => '/regions/123.json',
                ],
                'country' => [
                    'id' => 4,
                    'name' => 'Test country',
                    'url' => 'https://example.org',
                    'list_order' => 2,
                    'code' => 'NL',
                    'href' => '/countries/4.json',
                ],
                'cities' => [],
            ]));

        $this->assertEquals('/regions/123.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertInstanceOf(RegionResponse::class, $request->getResponse($httpResponse));
    }
}