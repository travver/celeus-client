<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\FacilitiesCategoryRequest;
use Celeus\Requests\Pagination\Pagination;
use Celeus\Responses\FacilitiesCategoryResponse;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @covers \Celeus\Requests\FacilitiesCategoryRequest
 * @covers \Celeus\Responses\FacilitiesCategoryResponse
 * @covers \Celeus\Collections\FacilitiesCategoryCollection
 */
final class FacilitiesCategoriesRequestTest extends TestCase
{
    public function test_the_facilities_request_can_be_created(): void
    {
        $request = new FacilitiesCategoryRequest();

        $httpResponse = Mockery::mock(ResponseInterface::class);
        $httpResponse->shouldReceive('getBody->getContents')
            ->andReturn(json_encode(['categories' => []]));

        $httpResponse->shouldReceive('getHeader')
            ->with('X-Per-Page')
            ->andReturn([60]);

        $httpResponse->shouldReceive('getHeader')
            ->with('X-Total-Count')
            ->andReturn([12345]);

        $this->assertEquals('/facilities/categories.json', $request->getEndpoint());
        $this->assertEquals(RequestMethod::GET, $request->getMethod());
        $this->assertEquals(new Pagination(), $request->getPagination());

        $this->assertInstanceOf(FacilitiesCategoryResponse::class, $request->getResponse($httpResponse));
    }
}