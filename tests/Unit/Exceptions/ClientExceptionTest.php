<?php

declare(strict_types=1);

namespace Tests\Unit\Exceptions;

use InvalidArgumentException;
use Tests\Fakes\FakeException;
use Tests\TestCase;

/**
 * @covers \Celeus\Exceptions\ClientException
*/
final class ClientExceptionTest extends TestCase
{
    public function test_the_authentication_exception_can_be_created_from_a_multi_error_array(): void
    {
        $exception = FakeException::fromArray([
            'errors' => [
                'field' => [
                    [
                        'message' => 'Example error description'
                    ]
                ]
            ]
        ]);

        $this->assertEquals('Example error description', $exception->getMessage());
    }

    public function test_when_the_response_structure_is_not_recognized_the_json_string_is_returned(): void
    {
        $exception = FakeException::fromArray([
            'unknown' => [
                'json' => 'structure',
            ],
        ]);

        $this->assertEquals('{"unknown":{"json":"structure"}}', $exception->getMessage());
    }

    public function test_an_invalid_argument_exception_is_thrown_when_no_array_is_provided(): void
    {
        $this->expectExceptionObject(
            new InvalidArgumentException('$data should be an array, boolean given'),
        );

        FakeException::fromArray(false);
    }
}
