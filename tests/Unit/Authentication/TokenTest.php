<?php

declare(strict_types=1);

namespace Tests\Unit\Authentication;

use Celeus\Authentication\Token;
use DateTimeImmutable;
use Tests\Fakes\ClockFake;
use Tests\TestCase;

/**
 * @covers \Celeus\Authentication\Token
 */
final class TokenTest extends TestCase
{
    public function test_a_token_can_be_constructed_from_an_array(): void
    {
        $token = Token::fromArray(
            new ClockFake('2021-01-01 00:00:00'),
            [
                'access_token' => 'test-token',
                'expires_in' => 3600,
                'permissions' => ['test-permission1', 'test-permission2'],
            ]
        );

        $this->assertEquals('test-token', $token->token);
        $this->assertEquals(new DateTimeImmutable('2021-01-01 01:00:00'), $token->expiresAt);
        $this->assertEquals(['test-permission1', 'test-permission2'], $token->permissions);
        $this->assertFalse($token->verified);
    }

    public function test_a_token_can_be_verified(): void
    {
        $token = Token::fromArray(
            new ClockFake('2021-01-01 00:00:00'),
            [
                'access_token' => 'test-token',
                'expires_in' => 3600,
                'permissions' => ['test-permission1', 'test-permission2'],
            ]
        );

        $this->assertFalse($token->verified);

        $token = $token->verify();

        $this->assertTrue($token->verified);
    }
}