<?php

declare(strict_types=1);

namespace Tests\Fakes;

use DateTimeImmutable;
use Exception;
use Psr\Clock\ClockInterface;

final readonly class ClockFake implements ClockInterface
{
    public function __construct(
        private string $dateString,
    ) {}

    /** @throws Exception */
    public function now(): DateTimeImmutable
    {
        return new DateTimeImmutable($this->dateString);
    }
}