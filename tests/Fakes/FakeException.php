<?php

declare(strict_types=1);

namespace Tests\Fakes;

use Celeus\Exceptions\ClientException;

final class FakeException extends ClientException {}
