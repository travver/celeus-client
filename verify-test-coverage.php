<?php

declare(strict_types=1);

if (!file_exists(__DIR__ . '/.phpunit.coverage/xml/index.xml')) {
    echo "Coverage XML file not found.\n";
    exit(1);
}

$coverage = simplexml_load_file(__DIR__ . '/.phpunit.coverage/xml/index.xml');

$percentage = (float)$coverage->project->directory->totals->lines['percent'];

if ($percentage < 100) {
    echo sprintf("Test code coverage is not 100%% (only %s%% line coverage).\n", $percentage);
    exit(1);
}