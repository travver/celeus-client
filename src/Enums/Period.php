<?php

declare(strict_types=1);

namespace Celeus\Enums;

enum Period: string
{
    case MIDWEEK = 'midweek';
    case WEEKEND = 'weekend';
    case WEEK = 'week';
}
