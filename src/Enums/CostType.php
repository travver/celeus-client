<?php

declare(strict_types=1);

namespace Celeus\Enums;

enum CostType: string
{
    case PERCENTAGE = 'percentage';
    case VALUE = 'value';
}
