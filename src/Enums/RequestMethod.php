<?php

declare(strict_types=1);

namespace Celeus\Enums;

enum RequestMethod: string
{
    case GET = 'GET';
    case POST = 'POST';
    case PUT = 'PUT';
    case OPTION = 'OPTION';
    case DELETE = 'DELETE';
}
