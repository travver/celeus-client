<?php

declare(strict_types=1);

namespace Celeus\Enums;

enum DiscountRestrictionType: string
{
    case YOUNGEST = 'youngest';
    case OLDEST = 'oldest';
    case EBD = 'ebd';
}
