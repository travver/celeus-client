<?php

declare(strict_types=1);

namespace Celeus\Enums;

enum CalculationBase: string
{
    case FULL = 'full';
    case DISCOUNTED = 'discounted';
}
