<?php

declare(strict_types=1);

namespace Celeus\Enums;

enum RelationType: string
{
    case ACCOMMODATION = 'accommodation';
    case CARD = 'card';
    case CATCH = 'catch';
    case CHAIN = 'chain';
    case CONTENT_ACTIVITY = 'contentactivity';
    case EMAIL_LAYOUT = 'email_layout';
    case EMAIL_TEMPLATE = 'email_template';
    case KNOWLEDGE = 'knowledge';
    case OFFER = 'offer';
    case PROPERTY = 'property';
}
