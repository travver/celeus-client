<?php

declare(strict_types=1);

namespace Celeus\Enums;

enum PaymentType: string
{
    case TO_BOOKIT = 'to_bookit';
    case AT_ARRIVAL = 'at_arrival';
    case BY_INVOICE = 'by_invoice';
}
