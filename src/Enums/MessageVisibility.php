<?php

declare(strict_types=1);

namespace Celeus\Enums;

enum MessageVisibility: string
{
    case ALERTBOX = 'alertbox';
    case BOOKING_CONFIRMATION = 'booking_confirmation';
    case CALLCENTER = 'callcenter';
    case NICE_TRIP_EMAIL = 'nice_trip_email';
    case REMARKS = 'remarks';
    case WEBSITE = 'website';
}
