<?php

declare(strict_types=1);

namespace Celeus\Authentication;

use DateInterval;
use DateTimeImmutable;
use Psr\Clock\ClockInterface;

final readonly class Token
{
    /**
     * @param string[] $permissions {
     */
    public function __construct(
        public string $token,
        public DateTimeImmutable $expiresAt,
        public array $permissions,
        public bool $verified,
    ) {
    }

    public static function fromArray(ClockInterface $clock, array $data): self
    {
        return new self(
            $data['access_token'],
            $clock->now()->add(new DateInterval('PT' . $data['expires_in'] . 'S')),
            $data['permissions'],
            false,
        );
    }

    public function verify(): self
    {
        return new self(
            $this->token,
            $this->expiresAt,
            $this->permissions,
            true,
        );
    }

}
