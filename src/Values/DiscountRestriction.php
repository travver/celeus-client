<?php

declare(strict_types=1);

namespace Celeus\Values;

use Celeus\Enums\DiscountRestrictionType;

final readonly class DiscountRestriction
{
    public function __construct(
        public DiscountRestrictionType $type,
        public int $limit,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            DiscountRestrictionType::from($data['type']),
            $data['limit'],
        );
    }
}
