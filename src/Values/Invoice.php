<?php

declare(strict_types=1);

namespace Celeus\Values;

use InvalidArgumentException;

final readonly class Invoice
{
    public function __construct(
        public int $languageId,
        public bool $issue,
        public string $email,
        public string $address,
    ) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            throw new InvalidArgumentException('The email address is not valid.');
        }
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['language'],
            $data['issue'],
            $data['email'],
            $data['address'],
        );
    }
}
