<?php

declare(strict_types=1);

namespace Celeus\Values;

final readonly class ImageUrl
{
    public function __construct(
        public string $main,
        public ?string $thumbnail,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['main'],
            $data['thumbnail'] ?? null,
        );
    }
}
