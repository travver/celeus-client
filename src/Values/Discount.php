<?php

declare(strict_types=1);

namespace Celeus\Values;

use Celeus\Enums\CalculationBase;
use Celeus\Enums\CostType;

final readonly class Discount
{
    public function __construct(
        public CostType $type,
        public CalculationBase $base,
        public float $value,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            CostType::from($data['type']),
            CalculationBase::from($data['base']),
            $data['value'],
        );
    }
}
