<?php

declare(strict_types=1);

namespace Celeus\Values;

final readonly class Bank
{
    public function __construct(
        public string $iban,
        public string $name,
        public string $swift,
        public string $bic,
        public string $holder,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['iban'],
            $data['name'],
            $data['swift'],
            $data['bic'],
            $data['holder']
        );
    }
}
