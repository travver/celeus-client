<?php

declare(strict_types=1);

namespace Celeus\Values;

use Celeus\Exceptions\InvalidPeriodException;
use Celeus\Utilities\DateParser;
use DateTimeImmutable;

final readonly class DatePeriod
{
    public function __construct(
        public ?DateTimeImmutable $from,
        public ?DateTimeImmutable $until,
    ) {
        if ($from !== null && $until !== null && $from > $until) {
            throw new InvalidPeriodException('The start date of a period can not be after the end date.');
        }
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['from'] ? DateParser::parse($data['from'])->setTime(0, 0) : null,
            $data['until'] ? DateParser::parse($data['until'])->setTime(0, 0) : null,
        );
    }
}
