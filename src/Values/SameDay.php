<?php

declare(strict_types=1);

namespace Celeus\Values;

final readonly class SameDay
{
    public function __construct(
        public ?string $confirmation,
        public ?string $stop,
    ) {
    }

    public static function fromArray(?array $data): ?self
    {
        if ($data === null) {
            return null;
        }

        return new self(
            $data['confirmation'],
            $data['stop'],
        );
    }
}
