<?php

declare(strict_types=1);

namespace Celeus\Values;

use Celeus\Enums\CostType;
use InvalidArgumentException;

final readonly class CostSharing
{
    public function __construct(
        public float $value,
        public CostType $type,
        public float $vat,
    ) {
        if ($vat < 0) {
            throw new InvalidArgumentException('VAT percentage can not be negative.');
        }
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['value'],
            CostType::from($data['type']),
            $data['vat'],
        );
    }
}
