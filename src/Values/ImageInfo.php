<?php

declare(strict_types=1);

namespace Celeus\Values;

use InvalidArgumentException;

final readonly class ImageInfo
{
    public function __construct(
        public int $width,
        public int $height,
        public int $imageType,
        public string $widthHeightString,
        public int $bits,
        public ?int $channels,
        public string $mime,
    ) {
        if ($width <= 0) {
            throw new InvalidArgumentException('The image width must be greater than zero.');
        }

        if ($height <= 0) {
            throw new InvalidArgumentException('The image height must be greater than zero.');
        }
    }

    public static function fromArray(?array $data): ?self
    {
        if ($data === null) {
            return null;
        }

        return new self(
            $data['0'],
            $data['1'],
            $data['2'],
            $data['3'],
            $data['bits'],
            array_key_exists('channels', $data) ? $data['channels'] : null,
            $data['mime'],
        );
    }
}
