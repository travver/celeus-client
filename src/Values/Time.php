<?php

declare(strict_types=1);

namespace Celeus\Values;

use InvalidArgumentException;

final readonly class Time
{
    public function __construct(
        public int $hour,
        public int $minute,
        public int $second
    ) {
        if ($hour < 0 || $hour > 23) {
            throw new InvalidArgumentException('Invalid time provided.');
        }

        if ($minute < 0 || $minute > 59) {
            throw new InvalidArgumentException('Invalid time provided.');
        }

        if ($second < 0 || $second > 59) {
            throw new InvalidArgumentException('Invalid time provided.');
        }
    }

    public static function fromString(string $time): self
    {
        [$hour, $minute, $second] = explode(':', $time);

        return new self(
            (int)$hour,
            (int)$minute,
            (int)$second
        );
    }

    public function __toString(): string
    {
        return sprintf('%02d:%02d:%02d', $this->hour, $this->minute, $this->second);
    }
}
