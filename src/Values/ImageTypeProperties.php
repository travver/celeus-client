<?php

declare(strict_types=1);

namespace Celeus\Values;

use InvalidArgumentException;

final readonly class ImageTypeProperties
{
    public function __construct(
        public ?string $directory,
        public int $width,
        public int $height,
        public bool $thumbnail,
        public int $thumbnailWidth,
        public int $thumbnailHeight,
    ) {
        if ($width <= 0) {
            throw new InvalidArgumentException('The image width must be greater than zero.');
        }

        if ($height <= 0) {
            throw new InvalidArgumentException('The image height must be greater than zero.');
        }

        if ($thumbnailWidth <= 0) {
            throw new InvalidArgumentException('The image thumbnail width must be greater than zero.');
        }

        if ($thumbnailHeight <= 0) {
            throw new InvalidArgumentException('The image thumbnail height must be greater than zero.');
        }
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['directory'],
            $data['width'],
            $data['height'],
            $data['thumbnail'],
            $data['thumbnail_width'],
            $data['thumbnail_height'],
        );
    }
}
