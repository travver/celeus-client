<?php

declare(strict_types=1);

namespace Celeus\Values;

use Celeus\Enums\CostType;

final readonly class FeePerBooking
{
    public function __construct(
        public float $value,
        public CostType $type,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['value'],
            CostType::from($data['type']),
        );
    }
}
