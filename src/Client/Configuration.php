<?php

declare(strict_types=1);

namespace Celeus\Client;

use Celeus\Authentication\Token;
use InvalidArgumentException;

final readonly class Configuration
{
    public string $endpoint;

    public function __construct(
        string $endpoint,
        public ?Token $token = null,
    ) {
        $this->validateEndpoint($endpoint);

        $this->endpoint = $this->sanitizeEndpoint($endpoint);
    }

    private function validateEndpoint(string $endpoint): void
    {
        if (!is_string(parse_url($endpoint, PHP_URL_SCHEME)) || !is_string(parse_url($endpoint, PHP_URL_HOST))) {
            throw new InvalidArgumentException('Invalid endpoint');
        }
    }

    private function sanitizeEndpoint(string $endpoint): string
    {
        return trim($endpoint, '/');
    }
}
