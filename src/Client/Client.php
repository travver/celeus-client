<?php

declare(strict_types=1);

namespace Celeus\Client;

use Celeus\Authentication\Token;
use Celeus\Enums\RequestMethod;
use Celeus\Requests\Request;
use Celeus\Responses\Response;
use Psr\Clock\ClockInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use RuntimeException;

final readonly class Client
{
    public function __construct(
        private ClientInterface $httpClient,
        private RequestFactoryInterface $requestFactory,
        private StreamFactoryInterface $streamFactory,
        private Configuration $configuration,
        private ClockInterface $clock,
    ) {
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function acquireToken(string $username, string $password): Token
    {
        $httpRequest = $this->requestFactory
            ->createRequest(
                RequestMethod::GET->value,
                $this->configuration->endpoint . '/token'
            )
            ->withHeader('Authorization', 'Basic ' . base64_encode($username . ':' . $password));

        $response = $this->httpClient->sendRequest($httpRequest);

        return Token::fromArray(
            $this->clock,
            (array)json_decode($response->getBody()->getContents())
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function verifyToken(string $pinCode): Token | false
    {
        if ($this->configuration->token === null) {
            throw new RuntimeException('No token provided for verification');
        }

        $httpRequest = $this->requestFactory
            ->createRequest(
                RequestMethod::POST->value,
                $this->configuration->endpoint . '/token/verify'
            )
            ->withBody(
                $this->streamFactory->createStream(
                    (string)json_encode([
                        'token' => $this->configuration->token->token,
                        'pin' => $pinCode,
                    ])
                )
            );

        $response = $this->httpClient->sendRequest($httpRequest);

        if ($response->getStatusCode() === 204) {
            return $this->configuration->token->verify();
        }

        return false;
    }

    /**
     * @template T of Response
     * @param Request<T> $request
     * @return T
     * @throws ClientExceptionInterface
     */
    public function get(Request $request): Response
    {
        if ($this->configuration->token === null) {
            throw new RuntimeException('No token provided');
        }

        $httpRequest = $this->requestFactory
            ->createRequest(
                $request->getMethod()->value,
                $this->buildEndpoint($request),
            )
            ->withHeader('Authorization', 'Bearer ' . $this->configuration->token->token);

        $response = $this->httpClient->sendRequest($httpRequest);

        // @todo: Handle other status codes
        if ($response->getStatusCode() === 404) {
            throw new RuntimeException('Not found', 404);
        }

        if ($response->getStatusCode() === 401) {
            throw new RuntimeException('Unauthorized', 401);
        }

        return $request->getResponse($response);
    }

    /**
     * @param Request<Response> $request
     */
    private function buildEndpoint(Request $request): string
    {
        $endpoint = $this->configuration->endpoint . $request->getEndpoint();

        $parameters = array_merge(
            $request->getFilter()?->getQueryParameters() ?? [],
            $request->getOptions()?->getQueryParameters() ?? [],
            $request->getPagination()?->getQueryParameters() ?? [],
        );

        if (count($parameters) === 0) {
            return $endpoint;
        }

        return $endpoint . '?' . http_build_query($parameters);
    }
}
