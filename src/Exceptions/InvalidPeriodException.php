<?php

declare(strict_types=1);

namespace Celeus\Exceptions;

use RuntimeException;

final class InvalidPeriodException extends RuntimeException
{
}
