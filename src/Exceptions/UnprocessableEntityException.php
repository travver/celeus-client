<?php

declare(strict_types=1);

namespace Celeus\Exceptions;

final class UnprocessableEntityException extends ClientException {}
