<?php

declare(strict_types=1);

namespace Celeus\Exceptions;

final class UnknownException extends ClientException {}
