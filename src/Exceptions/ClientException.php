<?php

declare(strict_types=1);

namespace Celeus\Exceptions;

use Illuminate\Support\Arr;
use InvalidArgumentException;
use RuntimeException;
use Throwable;

abstract class ClientException extends RuntimeException
{
    final public function __construct(string $message = '', int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function fromArray(mixed $data): static
    {
        if (!is_array($data)) {
            throw new InvalidArgumentException('$data should be an array, ' . gettype($data) . ' given');
        }

        if (array_key_exists('errors', $data)) {
            /** @phpstan-ignore-next-line */
            return new static(Arr::first($data['errors'])[0]['message']);
        }

        /** @phpstan-ignore-next-line */
        return new static(json_encode($data));
    }
}
