<?php

declare(strict_types=1);

namespace Celeus\Entities;

final readonly class Source
{
    public function __construct(
        public int $id,
        public string $name,
    ) {
    }

    public static function fromArray(?array $data): ?self
    {
        if($data === null) {
            return null;
        }

        return new self(
            $data['id'],
            $data['name'],
        );
    }
}
