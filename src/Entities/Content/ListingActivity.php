<?php

declare(strict_types=1);

namespace Celeus\Entities\Content;

final readonly class ListingActivity
{
    public function __construct(
        public int $id,
        public string $title,
        public bool $published,
        public ListingRegion $region,
        public ListingCountry $country,
        public string $href,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['title'],
            $data['published'],
            ListingRegion::fromArray($data['region']),
            ListingCountry::fromArray($data['country']),
            $data['href'],
        );
    }
}
