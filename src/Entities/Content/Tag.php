<?php

declare(strict_types=1);

namespace Celeus\Entities\Content;

final readonly class Tag
{
    public function __construct(
        public int $id,
        public string $title,
        public string $url,
        public string $description,
        public string $metaDescription,
        public string $metaKeywords,
        public string $pageTitle,
        public bool $asCategory,
        public bool $asFilter,
        public int $listOrder,
        public string $href,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['title'],
            $data['url'],
            $data['description'],
            $data['meta_description'],
            $data['meta_keywords'],
            $data['page_title'],
            $data['as_category'],
            $data['as_filter'],
            $data['list_order'],
            $data['href'],
        );
    }
}
