<?php

declare(strict_types=1);

namespace Celeus\Entities\Content;

final readonly class Activity
{
    /**
     * @param array<int, int> $tags
     */
    public function __construct(
        public int $id,
        public string $title,
        public string $url,
        public string $usp,
        public string $teaser,
        public string $description,
        public string $sourceUrl,
        public string $sourceUrlText,
        public string $image,
        public string $imageTitle,
        public string $imageSource,
        public string $address,
        public string $metaKeywords,
        public string $metaDescription,
        public string $pageTitle,
        public ?string $icon,
        public float $latitude,
        public float $longitude,
        public float $latitudeMin,
        public float $longitudeMin,
        public float $latitudeMax,
        public int $radius,
        public int $regionId,
        public bool $published,
        public string $href,
        public array $tags,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['title'],
            $data['url'],
            $data['usp'],
            $data['teaser'],
            $data['description'],
            $data['source_url'],
            $data['source_url_text'],
            $data['image'],
            $data['image_title'],
            $data['image_source'],
            $data['address'],
            $data['meta_keywords'],
            $data['meta_description'],
            $data['page_title'],
            $data['icon'],
            $data['latitude'],
            $data['longitude'],
            $data['latitude_min'],
            $data['longitude_min'],
            $data['latitude_max'],
            $data['radius'],
            $data['region_id'],
            $data['published'],
            $data['href'],
            $data['tags'],
        );
    }
}
