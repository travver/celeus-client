<?php

declare(strict_types=1);

namespace Celeus\Entities\Content;

final readonly class Country
{
    public function __construct(
        public int $id,
        public string $name,
        public string $url,
        public int $listOrder,
        public string $code,
        public string $href,
        public ?int $contentId,
        public string $title,
        public string $teaser,
        public string $description,
        public string $metaKeywords,
        public string $metaDescription,
        public string $pageTitle,
        public bool $asCategory,
        public bool $asFilter,
    ) {
    }

    public static function fromArray(array $data, array $content): self
    {
        return new self(
            $data['id'],
            $data['name'],
            $data['url'],
            $data['list_order'],
            $data['code'],
            $data['href'],
            $content['id'] ?? null,
            $content['title'],
            $content['teaser'],
            $content['description'],
            $content['meta_keywords'],
            $content['meta_description'],
            $content['page_title'],
            $content['as_category'],
            $content['as_filter'],
        );
    }
}
