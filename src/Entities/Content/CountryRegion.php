<?php

declare(strict_types=1);

namespace Celeus\Entities\Content;

final readonly class CountryRegion
{
    public function __construct(
        public int $countryId,
        public string $countryName,
        public string $countryUrl,
        public int $countryListOrder,
        public string $countryCode,
        public string $countryHref,
        public int $regionId,
        public string $regionName,
        public string $regionUrl,
        public bool $regionAdministrative,
        public string $regionHref,
        public string $title,
        public string $teaser,
        public string $description,
        public string $metaKeywords,
        public string $metaDescription,
        public string $pageTitle,
        public int $listOrder,
        public bool $asCategory,
        public bool $asFilter,
    ) {
    }

    public static function fromArray(array $country, array $region, array $content): self
    {
        return new self(
            $country['id'],
            $country['name'],
            $country['url'],
            $country['list_order'],
            $country['code'],
            $country['href'],
            $region['id'],
            $region['name'],
            $region['url'],
            $region['administrative'],
            $region['href'],
            $content['title'],
            $content['teaser'],
            $content['description'],
            $content['meta_keywords'],
            $content['meta_description'],
            $content['page_title'],
            $content['list_order'],
            $content['as_category'],
            $content['as_filter'],
        );
    }
}
