<?php

declare(strict_types=1);

namespace Celeus\Entities\Content;

final readonly class ListingTag
{
    public function __construct(
        public int $id,
        public string $title,
        public string $url,
        public bool $asCategory,
        public bool $asFilter,
        public int $listOrder,
        public int $activities,
        public string $href,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['title'],
            array_key_exists('url', $data) ? $data['url'] : '',
            $data['as_category'],
            $data['as_filter'],
            $data['list_order'],
            $data['activities'],
            $data['href'],
        );
    }
}
