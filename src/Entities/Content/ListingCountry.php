<?php

declare(strict_types=1);

namespace Celeus\Entities\Content;

final readonly class ListingCountry
{
    public function __construct(
        public int $id,
        public string $name,
        public string $url,
        public ?int $contentId,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['name'],
            array_key_exists('url', $data) ? $data['url'] : '',
            $data['content_id'] ?? null,
        );
    }
}
