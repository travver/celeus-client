<?php

declare(strict_types=1);

namespace Celeus\Entities\Content;

final readonly class Region
{
    public function __construct(
        public int $id,
        public string $name,
        public string $url,
        public int $countryId,
        public bool $administrative,
        public string $href,
        public string $title,
        public string $teaser,
        public string $description,
        public string $metaKeywords,
        public string $metaDescription,
        public string $pageTitle,
        public int $listOrder,
        public bool $asCategory,
        public bool $asFilter,
    ) {
    }

    public static function fromArray(array $data, array $content): self
    {
        return new self(
            $data['id'],
            $data['name'],
            $data['url'],
            $data['country_id'],
            $data['administrative'],
            $data['href'],
            $content['title'],
            $content['teaser'],
            $content['description'],
            $content['meta_keywords'],
            $content['meta_description'],
            $content['page_title'],
            $content['list_order'],
            $content['as_category'],
            $content['as_filter'],
        );
    }
}
