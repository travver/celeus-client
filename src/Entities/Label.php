<?php

declare(strict_types=1);

namespace Celeus\Entities;

final readonly class Label
{
    public function __construct(
        public int $id,
        public string $name,
        public string $nameList,
        public string $href,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['name'],
            $data['name_list'],
            $data['href'],
        );
    }
}
