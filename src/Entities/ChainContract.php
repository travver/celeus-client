<?php

declare(strict_types=1);

namespace Celeus\Entities;

use Celeus\Enums\PaymentType;
use Celeus\Values\Bank;
use Celeus\Values\Commission;
use Celeus\Values\CostSharing;
use Celeus\Values\DatePeriod;
use Celeus\Values\FeePerBooking;
use Celeus\Values\Invoice;

final readonly class ChainContract
{
    public function __construct(
        public int $id,
        public Commission $commission,
        public CostSharing $costSharing,
        public FeePerBooking $feePerBooking,
        public string $targetBonus,
        public PaymentType $paymentType,
        public string $debtorNumber,
        public string $vatNumber,
        public Invoice $invoice,
        public string $contactEmail,
        public Bank $bank,
        public string $costs,
        public DatePeriod $arrival,
        public DatePeriod $creation,
        public string $href,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            Commission::fromArray($data['commission']),
            CostSharing::fromArray($data['cost_sharing']),
            FeePerBooking::fromArray($data['fee_per_booking']),
            $data['target_bonus'],
            PaymentType::from($data['payment_type']),
            $data['debtor_number'],
            $data['vat_number'],
            Invoice::fromArray($data['invoice']),
            $data['contact_email'],
            Bank::fromArray($data['bank']),
            $data['costs'],
            DatePeriod::fromArray($data['arrival']),
            DatePeriod::fromArray($data['creation']),
            $data['href'],
        );
    }

}
