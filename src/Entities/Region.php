<?php

declare(strict_types=1);

namespace Celeus\Entities;

final readonly class Region
{
    public function __construct(
        public int $id,
        public string $name,
        public string $url,
        public int $countryId,
        public bool $administrative,
        public string $href,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['name'],
            $data['url'],
            $data['country_id'],
            $data['administrative'],
            $data['href'],
        );
    }
}
