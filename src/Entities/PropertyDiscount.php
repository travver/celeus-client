<?php

declare(strict_types=1);

namespace Celeus\Entities;

use Celeus\Enums\Period;
use Celeus\Values\DatePeriod;
use Celeus\Values\Discount;
use Celeus\Values\DiscountRestriction;

final readonly class PropertyDiscount
{
    /**
     * @param int[] $accommodations
     * @param Period[] $periods
     */
    public function __construct(
        public int $id,
        public Discount $discount,
        public DiscountRestriction $restriction,
        public int $accommodationsMax,
        public DatePeriod $dateBooking,
        public DatePeriod $dateArrival,
        public bool $allowCombination,
        public string $slogan,
        public array $accommodations,
        public array $periods,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            Discount::fromArray($data['discount']),
            DiscountRestriction::fromArray($data['restrict']),
            $data['accommodations_max'],
            DatePeriod::fromArray($data['date_booking']),
            DatePeriod::fromArray($data['date_arrival']),
            $data['allow_combination'],
            $data['slogan'],
            $data['accommodations'],
            array_map(fn (string $period): Period => Period::from($period), $data['periods']),
        );
    }
}
