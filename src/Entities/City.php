<?php

declare(strict_types=1);

namespace Celeus\Entities;

final readonly class City
{
    public function __construct(
        public int $id,
        public string $name,
        public int $regionId,
        public string $href,
        public ?Region $region,
        public ?Country $country,
    ) {
    }

    public static function fromArray(array $data, ?Region $region = null, ?Country $country = null): self
    {
        return new self(
            $data['id'],
            $data['name'],
            $data['region_id'],
            $data['href'],
            $region,
            $country,
        );
    }
}
