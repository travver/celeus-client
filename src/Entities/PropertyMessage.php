<?php

declare(strict_types=1);

namespace Celeus\Entities;

use Celeus\Enums\MessageVisibility;
use Celeus\Utilities\DateParser;
use DateTimeImmutable;

final readonly class PropertyMessage
{
    /**
     * @param MessageVisibility[] $visibility
     * @param int[] $excludeAccommodations
     */
    public function __construct(
        public int $id,
        public ?MessageCategory $category,
        public ?DateTimeImmutable $dateFrom,
        public ?DateTimeImmutable $dateUntil,
        public bool $dateStay,
        public string $message,
        public int $listOrder,
        public bool $published,
        public array $visibility,
        public array $excludeAccommodations,
        public string $href,
    ) {
    }

    public static function fromArray(array $data, ?MessageCategory $category): self
    {
        return new self(
            $data['id'],
            $category,
            $data['date_from'] ? DateParser::parse($data['date_from'])->setTime(0, 0) : null,
            $data['date_until'] ? DateParser::parse($data['date_until'])->setTime(0, 0) : null,
            $data['date_stay'],
            $data['message'],
            $data['list_order'],
            $data['published'],
            array_map(
                fn (string $visibility): MessageVisibility => MessageVisibility::from($visibility),
                $data['visibility']
            ),
            $data['exclude_accommodations'],
            $data['href'],
        );
    }
}
