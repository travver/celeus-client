<?php

declare(strict_types=1);

namespace Celeus\Entities;

final readonly class Accommodation
{
    /**
     * @param int[] $facilities
     */
    public function __construct(
        public int $id,
        public int $propertyId,
        public int $typeId,
        public int $label,
        public string $name,
        public string $url,
        public string $description,
        public int $personsMax,
        public int $adultsMin,
        public int $adultsMax,
        public int $teenagersMax,
        public int $childrenMax,
        public int $babiesMax,
        public int $petsMax,
        public int $bedrooms,
        public int $surface,
        public string $labelName,
        public string $labelInList,
        public bool $published,
        public bool $deleted,
        public array $facilities,
        public string $href,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['property_id'],
            $data['type_id'],
            $data['label'],
            $data['name'],
            $data['url'],
            $data['description'],
            $data['persons_max'],
            $data['adults_min'],
            $data['adults_max'],
            $data['teenagers_max'],
            $data['children_max'],
            $data['babies_max'],
            $data['pets_max'],
            $data['bedrooms'],
            (int) $data['surface'],
            $data['label_name'],
            $data['label_in_list'],
            $data['published'],
            $data['deleted'],
            $data['facilities'],
            $data['href'],
        );
    }
}
