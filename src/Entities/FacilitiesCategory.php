<?php

declare(strict_types=1);

namespace Celeus\Entities;

final readonly class FacilitiesCategory
{
    public function __construct(
        public int $id,
        public string $name,
        public bool $forProperties,
        public bool $forAccommodations,
        public string $href,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['name'],
            $data['for_properties'],
            $data['for_accommodations'],
            $data['href'],
        );
    }
}
