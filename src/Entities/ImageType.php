<?php

declare(strict_types=1);

namespace Celeus\Entities;

use Celeus\Values\ImageTypeProperties;

final readonly class ImageType
{
    public function __construct(
        public int $id,
        public string $name,
        public ImageTypeProperties $properties,
        public string $href,
        public ?string $url,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['name'],
            ImageTypeProperties::fromArray($data['properties']),
            $data['href'],
            $data['url'] ?? null,
        );
    }
}
