<?php

declare(strict_types=1);

namespace Celeus\Entities;

use Celeus\Values\Time;

final readonly class Property
{
    /**
     * @param array<int> $facilities
     */
    public function __construct(
        public int $id,
        public int $chainId,
        public int $cityId,
        public int $label,
        public string $name,
        public string $nameSorting,
        public string $nameLetter,
        public string $url,
        public string $description,
        public string $goodToKnow,
        public string $remarksComments,
        public string $usp,
        public string $address,
        public string $postalCode,
        public Time $checkIn,
        public Time $checkOut,
        public float $latitude,
        public float $longitude,
        public bool $published,
        public string $labelName,
        public string $labelInList,
        public string $notificationEmail,
        public bool $giftCardsAllowed,
        public array $facilities,
        public string $href,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['chain_id'],
            $data['city_id'],
            $data['label'],
            $data['name'],
            $data['name_sorting'],
            $data['name_letter'],
            $data['url'],
            $data['description'],
            $data['good_to_know'],
            $data['remarks_comments'],
            $data['usp'],
            $data['address'],
            $data['postalcode'],
            Time::fromString($data['check_in']),
            Time::fromString($data['check_out']),
            $data['latitude'],
            $data['longitude'],
            $data['published'],
            $data['label_name'],
            $data['label_in_list'],
            $data['notification_email'],
            $data['wj_cards'],
            $data['facilities'] ?? [],
            $data['href'],
        );
    }
}
