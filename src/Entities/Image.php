<?php

declare(strict_types=1);

namespace Celeus\Entities;

use Celeus\Enums\RelationType;
use Celeus\Values\ImageInfo;
use Celeus\Values\ImageUrl;

final readonly class Image
{
    public function __construct(
        public int $id,
        public string $file,
        public ?ImageInfo $imageInfo,
        public ?ImageInfo $thumbnailInfo,
        public string $alt,
        public string $title,
        public bool $isPrimary,
        public int $focusLeft,
        public int $focusTop,
        public int $listOrder,
        public int $relationValue,
        public RelationType $relationType,
        public bool $published,
        public ImageType $imageType,
        public ImageUrl $imageUrl,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['file'],
            ImageInfo::fromArray($data['image_info']),
            ImageInfo::fromArray($data['thumbnail_info']),
            $data['alt'],
            $data['title'],
            $data['is_primary'],
            $data['focus_left'],
            $data['focus_top'],
            $data['list_order'],
            $data['relation_value'],
            RelationType::from($data['relation_type']),
            $data['published'],
            ImageType::fromArray($data['type']),
            ImageUrl::fromArray($data['url']),
        );
    }
}
