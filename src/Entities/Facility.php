<?php

declare(strict_types=1);

namespace Celeus\Entities;

final readonly class Facility
{
    /**
     * @param int[]|null $categories
     */
    public function __construct(
        public int $id,
        public string $name,
        public bool $published,
        public bool $showInTeaser,
        public string $icon,
        public bool $asFilter,
        public int $accommodations,
        public int $properties,
        public string $href,
        public ?array $categories,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['name'],
            $data['published'],
            $data['show_in_teaser'],
            $data['icon'],
            $data['as_filter'],
            $data['accommodations'],
            $data['properties'],
            $data['href'],
            $data['categories'] ?? null,
        );
    }
}
