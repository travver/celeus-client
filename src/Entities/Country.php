<?php

declare(strict_types=1);

namespace Celeus\Entities;

final readonly class Country
{
    public function __construct(
        public int $id,
        public string $name,
        public string $url,
        public int $listOrder,
        public string $code,
        public string $href,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['name'],
            $data['url'],
            $data['list_order'],
            $data['code'],
            $data['href'],
        );
    }
}
