<?php

declare(strict_types=1);

namespace Celeus\Entities;

final readonly class MessageCategory
{
    public function __construct(
        public int $id,
        public string $name,
        public int $listOrder,
        public string $href,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['name'],
            $data['list_order'],
            $data['href'],
        );
    }
}
