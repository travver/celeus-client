<?php

declare(strict_types=1);

namespace Celeus\Entities;

use Celeus\Values\SameDay;

final readonly class Chain
{
    public function __construct(
        public int $id,
        public string $name,
        public string $url,
        public string $description,
        public string $homeOfficeAddress,
        public string $homeOfficeEmail,
        public string $homeOfficePhone,
        public int $homeOfficeLanguageId,
        public string $notificationEmail,
        public bool $giftCardsAllowed,
        public ?SameDay $sameDay,
        public string $href,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['name'],
            $data['url'],
            $data['description'],
            $data['ho_address'],
            $data['ho_email'],
            $data['ho_phone'],
            $data['ho_language'],
            $data['notification_email'],
            $data['wj_cards'],
            array_key_exists('sameday', $data) ? SameDay::fromArray($data['sameday']) : null,
            $data['href'],
        );
    }
}
