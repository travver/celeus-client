<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\ChainContractResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<ChainContractResponse>
 */
final readonly class ChainContractRequest extends BaseRequest implements Request
{
    public function __construct(
        private int $id
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/chains/%d/contract.json', $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): ChainContractResponse
    {
        return ChainContractResponse::create($this, $response);
    }
}
