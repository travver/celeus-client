<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\CountryResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<CountryResponse>
 */
final readonly class CountryRequest extends BaseRequest implements Request
{
    public function __construct(
        private int $id,
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/countries/%d.json', $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): CountryResponse
    {
        return CountryResponse::create($this, $response);
    }
}
