<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Filters\AccommodationsRequestFilter;
use Celeus\Requests\Pagination\Pagination;
use Celeus\Responses\AccommodationsResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<AccommodationsResponse>
 */
final readonly class AccommodationsRequest extends BaseRequest implements Request
{
    public function __construct(
        private AccommodationsRequestFilter $filter,
        private Pagination $pagination = new Pagination(),
    ) {
    }

    public function getEndpoint(): string
    {
        return '/accommodations.json';
    }

    public function getFilter(): AccommodationsRequestFilter
    {
        return $this->filter;
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): AccommodationsResponse
    {
        return AccommodationsResponse::create($this, $response);
    }
}
