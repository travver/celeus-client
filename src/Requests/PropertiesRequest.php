<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Filters\PropertiesRequestFilter;
use Celeus\Requests\Pagination\Pagination;
use Celeus\Responses\PropertiesResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<PropertiesResponse>
 */
final readonly class PropertiesRequest extends BaseRequest implements Request
{
    public function __construct(
        private PropertiesRequestFilter $filter,
        private Pagination $pagination = new Pagination(),
    ) {
    }

    public function getEndpoint(): string
    {
        return '/properties.json';
    }

    public function getFilter(): PropertiesRequestFilter
    {
        return $this->filter;
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): PropertiesResponse
    {
        return PropertiesResponse::create($this, $response);
    }
}
