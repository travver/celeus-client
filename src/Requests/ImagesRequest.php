<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Filters\ImageRequestFilter;
use Celeus\Requests\Pagination\Pagination;
use Celeus\Responses\ImagesResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<ImagesResponse>
 */
final readonly class ImagesRequest extends BaseRequest implements Request
{
    public function __construct(
        private ImageRequestFilter $filter,
        private Pagination $pagination = new Pagination(),
    ) {
    }

    public function getEndpoint(): string
    {
        return '/images.json';
    }

    public function getFilter(): ImageRequestFilter
    {
        return $this->filter;
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): ImagesResponse
    {
        return ImagesResponse::create($this, $response);
    }
}
