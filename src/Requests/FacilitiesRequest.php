<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Filters\FacilityRequestFilter;
use Celeus\Requests\Pagination\Pagination;
use Celeus\Responses\FacilitiesResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<FacilitiesResponse>
 */
final readonly class FacilitiesRequest extends BaseRequest implements Request
{
    public function __construct(
        private FacilityRequestFilter $filter,
        private Pagination $pagination = new Pagination(),
    ) {
    }

    public function getEndpoint(): string
    {
        return '/facilities.json';
    }

    public function getFilter(): FacilityRequestFilter
    {
        return $this->filter;
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): FacilitiesResponse
    {
        return FacilitiesResponse::create($this, $response);
    }
}
