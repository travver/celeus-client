<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\AccommodationMessagesResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<AccommodationMessagesResponse>
 */
final readonly class AccommodationMessagesRequest extends BaseRequest implements Request
{
    public function __construct(
        private int $id,
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/accommodations/%d/messages.json', $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): AccommodationMessagesResponse
    {
        return AccommodationMessagesResponse::create($this, $response);
    }
}
