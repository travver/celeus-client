<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\PropertyResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<PropertyResponse>
 */
final readonly class PropertyRequest extends BaseRequest implements Request
{
    public function __construct(
        public int $id
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/properties/%d.json', $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): PropertyResponse
    {
        return PropertyResponse::create($this, $response);
    }
}
