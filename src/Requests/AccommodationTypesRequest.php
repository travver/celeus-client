<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\AccommodationTypesResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<AccommodationTypesResponse>
 */
final readonly class AccommodationTypesRequest extends BaseRequest implements Request
{
    public function getEndpoint(): string
    {
        return '/accommodations/types.json';
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): AccommodationTypesResponse
    {
        return AccommodationTypesResponse::create($this, $response);
    }
}
