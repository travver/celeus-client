<?php

declare(strict_types=1);

namespace Celeus\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\BaseRequest;
use Celeus\Requests\Content\Filters\ListingTagsRequestFilter;
use Celeus\Requests\Pagination\Pagination;
use Celeus\Requests\Request;
use Celeus\Responses\Content\ListingTagsResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<ListingTagsResponse>
 */
final readonly class ListingTagsRequest extends BaseRequest implements Request
{
    public function __construct(
        private ?ListingTagsRequestFilter $filter = null,
        private Pagination $pagination = new Pagination(),
    ) {
    }

    public function getEndpoint(): string
    {
        return '/content/tags.json';
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): ListingTagsResponse
    {
        return ListingTagsResponse::create($this, $response);
    }

    public function getFilter(): ?ListingTagsRequestFilter
    {
        return $this->filter;
    }
}
