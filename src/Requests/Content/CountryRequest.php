<?php

declare(strict_types=1);

namespace Celeus\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\BaseRequest;
use Celeus\Requests\Request;
use Celeus\Responses\Content\CountryResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<CountryResponse>
 */
final readonly class CountryRequest extends BaseRequest implements Request
{
    public function __construct(
        public int $id,
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf("/content/countries/%d.json", $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): CountryResponse
    {
        return CountryResponse::create($this, $response);
    }
}
