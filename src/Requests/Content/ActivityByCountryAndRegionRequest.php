<?php

declare(strict_types=1);

namespace Celeus\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\BaseRequest;
use Celeus\Requests\Content\Filters\ListingActivityRequestFilter;
use Celeus\Requests\Request;
use Celeus\Responses\Content\ActivityResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<ActivityResponse>
 */
final readonly class ActivityByCountryAndRegionRequest extends BaseRequest implements Request
{
    public function __construct(
        public string $country,
        public string $region,
        public string $activity,
        private ?ListingActivityRequestFilter $filter = null,
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf("/content/activities/%s/%s/%s.json", $this->country, $this->region, $this->activity);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): ActivityResponse
    {
        return ActivityResponse::create($this, $response);
    }

    public function getFilter(): ?ListingActivityRequestFilter
    {
        return $this->filter;
    }
}
