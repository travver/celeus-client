<?php

declare(strict_types=1);

namespace Celeus\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\BaseRequest;
use Celeus\Requests\Request;
use Celeus\Responses\Content\RegionResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<RegionResponse>
 */
final readonly class RegionRequest extends BaseRequest implements Request
{
    public function __construct(
        public int $id
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/content/regions/%d.json', $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): RegionResponse
    {
        return RegionResponse::create($this, $response);
    }
}
