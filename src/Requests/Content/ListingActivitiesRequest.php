<?php

declare(strict_types=1);

namespace Celeus\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\BaseRequest;
use Celeus\Requests\Content\Filters\ListingActivitiesRequestFilter;
use Celeus\Requests\Pagination\Pagination;
use Celeus\Requests\Request;
use Celeus\Responses\Content\ListingActivitiesResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<ListingActivitiesResponse>
 */
final readonly class ListingActivitiesRequest extends BaseRequest implements Request
{
    public function __construct(
        private ?ListingActivitiesRequestFilter $filter = null,
        private Pagination $pagination = new Pagination(),
    ) {
    }

    public function getEndpoint(): string
    {
        return '/content/activities.json';
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): ListingActivitiesResponse
    {
        return ListingActivitiesResponse::create($this, $response);
    }

    public function getFilter(): ?ListingActivitiesRequestFilter
    {
        return $this->filter;
    }
}
