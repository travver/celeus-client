<?php

declare(strict_types=1);

namespace Celeus\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\BaseRequest;
use Celeus\Requests\Request;
use Celeus\Responses\Content\ListingRegionsResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<ListingRegionsResponse>
 */
final readonly class ListingRegionsRequest extends BaseRequest implements Request
{
    public function __construct(
        public int $countryId
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/content/countries/%d/regions.json', $this->countryId);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): ListingRegionsResponse
    {
        return ListingRegionsResponse::create($this, $response);
    }
}
