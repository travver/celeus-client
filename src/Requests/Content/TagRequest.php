<?php

declare(strict_types=1);

namespace Celeus\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\BaseRequest;
use Celeus\Requests\Request;
use Celeus\Responses\Content\TagResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<TagResponse>
 */
final readonly class TagRequest extends BaseRequest implements Request
{
    public function __construct(
        private int $id,
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/content/tags/%d.json', $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): TagResponse
    {
        return TagResponse::create($this, $response);
    }
}
