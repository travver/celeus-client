<?php

declare(strict_types=1);

namespace Celeus\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\BaseRequest;
use Celeus\Requests\Request;
use Celeus\Responses\Content\ListingCountriesResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<ListingCountriesResponse>
 */
final readonly class ListingCountriesRequest extends BaseRequest implements Request
{
    public function getEndpoint(): string
    {
        return '/content/countries.json';
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): ListingCountriesResponse
    {
        return ListingCountriesResponse::create($this, $response);
    }
}
