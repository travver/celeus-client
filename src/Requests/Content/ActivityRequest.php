<?php

declare(strict_types=1);

namespace Celeus\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\BaseRequest;
use Celeus\Requests\Content\Filters\ListingActivityRequestFilter;
use Celeus\Requests\Request;
use Celeus\Responses\Content\ActivityResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<ActivityResponse>
 */
final readonly class ActivityRequest extends BaseRequest implements Request
{
    public function __construct(
        public int $id,
        private ?ListingActivityRequestFilter $filter = null,
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf("/content/activities/%d.json", $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): ActivityResponse
    {
        return ActivityResponse::create($this, $response);
    }

    public function getFilter(): ?ListingActivityRequestFilter
    {
        return $this->filter;
    }
}
