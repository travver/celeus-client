<?php

declare(strict_types=1);

namespace Celeus\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\BaseRequest;
use Celeus\Requests\Request;
use Celeus\Responses\Content\ListingTagsByCountryAndRegionResponse;
use Celeus\Responses\Response;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<ListingTagsByCountryAndRegionResponse>
 */
final readonly class ListingTagsByCountryAndRegionRequest extends BaseRequest implements Request
{
    public function __construct(
        private string $country,
        private string $region,
    ) {}

    public function getEndpoint(): string
    {
        return sprintf(
            '/content/regions/%s/%s/tags.json',
            $this->country,
            $this->region
        );
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): ListingTagsByCountryAndRegionResponse
    {
        return ListingTagsByCountryAndRegionResponse::create($this, $response);
    }
}