<?php

declare(strict_types=1);

namespace Celeus\Requests\Content\Filters;

use Celeus\Requests\Filters\Filter;

final readonly class ListingTagsRequestFilter implements Filter
{
    /**
     * @param array<int, int> $ids
     * @param array<string, int> $urls
     */
    public function __construct(
        private array $ids = [],
        private array $urls = [],
    ) {
    }

    public function getQueryParameters(): array
    {
        return array_filter([
            'id' => implode(',', $this->ids),
            'url' => implode(',', $this->urls),
        ]);
    }
}
