<?php

declare(strict_types=1);

namespace Celeus\Requests\Content\Filters;

use Celeus\Requests\Filters\Filter;

final readonly class ListingActivityRequestFilter implements Filter
{
    public function __construct(
        private ?bool $nearby = null,
        private ?bool $images = null,
    ) {
    }

    public function getQueryParameters(): array
    {
        return array_filter([
            'nearby' => $this->nearby,
            'images' => $this->images,
        ], fn (mixed $value): bool => $value !== null);
    }
}
