<?php

declare(strict_types=1);

namespace Celeus\Requests\Content\Filters;

use Celeus\Requests\Filters\Filter;

final readonly class ListingActivitiesRequestFilter implements Filter
{
    /**
     * @param array<int, int> $tags
     * @param array<int, int> $countries
     * @param array<int, int> $regions
     */
    public function __construct(
        private array $tags = [],
        private array $countries = [],
        private array $regions = [],
        private string $lat = '',
        private string $lng = '',
    ) {
    }

    public function getQueryParameters(): array
    {
        return array_filter([
            'tag' => implode(',', $this->tags),
            'country' => implode(',', $this->countries),
            'region' => implode(',', $this->regions),
            'lat' => $this->lat,
            'lng' => $this->lng,
        ]);
    }
}
