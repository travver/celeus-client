<?php

declare(strict_types=1);

namespace Celeus\Requests\Content;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\BaseRequest;
use Celeus\Requests\Request;
use Celeus\Responses\Content\CountryRegionResponse;
use Celeus\Responses\Content\RegionResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<CountryRegionResponse>
 */
final readonly class CountryRegionRequest extends BaseRequest implements Request
{
    public function __construct(
        public string $country,
        public string $region,
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/content/regions/%s/%s.json', $this->country, $this->region);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): CountryRegionResponse
    {
        return CountryRegionResponse::create($this, $response);
    }
}
