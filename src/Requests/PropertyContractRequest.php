<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\PropertyContractResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<PropertyContractResponse>
 */
final readonly class PropertyContractRequest extends BaseRequest implements Request
{
    public function __construct(
        private int $id
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/properties/%d/contract.json', $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): PropertyContractResponse
    {
        return PropertyContractResponse::create($this, $response);
    }
}
