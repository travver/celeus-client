<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\ChainMessageResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<ChainMessageResponse>
 */
final readonly class ChainMessagesRequest extends BaseRequest implements Request
{
    public function __construct(
        private int $id
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/chains/%d/messages.json', $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): ChainMessageResponse
    {
        return ChainMessageResponse::create($this, $response);
    }
}
