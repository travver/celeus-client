<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\ChainResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<ChainResponse>
 */
final readonly class ChainRequest extends BaseRequest implements Request
{
    public function __construct(
        private int $int
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/chains/%d.json', $this->int);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): ChainResponse
    {
        return ChainResponse::create($this, $response);
    }
}
