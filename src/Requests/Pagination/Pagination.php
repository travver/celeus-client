<?php

declare(strict_types=1);

namespace Celeus\Requests\Pagination;

final readonly class Pagination
{
    public function __construct(
        public int $page = 1,
        public ?int $perPage = null,
    ) {
    }

    /**
     * @return array<string, int>
     */
    public function getQueryParameters(): array
    {
        return array_filter([
            'page' => $this->page,
            'per_page' => $this->perPage,
        ]);
    }
}
