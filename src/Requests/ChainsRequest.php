<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\ChainsResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<ChainsResponse>
 */
final readonly class ChainsRequest extends BaseRequest implements Request
{
    public function getEndpoint(): string
    {
        return '/chains.json';
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): ChainsResponse
    {
        return ChainsResponse::create($this, $response);
    }
}
