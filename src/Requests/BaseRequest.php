<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Requests\Filters\Filter;
use Celeus\Requests\Options\Options;
use Celeus\Requests\Pagination\Pagination;

abstract readonly class BaseRequest
{
    public function getFilter(): ?Filter
    {
        return null;
    }

    public function getOptions(): ?Options
    {
        return null;
    }

    public function getPagination(): ?Pagination
    {
        return null;
    }
}
