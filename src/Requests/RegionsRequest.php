<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Filters\RegionsRequestFilter;
use Celeus\Responses\RegionsResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<RegionsResponse>
 */
final readonly class RegionsRequest extends BaseRequest implements Request
{
    public function __construct(
        private RegionsRequestFilter $filter,
    ) {
    }

    public function getEndpoint(): string
    {
        return '/regions.json';
    }

    public function getFilter(): RegionsRequestFilter
    {
        return $this->filter;
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): RegionsResponse
    {
        return RegionsResponse::create($this, $response);
    }
}
