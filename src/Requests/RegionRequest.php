<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\RegionResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<RegionResponse>
 */
final readonly class RegionRequest extends BaseRequest implements Request
{
    public function __construct(
        private int $id
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/regions/%d.json', $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): RegionResponse
    {
        return RegionResponse::create($this, $response);
    }
}
