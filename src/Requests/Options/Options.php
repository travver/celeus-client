<?php

declare(strict_types=1);

namespace Celeus\Requests\Options;

interface Options
{
    /**
     * @return array<string, mixed>
     */
    public function getQueryParameters(): array;
}
