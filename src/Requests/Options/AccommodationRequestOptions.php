<?php

declare(strict_types=1);

namespace Celeus\Requests\Options;

final readonly class AccommodationRequestOptions implements Options
{
    public function __construct(
        private ?bool $labels = null,
    ) {
    }

    public function getQueryParameters(): array
    {
        return array_filter([
            'labels' => $this->labels,
        ], fn (mixed $value): bool => $value !== null);
    }
}
