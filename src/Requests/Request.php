<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Filters\Filter;
use Celeus\Requests\Options\Options;
use Celeus\Requests\Pagination\Pagination;
use Celeus\Responses\Response;
use Psr\Http\Message\ResponseInterface;

/**
 * @template T of Response
 */
interface Request
{
    public function getEndpoint(): string;

    public function getFilter(): ?Filter;

    public function getOptions(): ?Options;

    public function getPagination(): ?Pagination;

    public function getMethod(): RequestMethod;

    /**
     * @return T
     */
    public function getResponse(ResponseInterface $response): Response;
}
