<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\FacilityResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<FacilityResponse>
 */
final readonly class FacilityRequest extends BaseRequest implements Request
{
    public function __construct(
        private int $id
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/facilities/%d.json', $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): FacilityResponse
    {
        return FacilityResponse::create($this, $response);
    }
}
