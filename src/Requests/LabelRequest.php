<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\LabelResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<LabelResponse>
 */
final readonly class LabelRequest extends BaseRequest implements Request
{
    public function __construct(
        private int $id
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/labels/%d.json', $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): LabelResponse
    {
        return LabelResponse::create($this, $response);
    }
}
