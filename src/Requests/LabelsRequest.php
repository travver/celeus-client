<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\LabelsResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<LabelsResponse>
 */
final readonly class LabelsRequest extends BaseRequest implements Request
{
    public function getEndpoint(): string
    {
        return '/labels.json';
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): LabelsResponse
    {
        return LabelsResponse::create($this, $response);
    }
}
