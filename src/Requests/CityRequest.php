<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\CityResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<CityResponse>
 */
final readonly class CityRequest extends BaseRequest implements Request
{
    public function __construct(
        private int $id,
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/cities/%d.json', $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): CityResponse
    {
        return CityResponse::create($this, $response);
    }
}
