<?php

declare(strict_types=1);

namespace Celeus\Requests\Filters;

final readonly class AccommodationsRequestFilter implements Filter
{
    public function __construct(
        private ?int $chain = null,
        private ?int $property = null,
        private ?int $facility = null,
    ) {
    }

    public function getQueryParameters(): array
    {
        return array_filter([
            'chain' => $this->chain,
            'property' => $this->property,
            'facility' => $this->facility,
        ], fn (int | bool | null $value): bool => $value !== null);
    }
}
