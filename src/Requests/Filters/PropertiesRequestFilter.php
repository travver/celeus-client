<?php

declare(strict_types=1);

namespace Celeus\Requests\Filters;

final readonly class PropertiesRequestFilter implements Filter
{
    public function __construct(
        private ?int $chain = null,
        private ?int $country = null,
        private ?int $region = null,
        private ?int $city = null,
        private ?int $facility = null,
        private ?bool $published = null,
    ) {
    }

    public function getQueryParameters(): array
    {
        return array_filter([
            'chain' => $this->chain,
            'country' => $this->country,
            'region' => $this->region,
            'city' => $this->city,
            'facility' => $this->facility,
            'published' => $this->published,
        ], fn (int|bool|null $value): bool => $value !== null);
    }
}
