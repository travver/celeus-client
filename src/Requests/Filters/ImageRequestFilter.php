<?php

declare(strict_types=1);

namespace Celeus\Requests\Filters;

use Celeus\Enums\RelationType;

final readonly class ImageRequestFilter implements Filter
{
    public function __construct(
        private ?int $imageTypeId = null,
        private ?RelationType $relationType = null,
        private ?int $relationValue = null,
    ) {
    }

    public function getQueryParameters(): array
    {
        return array_filter([
            'image_type_id' => $this->imageTypeId,
            'relation_type' => $this->relationType?->value,
            'relation_value' => $this->relationValue,
        ]);
    }
}
