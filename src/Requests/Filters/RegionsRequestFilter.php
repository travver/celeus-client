<?php

declare(strict_types=1);

namespace Celeus\Requests\Filters;

final readonly class RegionsRequestFilter implements Filter
{
    /**
     * @param array<int, int> $countries
     */
    public function __construct(
        private ?array $countries = [],
    ) {
    }

    public function getQueryParameters(): array
    {
        return array_filter(['country' => implode(',', $this->countries)]);
    }
}
