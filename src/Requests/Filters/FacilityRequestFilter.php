<?php

declare(strict_types=1);

namespace Celeus\Requests\Filters;

final readonly class FacilityRequestFilter implements Filter
{
    public function __construct(
        private ?int $category = null,
        private ?bool $published = null,
        private ?bool $asFilter = null,
        private ?bool $forProperties = null,
        private ?bool $forAccommodations = null,
    ) {
    }

    public function getQueryParameters(): array
    {
        return array_filter([
            'category' => $this->category,
            'published' => $this->published,
            'as_filter' => $this->asFilter,
            'for_properties' => $this->forProperties,
            'for_accommodations' => $this->forAccommodations,
        ], fn (int | bool | null $value): bool => $value !== null);
    }
}
