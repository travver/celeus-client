<?php

declare(strict_types=1);

namespace Celeus\Requests\Filters;

final readonly class AccommodationRequestFilter implements Filter
{
    /**
     * @param array<int, int> $facilities
     */
    public function __construct(
        private ?int $accommodation = null,
        private ?int $property = null,
        private ?array $facilities = null,
        private ?bool $published = null,
        private ?bool $source = null
    ) {
    }

    public function getQueryParameters(): array
    {
        return array_filter([
            'accommodation' => $this->accommodation,
            'property' => $this->property,
            'facilities' => $this->facilities,
            'published' => $this->published,
            'source' => $this->source,
        ], fn (mixed $value): bool => $value !== null);
    }
}
