<?php

declare(strict_types=1);

namespace Celeus\Requests\Filters;

interface Filter
{
    /**
     * @return array<string, mixed>
     */
    public function getQueryParameters(): array;
}
