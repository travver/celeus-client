<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\PropertyDiscountsResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<PropertyDiscountsResponse>
 */
final readonly class PropertyDiscountsRequest extends BaseRequest implements Request
{
    public function __construct(
        private int $id,
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/properties/%d/discounts.json', $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): PropertyDiscountsResponse
    {
        return PropertyDiscountsResponse::create($this, $response);
    }
}
