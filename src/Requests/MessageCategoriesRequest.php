<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\MessageCategoriesResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<MessageCategoriesResponse>
 */
final readonly class MessageCategoriesRequest extends BaseRequest implements Request
{
    public function getEndpoint(): string
    {
        return '/messages/categories.json';
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): MessageCategoriesResponse
    {
        return MessageCategoriesResponse::create($this, $response);
    }
}
