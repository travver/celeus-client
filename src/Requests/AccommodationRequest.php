<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Filters\AccommodationRequestFilter;
use Celeus\Requests\Options\AccommodationRequestOptions;
use Celeus\Requests\Options\Options;
use Celeus\Responses\AccommodationResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<AccommodationResponse>
 */
final readonly class AccommodationRequest extends BaseRequest implements Request
{
    public function __construct(
        private int $id,
        private ?AccommodationRequestFilter $filter = null,
        private ?AccommodationRequestOptions $options = null,
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/accommodations/%d.json', $this->id);
    }

    public function getFilter(): ?AccommodationRequestFilter
    {
        return $this->filter;
    }

    public function getOptions(): ?Options
    {
        return $this->options;
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): AccommodationResponse
    {
        return AccommodationResponse::create($this, $response);
    }
}
