<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Requests\Pagination\Pagination;
use Celeus\Responses\FacilitiesCategoryResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<FacilitiesCategoryResponse>
 */
final readonly class FacilitiesCategoryRequest extends BaseRequest implements Request
{
    public function __construct(
        private Pagination $pagination = new Pagination(),
    ) {
    }

    public function getEndpoint(): string
    {
        return '/facilities/categories.json';
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): FacilitiesCategoryResponse
    {
        return FacilitiesCategoryResponse::create($this, $response);
    }
}
