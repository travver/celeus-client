<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\PropertyMessagesResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<PropertyMessagesResponse>
 */
final readonly class PropertyMessagesRequest extends BaseRequest implements Request
{
    public function __construct(
        private int $id
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf('/properties/%d/messages.json', $this->id);
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): PropertyMessagesResponse
    {
        return PropertyMessagesResponse::create($this, $response);
    }
}
