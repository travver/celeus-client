<?php

declare(strict_types=1);

namespace Celeus\Requests;

use Celeus\Enums\RequestMethod;
use Celeus\Responses\CountriesResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * @implements Request<CountriesResponse>
 */
final readonly class CountriesRequest extends BaseRequest implements Request
{
    public function getEndpoint(): string
    {
        return '/countries.json';
    }

    public function getMethod(): RequestMethod
    {
        return RequestMethod::GET;
    }

    public function getResponse(ResponseInterface $response): CountriesResponse
    {
        return CountriesResponse::create($this, $response);
    }
}
