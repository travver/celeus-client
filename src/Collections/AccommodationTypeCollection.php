<?php

declare(strict_types=1);

namespace Celeus\Collections;

use Celeus\Entities\AccommodationType;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, AccommodationType>
 */
final class AccommodationTypeCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(
                fn (array $accommodationType): AccommodationType => AccommodationType::fromArray($accommodationType),
                $data
            )
        );
    }
}
