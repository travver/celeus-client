<?php

declare(strict_types=1);

namespace Celeus\Collections;

use Celeus\Entities\Property;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, Property>
 */
final class PropertyCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(fn (array $property): Property => Property::fromArray($property), $data)
        );
    }
}
