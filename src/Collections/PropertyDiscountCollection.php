<?php

declare(strict_types=1);

namespace Celeus\Collections;

use Celeus\Entities\PropertyDiscount;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, PropertyDiscount>
 */
final class PropertyDiscountCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(
                fn (array $propertyDiscount): PropertyDiscount => PropertyDiscount::fromArray($propertyDiscount),
                $data
            )
        );
    }
}
