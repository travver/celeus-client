<?php

declare(strict_types=1);

namespace Celeus\Collections;

use Celeus\Entities\Facility;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, Facility>
 */
final class FacilityCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(fn (array $facility): Facility => Facility::fromArray($facility), $data)
        );
    }
}
