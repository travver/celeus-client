<?php

declare(strict_types=1);

namespace Celeus\Collections;

use Celeus\Entities\Country;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, Country>
 */
final class CountryCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(fn (array $country): Country => Country::fromArray($country), $data)
        );
    }
}
