<?php

declare(strict_types=1);

namespace Celeus\Collections;

use Celeus\Entities\MessageCategory;
use Celeus\Entities\PropertyMessage;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, PropertyMessage>
 */
final class PropertyMessageCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        $categories = array_combine(
            array_map(fn (array $category): int => $category['id'], $data['categories']),
            array_map(fn (array $category): MessageCategory => MessageCategory::fromArray($category), $data['categories']),
        );

        return new self(
            array_map(
                fn (array $propertyMessage): PropertyMessage => PropertyMessage::fromArray(
                    $propertyMessage,
                    $categories[$propertyMessage['category_id']] ?? null
                ),
                $data['messages']
            )
        );
    }
}
