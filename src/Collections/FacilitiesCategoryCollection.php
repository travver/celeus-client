<?php

declare(strict_types=1);

namespace Celeus\Collections;

use Celeus\Entities\FacilitiesCategory;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, FacilitiesCategory>
 */
final class FacilitiesCategoryCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(fn (array $facilitiesCategory): FacilitiesCategory => FacilitiesCategory::fromArray($facilitiesCategory), $data)
        );
    }
}
