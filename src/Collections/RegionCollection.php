<?php

declare(strict_types=1);

namespace Celeus\Collections;

use Celeus\Entities\Region;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, Region>
 */
final class RegionCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(fn (array $region): Region => Region::fromArray($region), $data)
        );
    }
}
