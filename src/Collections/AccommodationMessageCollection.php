<?php

declare(strict_types=1);

namespace Celeus\Collections;

use Celeus\Entities\AccommodationMessage;
use Celeus\Entities\MessageCategory;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, AccommodationMessage>
 */
final class AccommodationMessageCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        $categories = array_combine(
            array_map(fn (array $category): int => $category['id'], $data['categories']),
            array_map(fn (array $category): MessageCategory => MessageCategory::fromArray($category), $data['categories']),
        );

        return new self(
            array_map(
                fn (array $message): AccommodationMessage => AccommodationMessage::fromArray(
                    $message,
                    $categories[$message['category_id']] ?? null
                ),
                $data['messages']
            )
        );
    }
}
