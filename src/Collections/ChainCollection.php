<?php

declare(strict_types=1);

namespace Celeus\Collections;

use Celeus\Entities\Chain;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, Chain>
 */
final class ChainCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(
                fn (array $chain): Chain => Chain::fromArray($chain),
                $data
            )
        );
    }
}
