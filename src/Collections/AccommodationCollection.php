<?php

declare(strict_types=1);

namespace Celeus\Collections;

use Celeus\Entities\Accommodation;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, Accommodation>
 */
final class AccommodationCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(
                fn (array $accommodation): Accommodation => Accommodation::fromArray($accommodation),
                $data,
            )
        );
    }
}
