<?php

declare(strict_types=1);

namespace Celeus\Collections\Content;

use Celeus\Entities\Content\ListingCountry;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, ListingCountry>
 */
final class ListingCountryCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(
                fn (array $listingCountry): ListingCountry => ListingCountry::fromArray($listingCountry),
                $data
            )
        );
    }
}
