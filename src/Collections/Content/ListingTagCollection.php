<?php

declare(strict_types=1);

namespace Celeus\Collections\Content;

use Celeus\Entities\Content\ListingTag;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, ListingTag>
 */
final class ListingTagCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(fn (array $listingTag): ListingTag => ListingTag::fromArray($listingTag), $data)
        );
    }
}
