<?php

declare(strict_types=1);

namespace Celeus\Collections\Content;

use Celeus\Entities\Content\ListingRegion;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, ListingRegion>
 */
final class ListingRegionCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(
                fn (array $listingRegion): ListingRegion => ListingRegion::fromArray($listingRegion),
                $data
            )
        );
    }
}
