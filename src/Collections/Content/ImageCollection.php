<?php

declare(strict_types=1);

namespace Celeus\Collections\Content;

use Celeus\Entities\Image;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, Image>
 */
final class ImageCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(fn (array $image): Image => Image::fromArray($image), $data)
        );
    }
}
