<?php

declare(strict_types=1);

namespace Celeus\Collections\Content;

use Celeus\Entities\Content\ListingActivity;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, ListingActivity>
 */
final class ListingActivityCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(fn (array $listingActivity): ListingActivity => ListingActivity::fromArray($listingActivity), $data)
        );
    }
}
