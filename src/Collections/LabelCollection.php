<?php

declare(strict_types=1);

namespace Celeus\Collections;

use Celeus\Entities\Label;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, Label>
 */
final class LabelCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(fn (array $label): Label => Label::fromArray($label), $data)
        );
    }
}
