<?php

declare(strict_types=1);

namespace Celeus\Collections;

use Celeus\Entities\MessageCategory;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, MessageCategory>
 */
final class MessageCategoryCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(
            array_map(
                fn (array $messageCategory): MessageCategory => MessageCategory::fromArray($messageCategory),
                $data
            )
        );
    }
}
