<?php

declare(strict_types=1);

namespace Celeus\Collections;

use Celeus\Entities\City;
use Illuminate\Support\Collection;

/**
 * @extends Collection<int, City>
 */
final class CityCollection extends Collection
{
    public static function fromArray(array $data): self
    {
        return new self(array_map(fn (array $city) => City::fromArray($city), $data));
    }
}
