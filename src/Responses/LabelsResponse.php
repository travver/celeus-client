<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\LabelCollection;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class LabelsResponse extends BaseResponse implements Response
{
    public function __construct(
        public LabelCollection $labels,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            LabelCollection::fromArray((array)$data['labels']),
        );
    }
}
