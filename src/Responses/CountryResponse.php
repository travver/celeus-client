<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\RegionCollection;
use Celeus\Entities\Country;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class CountryResponse extends BaseResponse implements Response
{
    public function __construct(
        public Country $country,
        public RegionCollection $regions,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            Country::fromArray((array)$data['country']),
            RegionCollection::fromArray((array)$data['regions']),
        );
    }
}
