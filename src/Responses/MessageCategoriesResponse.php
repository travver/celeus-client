<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\MessageCategoryCollection;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class MessageCategoriesResponse extends BaseResponse implements Response
{
    public function __construct(
        public MessageCategoryCollection $messageCategories,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            MessageCategoryCollection::fromArray((array)$data['categories']),
        );
    }
}
