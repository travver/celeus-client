<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Entities\Property;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class PropertyResponse extends BaseResponse implements Response
{
    public function __construct(
        public Property $property,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            Property::fromArray((array)$data['property']),
        );
    }
}
