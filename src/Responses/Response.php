<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

interface Response
{
    /**
     * @param Request<Response> $request
     */
    public static function create(
        Request $request,
        ResponseInterface $response,
    ): self;
}
