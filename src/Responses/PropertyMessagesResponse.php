<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\PropertyMessageCollection;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class PropertyMessagesResponse extends BaseResponse implements Response
{
    public function __construct(
        public PropertyMessageCollection $messages,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            PropertyMessageCollection::fromArray($data),
        );
    }
}
