<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\CityCollection;
use Celeus\Entities\Country;
use Celeus\Entities\Region;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class RegionResponse extends BaseResponse implements Response
{
    public function __construct(
        public Region $region,
        public Country $country,
        public CityCollection $cities,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            Region::fromArray((array)$data['region']),
            Country::fromArray((array)$data['country']),
            CityCollection::fromArray((array)$data['cities']),
        );
    }
}
