<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\AccommodationMessageCollection;
use Celeus\Requests\Request;
use Celeus\Responses\Pagination\Pagination;
use Psr\Http\Message\ResponseInterface;

final readonly class AccommodationMessagesResponse extends BaseResponse implements Response
{
    public function __construct(
        public AccommodationMessageCollection $messages,
        public ?Pagination $pagination = null,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        return new self(
            AccommodationMessageCollection::fromArray(
                (array)json_decode($response->getBody()->getContents(), true)
            ),
        );
    }
}
