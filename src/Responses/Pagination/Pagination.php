<?php

declare(strict_types=1);

namespace Celeus\Responses\Pagination;

final readonly class Pagination
{
    public function __construct(
        public int $page,
        public int $perPage,
        public int $total,
        public int $pages,
    ) {
    }
}
