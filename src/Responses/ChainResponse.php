<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Entities\Chain;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class ChainResponse extends BaseResponse implements Response
{
    public function __construct(
        public Chain $chain,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            Chain::fromArray((array)$data['chain']),
        );
    }
}
