<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\CountryCollection;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class CountriesResponse extends BaseResponse implements Response
{
    public function __construct(
        public CountryCollection $countries,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            CountryCollection::fromArray((array)$data['countries']),
        );
    }
}
