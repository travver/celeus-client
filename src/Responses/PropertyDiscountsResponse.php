<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\PropertyDiscountCollection;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class PropertyDiscountsResponse extends BaseResponse implements Response
{
    public function __construct(
        public PropertyDiscountCollection $discounts,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            PropertyDiscountCollection::fromArray((array)$data['discounts']),
        );
    }
}
