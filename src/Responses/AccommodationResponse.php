<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\LabelCollection;
use Celeus\Entities\Accommodation;
use Celeus\Entities\Source;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class AccommodationResponse extends BaseResponse implements Response
{
    public function __construct(
        public Accommodation $accommodation,
        public LabelCollection $labels,
        public ?Source $source,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            Accommodation::fromArray((array)$data['accommodation']),
            LabelCollection::fromArray((array)($data['labels'] ?? [])),
            Source::fromArray($data['source'] ?? null),
        );
    }
}
