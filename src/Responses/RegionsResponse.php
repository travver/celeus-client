<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\RegionCollection;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class RegionsResponse extends BaseResponse implements Response
{
    public function __construct(
        public RegionCollection $regions,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            RegionCollection::fromArray((array)$data['regions']),
        );
    }
}
