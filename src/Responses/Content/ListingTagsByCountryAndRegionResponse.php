<?php

declare(strict_types=1);

namespace Celeus\Responses\Content;

use Celeus\Collections\Content\ListingTagCollection;
use Celeus\Exceptions\UnknownException;
use Celeus\Exceptions\UnprocessableEntityException;
use Celeus\Requests\Request;
use Celeus\Responses\BaseResponse;
use Celeus\Responses\Response;
use Psr\Http\Message\ResponseInterface;

final readonly class ListingTagsByCountryAndRegionResponse extends BaseResponse implements Response
{
    public function __construct(
        public ListingTagCollection $listingTags,
    ) {}

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        if ($response->getStatusCode() !== 200) {
            match ($response->getStatusCode()) {
                422 => throw UnprocessableEntityException::fromArray($data),
                default => throw UnknownException::fromArray($data),
            };
        }

        return new self(
            ListingTagCollection::fromArray((array)$data['tags'])
        );
    }
}
