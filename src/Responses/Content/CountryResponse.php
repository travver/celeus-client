<?php

declare(strict_types=1);

namespace Celeus\Responses\Content;

use Celeus\Entities\Content\Country;
use Celeus\Requests\Request;
use Celeus\Responses\BaseResponse;
use Celeus\Responses\Response;
use Psr\Http\Message\ResponseInterface;

final readonly class CountryResponse extends BaseResponse implements Response
{
    public function __construct(
        public Country $country
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            Country::fromArray((array)$data['country'], (array)$data['content']),
        );
    }
}
