<?php

declare(strict_types=1);

namespace Celeus\Responses\Content;

use Celeus\Entities\Content\Region;
use Celeus\Requests\Request;
use Celeus\Responses\BaseResponse;
use Celeus\Responses\Response;
use Psr\Http\Message\ResponseInterface;

final readonly class RegionResponse extends BaseResponse implements Response
{
    public function __construct(
        public Region $region
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            Region::fromArray((array)$data['region'], (array)$data['content'])
        );
    }
}
