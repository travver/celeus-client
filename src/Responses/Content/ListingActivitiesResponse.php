<?php

declare(strict_types=1);

namespace Celeus\Responses\Content;

use Celeus\Collections\Content\ListingActivityCollection;
use Celeus\Exceptions\UnknownException;
use Celeus\Exceptions\UnprocessableEntityException;
use Celeus\Requests\Request;
use Celeus\Responses\BaseResponse;
use Celeus\Responses\Pagination\Pagination;
use Celeus\Responses\Response;
use Celeus\Utilities\PaginationBuilder;
use Psr\Http\Message\ResponseInterface;

final readonly class ListingActivitiesResponse extends BaseResponse implements Response
{
    public function __construct(
        public Pagination $pagination,
        public ListingActivityCollection $listingActivities,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        if ($response->getStatusCode() !== 200) {
            match ($response->getStatusCode()) {
                422 => throw UnprocessableEntityException::fromArray($data),
                default => throw UnknownException::fromArray($data),
            };
        }

        return new self(
            PaginationBuilder::build($response, $request),
            ListingActivityCollection::fromArray((array)$data['activities']),
        );
    }
}
