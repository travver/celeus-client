<?php

declare(strict_types=1);

namespace Celeus\Responses\Content;

use Celeus\Entities\Content\Tag;
use Celeus\Requests\Request;
use Celeus\Responses\BaseResponse;
use Celeus\Responses\Response;
use Psr\Http\Message\ResponseInterface;

final readonly class TagResponse extends BaseResponse implements Response
{
    public function __construct(
        public Tag $tag,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            Tag::fromArray((array)$data['tag']),
        );
    }
}
