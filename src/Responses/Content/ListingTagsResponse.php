<?php

declare(strict_types=1);

namespace Celeus\Responses\Content;

use Celeus\Collections\Content\ListingTagCollection;
use Celeus\Requests\Request;
use Celeus\Responses\BaseResponse;
use Celeus\Responses\Response;
use Psr\Http\Message\ResponseInterface;

final readonly class ListingTagsResponse extends BaseResponse implements Response
{
    public function __construct(
        public ListingTagCollection $tags,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            ListingTagCollection::fromArray((array)$data['tags']),
        );
    }
}
