<?php

declare(strict_types=1);

namespace Celeus\Responses\Content;

use Celeus\Entities\Content\CountryRegion;
use Celeus\Exceptions\UnknownException;
use Celeus\Exceptions\UnprocessableEntityException;
use Celeus\Requests\Request;
use Celeus\Responses\BaseResponse;
use Celeus\Responses\Response;
use Psr\Http\Message\ResponseInterface;

final readonly class CountryRegionResponse extends BaseResponse implements Response
{
    public function __construct(
        public CountryRegion $countryRegion
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        if ($response->getStatusCode() !== 200) {
            match ($response->getStatusCode()) {
                422 => throw UnprocessableEntityException::fromArray($data),
                default => throw UnknownException::fromArray($data),
            };
        }

        return new self(
            CountryRegion::fromArray((array)$data['country'], (array)$data['region'], (array)$data['content'])
        );
    }
}
