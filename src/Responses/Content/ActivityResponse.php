<?php

declare(strict_types=1);

namespace Celeus\Responses\Content;

use Celeus\Collections\Content\ImageCollection;
use Celeus\Entities\Content\Activity;
use Celeus\Entities\Country;
use Celeus\Entities\Region;
use Celeus\Exceptions\UnknownException;
use Celeus\Exceptions\UnprocessableEntityException;
use Celeus\Requests\Request;
use Celeus\Responses\BaseResponse;
use Celeus\Responses\Response;
use Psr\Http\Message\ResponseInterface;

final readonly class ActivityResponse extends BaseResponse implements Response
{
    public function __construct(
        public Activity $activity,
        public Region $region,
        public Country $country,
        public ImageCollection $imageCollection,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        if ($response->getStatusCode() !== 200) {
            match ($response->getStatusCode()) {
                422 => throw UnprocessableEntityException::fromArray($data),
                default => throw UnknownException::fromArray($data),
            };
        }

        return new self(
            Activity::fromArray((array)$data['activity']),
            Region::fromArray((array)$data['region']),
            Country::fromArray((array)$data['country']),
            ImageCollection::fromArray(array_key_exists('images', $data) ? (array)$data['images']: []),
        );
    }
}
