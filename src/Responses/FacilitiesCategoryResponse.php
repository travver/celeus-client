<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\FacilitiesCategoryCollection;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class FacilitiesCategoryResponse extends BaseResponse implements Response
{
    public function __construct(
        public FacilitiesCategoryCollection $facilitiesCategory,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            FacilitiesCategoryCollection::fromArray((array)$data['categories']),
        );
    }
}
