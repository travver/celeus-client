<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\ImageCollection;
use Celeus\Requests\Request;
use Celeus\Responses\Pagination\Pagination;
use Celeus\Utilities\PaginationBuilder;
use Psr\Http\Message\ResponseInterface;

final readonly class ImagesResponse implements Response
{
    public function __construct(
        public Pagination $pagination,
        public ImageCollection $images,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            PaginationBuilder::build($response, $request),
            ImageCollection::fromArray((array)$data['images'])
        );
    }
}
