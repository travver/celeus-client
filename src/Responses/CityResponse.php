<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Entities\City;
use Celeus\Entities\Country;
use Celeus\Entities\Region;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class CityResponse extends BaseResponse implements Response
{
    public function __construct(
        public City $city,
        public Region $region,
        public Country $country,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            City::fromArray((array)$data['city']),
            Region::fromArray((array)$data['region']),
            Country::fromArray((array)$data['country']),
        );
    }
}
