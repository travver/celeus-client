<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Entities\Facility;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class FacilityResponse extends BaseResponse implements Response
{
    public function __construct(
        public Facility $facility,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            Facility::fromArray((array)$data['facility']),
        );
    }
}
