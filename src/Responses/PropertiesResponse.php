<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\PropertyCollection;
use Celeus\Requests\Request;
use Celeus\Responses\Pagination\Pagination;
use Celeus\Utilities\PaginationBuilder;
use Psr\Http\Message\ResponseInterface;

final readonly class PropertiesResponse extends BaseResponse implements Response
{
    public function __construct(
        public Pagination $pagination,
        public PropertyCollection $properties,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            PaginationBuilder::build($response, $request),
            PropertyCollection::fromArray((array)$data['properties']),
        );
    }
}
