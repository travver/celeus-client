<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\ChainMessageCollection;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class ChainMessageResponse extends BaseResponse implements Response
{
    public function __construct(
        public ChainMessageCollection $messages,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        return new self(
            ChainMessageCollection::fromArray((array)json_decode($response->getBody()->getContents(), true)),
        );
    }
}
