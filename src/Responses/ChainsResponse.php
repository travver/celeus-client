<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Collections\ChainCollection;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class ChainsResponse extends BaseResponse implements Response
{
    public function __construct(
        public ChainCollection $chains,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            ChainCollection::fromArray((array)$data['chains']),
        );
    }
}
