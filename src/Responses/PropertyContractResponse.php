<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Entities\PropertyContract;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class PropertyContractResponse extends BaseResponse implements Response
{
    public function __construct(
        public PropertyContract $contract,
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        $data = (array)json_decode($response->getBody()->getContents(), true);

        return new self(
            PropertyContract::fromArray((array)$data['contract']),
        );
    }
}
