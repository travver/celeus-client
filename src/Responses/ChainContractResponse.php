<?php

declare(strict_types=1);

namespace Celeus\Responses;

use Celeus\Entities\ChainContract;
use Celeus\Requests\Request;
use Psr\Http\Message\ResponseInterface;

final readonly class ChainContractResponse extends BaseResponse implements Response
{
    public function __construct(
        public ChainContract $contract
    ) {
    }

    public static function create(Request $request, ResponseInterface $response): self
    {
        return new self(
            ChainContract::fromArray((array)((array)json_decode($response->getBody()->getContents(), true))['contract'])
        );
    }
}
