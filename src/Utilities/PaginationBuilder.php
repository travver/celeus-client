<?php

declare(strict_types=1);

namespace Celeus\Utilities;

use Celeus\Requests\Request;
use Celeus\Responses\Pagination\Pagination as ResponsePagination;
use Celeus\Responses\Response;
use Psr\Http\Message\ResponseInterface;

final readonly class PaginationBuilder
{
    /**
     * @param Request<Response> $request
     */
    public static function build(ResponseInterface $response, Request $request): ResponsePagination
    {
        $perPage = (int)$response->getHeader('X-Per-Page')[0];
        $total = (int)$response->getHeader('X-Total-Count')[0];

        return new ResponsePagination(
            $request->getPagination()?->page ?? 1,
            $perPage,
            $total,
            (int)ceil($total / $perPage),
        );
    }
}
