<?php

declare(strict_types=1);

namespace Celeus\Utilities;

use Celeus\Exceptions\ParseDateFailed;
use DateTimeImmutable;

final readonly class DateParser
{
    public static function parse(string $dateString): DateTimeImmutable
    {
        $date = DateTimeImmutable::createFromFormat('Y-m-d', $dateString);

        if ($date === false) {
            throw new ParseDateFailed(sprintf('Failed to parse date "%s"', $dateString));
        }

        return $date;
    }
}
